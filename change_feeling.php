<?php
include("check_cookies.php");
//Updating the feeling in the session
$answer = $conn->prepare('SELECT feeling FROM user WHERE id = ?');
$answer->execute(array($_SESSION['user']->id()));
if ($data = $answer->fetch()) {
	$_SESSION['user']->setFeeling($data['feeling']);
}
$answer->closeCursor();
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
        <style>
        	td {
        		width:14%;
        	}
        	.thumbnail {
        		height:60px;
        	}
        </style>
    </head>
    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Settings";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                		<div class="col-md-9">
	                        <!-- panel -->
	                        <div class="panel panel-default">
	                            <div class="panel-heading">Change Your Feeling : <img src="<?php if ($_SESSION['user']->feeling() != "") { echo $_SESSION['user']->feeling(); } else { echo 'images/useronline/1_2/online.gif'; } ?>" /></div>
	                            <div class="panel-body">
	                            	<table class="table">
	                            		<thead>
	                            			<tr>
	                            				<th><strong>Trainee</strong></th>
	                            				<th></th>
	                            				<th></th>
	                            				<th></th>
	                            				<th></th>
	                            				<th></th>
	                            				<th></th>
	                            			</tr>
	                            		</thead>
	                            		<tbody>
	                                		<?php
			                                $nbFile = 0;
			                                $columnCount = 0;
			                                //For everyone
			                                if ($dir = opendir('./images/useronline/1_2')) {
			                                    while (false !== ($file = readdir($dir))) {
			                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
			                                    		list($width, $height) = getimagesize('images/useronline/1_2/' . $file);
                                                        if ($width > 50 && $height > 50) {
                                                            $width = 50;
                                                            $height = 50;
                                                        } else if ($width > 50) {
                                                            $width = 50;
                                                        } else if ($height > 50) {
                                                            $height = 50;
                                                        }
			                                    		$nbFile++;
			                                    		if ($columnCount == 0) {
			                                    			echo '<tr>';
			                                    		}
			                                    		if ($columnCount == 6) {
			                                    			$columnCount = 0;
			                                    			echo '<td>';
			                                    			echo '<a href="#" class="thumbnail">';
			                                    			?>
			                                    			<img src="images/useronline/1_2/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/1_2/<?php echo $file; ?>');" />
			                                    			<?php
			                                    			echo '</a>';
			                                    			echo '</td>';
			                                    			echo '</tr>';
			                                    		} else {
			                                    			$columnCount++;
			                                    			echo '<td>';
			                                    			echo '<a href="#" class="thumbnail">';
			                                    			?>
			                                    			<img src="images/useronline/1_2/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/1_2/<?php echo $file; ?>');" />
			                                    			<?php
			                                    			echo '</a>';
			                                    			echo '</td>';
			                                    		}
			                                    	}
			                                    }
			                                    if ($columnCount < 6) {
				                                    for ($columnCount; $columnCount < 6; $columnCount++) {
				                                    	echo '<td></td>';
				                                    }
				                                    echo '</tr>';
				                                } else {
				                                	echo '</tr>';
				                                }
			                                    closedir($dir);
			                                }
			                                //Feelings only upon level 3
			                                if ($_SESSION['user']->level() > 2) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Staff</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/3')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/3/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/3/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/3/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/3/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/3/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon lvl 4
			                                if ($_SESSION['user']->level() > 3) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Duty Supervisor</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/4')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/4/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/4/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/4/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/4/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/4/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon level 5
			                                if ($_SESSION['user']->level() > 4) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Supervisor</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/5')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/5/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/5/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/5/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/5/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/5/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon level 6
			                                if ($_SESSION['user']->level() > 5) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Duty Manager</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/6')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/6/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/6/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/6/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/6/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/6/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon level 7
			                                if ($_SESSION['user']->level() > 6) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Manager</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/7')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/7/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/7/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/7/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/7/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/7/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon level 8
			                                if ($_SESSION['user']->level() > 7) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Duty Office Manager</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/8')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/8/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/8/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/8/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/8/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/8/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                //Feelings only upon level 9
			                                if ($_SESSION['user']->level() > 8) {
			                                	?>
			                                	<tr>
			                                		<td><strong>Office Manager - Office King</strong></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                		<td></td>
			                                	</tr>
			                                	<?php
			                                	$nbFile = 0;
				                                $columnCount = 0;
				                                if ($dir = opendir('./images/useronline/9_10')) {
				                                    while (false !== ($file = readdir($dir))) {
				                                    	if ($file != '.' && $file != '..' && $file != '.listing') {
				                                    		list($width, $height) = getimagesize('images/useronline/9_10/' . $file);
                                                            if ($width > 50 && $height > 50) {
                                                                $width = 50;
                                                                $height = 50;
                                                            } else if ($width > 50) {
                                                                $width = 50;
                                                            } else if ($height > 50) {
                                                                $height = 50;
                                                            }
				                                    		$nbFile++;
				                                    		if ($columnCount == 0) {
				                                    			echo '<tr>';
				                                    		}
				                                    		if ($columnCount == 6) {
				                                    			$columnCount = 0;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/9_10/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/9_10/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    			echo '</tr>';
				                                    		} else {
				                                    			$columnCount++;
				                                    			echo '<td>';
				                                    			echo '<a href="#" class="thumbnail">';
				                                    			?>
				                                    			<img src="images/useronline/9_10/<?php echo $file; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" onclick="changeFeeling('<?php echo $_SESSION['user']->id(); ?>', 'images/useronline/9_10/<?php echo $file; ?>');" />
				                                    			<?php
				                                    			echo '</a>';
				                                    			echo '</td>';
				                                    		}
				                                    	}
				                                    }
				                                    if ($columnCount < 6) {
					                                    for ($columnCount; $columnCount < 6; $columnCount++) {
					                                    	echo '<td></td>';
					                                    }
					                                    echo '</tr>';
					                                } else {
					                                	echo '</tr>';
					                                }
				                                    closedir($dir);
				                                }
			                                }
			                                ?>
			                            </tbody>
			                        </table>
	                            </div>
	                        </div>
	                        <!-- /panel -->
	                    </div>
	                    <?php
                    	include('right.php');
                    	?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
            	<p>&copy; Studio-Solution.com 2015</p>
        	</footer>
        </div>
    	<!--/.fluid-container-->
        <script>
        	//Function called onclick to update the feeling of the user
        	function changeFeeling(id, feeling) {
				xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
			        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			            if (xmlhttp.responseText == "true") {
			            	alert("Your feeling has been successfully changed !");
			            	location.reload();
			            } else {
			            	alert("Your feeling failed to be changed !");
			            }
			        }
			    }
			    xmlhttp.open("POST","change_feeling_script.php",true);
			    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("id="+id+"&feeling="+feeling);
			}
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>