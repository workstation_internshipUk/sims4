<?php
include('check_cookies.php');
if (isset($_GET['id']) && isset($_GET['id_user']) && isset($_GET['type'])) {
	$_GET['id'] = (int) $_GET['id'];
	$_GET['id_user'] = (int) $_GET['id_user'];
	if ($_GET['id'] > 0 && $_GET['id_user'] > 0) {
		if ($_GET['type'] == "previous") {
			$answer = $conn->prepare('SELECT id FROM message WHERE id_receiver = ? AND id < ? ORDER BY id DESC');
			$answer->execute(array($_GET['id_user'], $_GET['id']));
			if ($data = $answer->fetch()) {
				header('Location: read_sim.php?id=' . $data['id']);
			} else {
				header('Location: read_sim.php?id=' . $_GET['id']);
			}
			$answer->closeCursor();
		} else {
			$answer = $conn->prepare('SELECT id FROM message WHERE id_receiver = ? AND id > ? ORDER BY id ASC');
			$answer->execute(array($_GET['id_user'], $_GET['id']));
			if ($data = $answer->fetch()) {
				header('Location: read_sim.php?id=' . $data['id']);
			} else {
				header('Location: read_sim.php?id=' . $_GET['id']);
			}
			$answer->closeCursor();
		}
	} else {
		echo '<script>window.close();</script>';
	}
} else {
	echo '<script>window.close();</script>';
}
if (isset ($conn)) {
	$conn = null;
}
?>