<script>
    //Konami Code listener
    if (window.addEventListener) {
        var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";
        window.addEventListener("keydown", function(e) {
            kkeys.push(e.keyCode);
            if (kkeys.toString().indexOf( konami ) >= 0) {
                var c = document.body.childNodes;
                for (i = 0; i < c.length; i++) { 
                    $(c[i].nodeName).toggle(2000);
                }
                kkeys = [];
            }
        }, true);
    }
</script>
<link href='http://fonts.googleapis.com/css?family=Play:700' rel='stylesheet' type='text/css'>

<style type="text/css">
    #clock {
        font-size:20px;
        font-family:'Play',Arial, Helvetica, sans-serif;
        margin-top:5%;
    }
</style>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.php" style="padding:0px;"><img src="images/stusio-s_transparent.png" height="50" width="100" alt="Studio-Solution" title="Studio-Solution"/></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <script>
                //Displays a clock
                setInterval(function() {
                    var date = new Date();
                    var hours;
                    var minutes;
                    var seconds;
                    if (date.getHours() < 10) {
                        hours = "0" + date.getHours();
                    } else {
                        hours = date.getHours();
                    }
                    if (date.getMinutes() < 10) {
                        minutes = "0" + date.getMinutes();
                    } else {
                        minutes = date.getMinutes();
                    }
                    if (date.getSeconds() < 10) {
                        seconds = "0" + date.getSeconds();
                    } else {
                        seconds = date.getSeconds();
                    }
                    document.getElementById("clock").innerHTML = hours + ":" + minutes + ":" + seconds;
                }, 1000);
            </script>
            <script>
                function toggleRight() {
                    $('#rightspan').stop(true,true);
                    $('#rightspan').toggle(1000);
                    if (document.getElementById('toggleright').className == "glyphicon glyphicon-minus") {
                        document.getElementById('toggleright').className = "glyphicon glyphicon-plus";
                    } else {
                        document.getElementById('toggleright').className = "glyphicon glyphicon-minus";
                    }
                }
            </script>
            <ul class="nav navbar-nav">
                <?php if (lookPermissions(0, 1) && (lookPermissions(6, 1) || lookPermissions(0, 6))) { ?>
                <li class="dropdown" id="navUsers">
                    <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a tabindex="-1" href="connection_logs.php">Connection logs</a>
                        </li>
                        <li>
                            <a tabindex="-1" href="shift_logs.php">Shift logs</a>
                        </li>
                        <?php if (lookPermissions(6,2)) { ?>
                        <li>
                            <a tabindex="-1" href="manage_ip.php">Manage IP Addresses</a>
                        </li>
                        <li>
                            <a tabindex="-1" href="logout_user.php">Logout users</a>
                        </li>
                        <?php if (lookPermissions(6,3)) { ?>
                        <li>
                            <a tabindex="-1" href="staff.php">User List</a>
                        </li>

                        <?php if (lookPermissions(6,4)) { ?>
                            
                        <li>
                            <a tabindex="-1" href="adduser.php">Add user</a>
                        </li>
                        <?php if (lookPermissions(6,5)) { ?>

                        <li>
                            <a tabindex="-1" href="http://www.studios92.co.uk/sims3/barclays/pdq_light_studio-solution_Feb2013.html">EPDQ for Studio-Solution</a>
                        </li>

                        <?php }}}} ?>
                    </ul>
                </li>
                <?php } ?>
                <li class="dropdown" id="navLinks">
                    <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Links <i class="caret"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a tabindex="-1" target="_blank" href="http://www.studios92.co.uk/sims3/mysims/links/alphabet.htm">Phonetical alphabet</a>
                        </li>
                        <li>
                            <a tabindex="-1" target="_blank" href="http://www.timeticker.com/">World time zones</a>
                        </li>
                        <li>
                            <a tabindex="-1" target="_blank" href="http://dictionary.cambridge.org/">Dictionary</a>
                        </li>
                        <li>
                            <a tabindex="-1" target="_blank" href="http://www.xe.com/">Currency converter</a>
                        </li>
                    </ul>
                </li>
                <?php if ($_SESSION['user']->name() == "Dino" || $_SESSION['user']->name() == "BenoitT") { ?>
                <li class="dropdown" id="dinoAccess">
                    <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Dino <i class="caret"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a tabindex="-1" href="communication_flows.php">Communication Flows</a>
                        </li>
                        <li>
                            <a tabindex="-1" href="deleted_user.php">Deleted Users</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <li style="padding-top:12px; padding-left:15px">
                    <strong>Your IP Address is <?php echo $_SERVER['REMOTE_ADDR']; ?></strong>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li style="margin-top:15px;margin-right:15px;">
                    <span class="glyphicon glyphicon-minus" id="toggleright" onclick="toggleRight();"></span>
                </li>
                    
                <li id="clock">
                </li>
                <li class="dropdown">
                    <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" style="padding:0px; margin-left:10px">
                        <img style="max-height:60px; max-width:60px;" src="<?php if ($_SESSION['user']->picture() != '') { echo $_SESSION['user']->picture(); $cut = explode('/', $_SESSION['user']->picture()); $alt = substr($cut[2], 0, -4); } else { echo 'images/userpicture/default.gif'; $alt = 'default'; } ?>" alt="<?php if (isset($alt)) { echo $alt; } ?>" title="<?php if (isset($alt)) { echo $alt; } ?>" /> <?php echo $_SESSION["user"]->name(); ?> <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a tabindex="-1" href="settings.php">Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a tabindex="-1" href="disconnection.php">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2" id="sidebar">
            <ul class="nav nav-pills nav-stacked">

            <!-- HOME -->
                <li id="leftHome">
                    <a href="home.php"><i class="icon-chevron-right" id="navHomeIcon"></i> Home</a>
                    <ul class="dropdown-nav forAnimate" role='nav' id='navHomeDown' style='display: none;'>
                        <li><a href="http://studios92.co.uk/SIMS3/mysims/presentation/presentations.html" target="_blank">Presentation</a></li>
                        <li><a onclick="document.getElementById('quizForm').submit();" style="cursor:pointer;">Quiz</a></li>
                        <li><a href="userList.php">User List</a></li> 
                        <li><a href="users_extra_hours.php">Users' Total Extra Hours</a></li>
                        <li><a href="every_extra_hours.php">Every Extra Hours</a></li>
                        <li><a href="calendar.php">Events Calendar</a></li>
                        <li><a onclick="document.getElementById('seo_form').submit();" style="cursor:pointer;" >SEO database</a></li>
                    </ul>
                </li>
                <!-- SIMS -->
                <li id="leftMysims">
                    <a href="#"><i class="icon-chevron-right" id="navMysimsIcon" ></i>MySims</a>
                    <ul class="dropdown-nav forAnimate" role='nav' id='navMysimsDown' style='display: none;'>
                        <li><a href="inbox.php">Inbox</a></li>
                        <li><a href="shiftlist.php">Shiftlist</a></li>
                        <?php if (lookPermissions(0, 3)) { ?>
                        <li><a href="extra_hours.php">Add Extra hours</a></li>
                        <?php } else { ?>
                        <li><a href="#" onclick="alert('You need to be at least level 3 to go there !');">Add Extra hours</a></li>  
                        <?php } ?>
                    </ul>
                </li>

                <!-- CRM -->
                <?php if (lookPermissions(0, 1) && lookPermissions(3, 1)) { ?>
                <li id="leftCrm">
                    <a href="#" ><i class="icon-chevron-right" id="navCrmIcon"></i>CRM</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navCrmDown" style="display: none;">
                        <li><a onclick="document.getElementById('CrmWorkstation').submit();" style="cursor:pointer;" target='_blank'>CRM workstation</a></li>
                        <li><a href='http://studios92.co.uk/sims3/partners/newsearch.asp?site=acc_view.asp' target='_blank'>Search accommodation</a></li>
                        <li><a href='http://studios92.co.uk/SIMS3/barclays/pdq_mpi_Traveltura.html' target='_blank'>Traveltura Banking</a></li>
                        <li><a onclick="document.getElementById('MarketingWorkstation').submit();" style="cursor:pointer;" >Marketing database</a></li>
                        <li><a onclick="document.getElementById('UniversitiesWorkstation').submit();" style="cursor:pointer;" >Universities database</a></li>
                    </ul>
                </li>
                <?php } else { ?>
                <li id="leftCrm">
                    <a href="#" ><i class="icon-chevron-right" id="navCrmIcon"></i>CRM</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navCrmDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">CRM workstation</a></li>
                        <li><a href='#' onclick="alert('You don\'t have enough rights to go there !');">Search accommodation</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- HR -->
                <?php if (lookPermissions(0, 1) && lookPermissions(1, 1)) { ?>
                <li id="leftHr">
                    <a href="#" ><i class="icon-chevron-right" id="navHrIcon"></i>HR</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navHrDown" style="display: none;">
                        <li><a onclick="document.getElementById('HrWorkstation').submit();" style="cursor:pointer;" >HR workstation</a></li>
                        <li><a href='hr_calendar.php' target="_blank">HR calendar</a></li>
                    </ul>

                </li>
                <?php } else { ?>
                <li id="leftHr">
                    <a href="#" ><i class="icon-chevron-right" id="navHrIcon"></i>Hr</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navHrDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">HR workstation</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- Translation -->
                <?php if (lookPermissions(0, 1) && lookPermissions(2, 1)) { ?>
                <li id="leftTranslation">
                    <a href="#" ><i class="icon-chevron-right" id="navTranslationIcon"></i>Translation</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navTranslationDown" style="display: none;">
                        <li><a onclick="#" style="cursor:pointer;" >Translation workstation ?</a></li>
                    </ul>
                </li>
                <?php } else { ?>
                <li id="leftTranslation">
                    <a href="#" ><i class="icon-chevron-right" id="navTranslationIcon"></i>Translation</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navTranslationDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Translation workstation ?</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- IT -->
                <?php if (lookPermissions(0, 1) && lookPermissions(4, 1)) { ?>
                <li id="leftIt">
                    <a href="#" ><i class="icon-chevron-right" id="navItIcon"></i>IT</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navItDown" style="display: none;">
                        <li><a onclick="document.getElementById('ItWorkstation').submit();" style="cursor:pointer;" >IT workstation</a></li>
                    </ul>
                </li>
                <?php } else { ?>
                <li id="leftIt">
                    <a href="#" ><i class="icon-chevron-right" id="navItIcon"></i>IT</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navItDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">IT workstation</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- MARKETING -->
                <?php if (lookPermissions(0, 1) && lookPermissions(5, 1)) { ?>
                <li id="leftMarketing">
                    <a href="#" ><i class="icon-chevron-right" id="navMarketingIcon"></i>Marketing</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navMarketingDown" style="display: none;">
                        <li><a onclick="document.getElementById('MarketingWorkstation').submit();" style="cursor:pointer;" >Marketing database</a></li>
                        <li><a onclick="document.getElementById('UniversitiesWorkstation').submit();" style="cursor:pointer;" >Universities database</a></li>
                        <li><a href="http://www.traveltura.com/admin" target="_blank">Admin Traveltura</a></li>
                        <li><a href="https://www.youtube.com/user/TravelTura/videos" target="_blank">YouTube : Traveltura Videos</a></li>
                        <li><a href="https://www.youtube.com/user/UKInternship" target="_blank">YouTube : Internship-UK Videos</a></li>
                    </ul>
                </li>
                <?php } else { ?>
                <li id="leftMarketing">
                    <a href="#" ><i class="icon-chevron-right" id="navMarketingIcon"></i>Marketing</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navMarketingDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Marketing workstation ?</a></li>
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Universities</a></li>
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Admin Traveltura</a></li>
                        <li><a href="https://www.youtube.com/user/TravelTura/videos" target="_blank">YouTube : Traveltura Videos</a></li>
                        <li><a href="https://www.youtube.com/user/UKInternship" target="_blank">YouTube : Internship-UK Videos</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- SETTINGS -->
                <?php if (lookPermissions(0, 1)) { ?>
                <li id="leftSettings">
                    <a href="settings.php" ><i class="icon-chevron-right" id="navSettingsIcon"></i>Settings</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navSettingsDown" style="display: none;">
                        <li><a href="change_pass.php">Change password</a></li>
                        <li><a href="change_picture.php">Change picture</a></li>
                        <li><a href="change_feeling.php">Change feeling</a></li>
                    </ul>
                </li>
                <?php } else { ?>
                <li id="leftSettings">
                    <a href="#" ><i class="icon-chevron-right" id="navSettingsIcon"></i>Settings</a>
                    <ul class="dropdown-nav forAnimate" role="nav" id="navSettingsDown" style="display: none;">
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Change password</a></li>
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Change picture</a></li>
                        <li><a href="#" onclick="alert('You don\'t have enough rights to go there !');">Change feeling</a></li>
                    </ul>
                </li>
                <?php } ?>
                <!-- Following here are the post used in some links -->

                <!-- Access to the Crm workstation -->
                <form name="CrmWorkstation" id="CrmWorkstation" target="_blank" action="http://www.traveltura.net/crm/admin/workstation/index.php" method="post">
                    <input type="hidden" name="sims3name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!-- Access to the IT workstation -->
                <form name="ItWorkstation" id="ItWorkstation" target="_blank" action="http://internship-uk.com/it_workstation/dist/index.php" method="post">
                    <input type="hidden" name="sims3name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!-- Access to the Universities workstation -->
                <form name="UniversitiesWorkstation" id="UniversitiesWorkstation" target="_blank" action="http://www.internship-uk.com/workstationUniversities/pages/index.php" method="post">
                    <input type="hidden" name="name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!-- Access to the Marketing workstation -->
                <form name="MarketingWorkstation" id="MarketingWorkstation" target="_blank" action="http://www.internship-uk.com/workstationmarketing/pages/index.php" method="post">
                    <input type="hidden" name="name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!-- Access to the HR workstation -->
                <form name="MarketingWorkstation" id="HrWorkstation" target="_blank" action="http://internship-uk.com/workstation/pages/index.php" method="post">
                    <input type="hidden" name="sims3name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!-- Access to the quizzes -->
                <form name="quizForm" id="quizForm" target="_blank" action="http://quiz.internship-uk.com/Quiz/index.php" method="post">
                    <input type="hidden" name="iuk" value="true">
                    <input type="hidden" name="position" value="1">
                    <input type="hidden" name="fromiuk" value="1">
                    <input type="hidden" name="name" value="<?php echo $_SESSION['user']->name();?>">
                </form>

                <!--Access to the seo database -->
                <form target="_blank" action="http://www.traveltura.co.uk/seo/" method="post" name="seo_form" id="seo_form">
                    <input type="hidden" name="sims3" value="true">
                    <input type="hidden" name="name" value="<?php echo $_SESSION['user']->name();?>">
                </form>
            </ul>
        </div>

        <script type="text/javascript">
            //display of the menu according to the actual page ($selected)
            (function () {
                switch("<?php echo $selected?>") {
                    
                    case "0":
                        break;
                    case "Home":
                        document.getElementById('leftHome').className = "dropdown active";
                            document.getElementById('navHomeDown').style.display = "";
                        document.getElementById('navHomeIcon').className = "icon-chevron-down";
                        break;
                    case "Settings":

                        if (!!document.getElementById('navSettings')) {
                            document.getElementById('navSettings').className = "dropdown active";
                        }
                        if (!!document.getElementById('leftSettings')) {
                            document.getElementById('leftSettings').className = "active";
                            document.getElementById('navSettingsDown').style.display = "";
                            document.getElementById('navSettingsIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "Users":
                        if (!!document.getElementById('navUsers')) {
                            document.getElementById('navUsers').className = "dropdown active";
                        }
                        break;
                    case "Mysims":
                        if (!!document.getElementById('leftMysims')) {
                            document.getElementById('leftMysims').className = "dropdown active";
                            document.getElementById('navMysimsDown').style.display = "";
                            document.getElementById('navMysimsIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "Hr":
                        if (!!document.getElementById('leftHr')) {
                            document.getElementById('leftHr').className = "active";
                            document.getElementById('navHrIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "Translation":
                        if (!!document.getElementById('leftTranslation')) {
                            document.getElementById('leftTranslation').className = "active";
                            document.getElementById('navTranslationIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "Marketing":
                        if (!!document.getElementById('leftMarketing')) {
                            document.getElementById('leftMarketing').className = "active";
                            document.getElementById('navMarketingIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "Crm":
                        if (!!document.getElementById('leftCrm')) {
                            document.getElementById('leftCrm').className = "active";
                            document.getElementById('navCrmIcon').className = "icon-chevron-down";
                        }
                        break;

                    case "It":
                        if (!!document.getElementById('leftIt')) {
                            document.getElementById('leftIt').className = "active";
                            document.getElementById('navItIcon').className = "icon-chevron-down";
                        }
                        break;
                    default:
                }
            })();

            ////make the submenu appear/disappear
            function togglenav() {
                if (this.getElementsByTagName('ul')['0'].style.display == 'none') {
                    this.getElementsByTagName('ul')['0'].style.display = '';
                    this.getElementsByTagName('i')['0'].className = 'icon-chevron-down';
                } else {
                    this.getElementsByTagName('ul')['0'].style.display = 'none';
                    this.getElementsByTagName('i')['0'].className = 'icon-chevron-right';
                }
            }

            //make the submenu appear
            function displaynav() {
                <?php 
                if(isset($_COOKIE['changemenucookie'])){
                    if($_COOKIE['changemenucookie'] != 'dynamicex'){
                        echo "$(this).find('ul').stop(true,true);";
                    }
                }
                ?>
                $(this).find('ul').slideDown();
                this.getElementsByTagName('i')['0'].className = 'icon-chevron-down';
            }
            //make the submenu disappear
            function hidenav() {
                $(this).find('ul').slideUp();
                this.getElementsByTagName('i')['0'].className = 'icon-chevron-right';
            }

            //gestion of events on the leftmenu
            if (!!document.getElementById('leftSettings')) {  
                if('<?php echo $selected;?>' !== "Settings"){
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftSettings').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftSettings').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftSettings').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftSettings').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftTranslation')) {
                if ('<?php echo $selected;?>' !== "Translation") {
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftTranslation').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftTranslation').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftTranslation').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftTranslation').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftMysims')) {
                if('<?php echo $selected;?>' !== "Mysims"){
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftMysims').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftMysims').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftMysims').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftMysims').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftHome')) {
                if('<?php echo $selected;?>' !== "Home"){
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftHome').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftHome').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftHome').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftHome').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftMarketing')) {
                if('<?php echo $selected;?>' !== "Marketing"){
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftMarketing').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftMarketing').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftMarketing').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftMarketing').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftCrm')) {
                if('<?php echo $selected;?>' !== "Crm"){
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftCrm').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftCrm').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftCrm').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftCrm').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftHr')) {
                if ('<?php echo $selected;?>' !== "Hr") {
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftHr').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftHr').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftHr').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftHr').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }

            if (!!document.getElementById('leftIt')) {
                if ('<?php echo $selected;?>' !== "It") {
                    <?php if(isset($_COOKIE['changemenucookie'])){
                        if($_COOKIE['changemenucookie'] != 'static'){
                            echo "document.getElementById('leftIt').addEventListener('mouseenter',displaynav);
                            document.getElementById('leftIt').addEventListener('mouseleave',hidenav);";
                        }else{
                            echo "document.getElementById('leftIt').addEventListener('click',togglenav);";
                        }
                    }else{
                        echo "document.getElementById('leftIt').addEventListener('click',togglenav);";
                    }
                    ?>
                }
            }
        </script>
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <?php
        if (isset($_COOKIE['color']) && !(isset($_COOKIE['backgroundimg']))) {
            ?>
            <script>
                $(document).ready(function() {
                    var pagename = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
                    if (pagename == "calendar.php") { 
                        $.noConflict();
                        var color = "<?php echo $_COOKIE['color']; ?>";
                        jQuery(".panel-heading").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        jQuery(".container-fluid").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        jQuery("body").css("background-color",color);
                    } else {
                        var color = "<?php echo $_COOKIE['color']; ?>";
                        $(".panel-heading").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        $(".navbar-fixed-top").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        $(".container-fluid").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        //$("body").css("background-color",color);
                    };
                });
            </script>
            <?php
        } else if (isset($_COOKIE['backgroundimg']) && !(isset($_COOKIE['color']))) {
            ?>
            <script>
                $(document).ready(function() {
                    var image = "<?php echo $_COOKIE['backgroundimg']; ?>";
                    $("body").css('background-image', 'url(images/wallpaper/' + image + ')');
                });
            </script>
            <?php
        } else if (isset($_COOKIE['color']) && isset($_COOKIE['backgroundimg'])) {
            ?>
            <script>
                $(document).ready(function() {
                    var pagename = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
                    if (pagename == "calendar.php") { 
                        $.noConflict();
                        var color = "<?php echo $_COOKIE['color']; ?>";
                        jQuery(".panel-heading").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        jQuery(".container-fluid").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        jQuery("body").css("background-color",color);
                    } else {
                        var color = "<?php echo $_COOKIE['color']; ?>";
                        var image = "<?php echo $_COOKIE['backgroundimg']; ?>";
                        $(".panel-heading").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        $(".navbar-fixed-top").css("background-image","linear-gradient(to bottom, #FFF," + color);
                        $("body").css('background-image', 'url(images/wallpaper/' + image + ')');
                    };
                });
            </script>
            <?php
        }
        ?>