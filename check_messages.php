<?php
include_once('connect_db.php');
$answer0 = $conn->prepare('SELECT last_used FROM secure_logs WHERE name = ?');
$answer0->execute(array("check_messages"));
if ($data0 = $answer0->fetch()) {
	//Only delete the old messages once per day
	if ($data0['last_used'] < date("Y-m-d")) {
		$isDeleteDateMessage = date("Y-m-d", time() - (3 * 24 * 60 * 60));
		$isNotDeleteDateMessage = date("Y-m-d", time() - (15 * 24 * 60 * 60));
		$del = $conn->prepare('DELETE FROM message WHERE (send_date < ? AND is_deleted = 0 AND is_saved = 0) OR (send_date < ? AND is_deleted = 1)');
		$del->execute(array($isNotDeleteDateMessage, $isDeleteDateMessage));
		$mod = $conn->prepare('UPDATE secure_logs SET last_used = ? WHERE name = ?');
		$mod->execute(array(date("Y-m-d"),"check_messages"));
		echo 'true';
	} else {
		echo 'true';
	}
} else {
	echo 'false';
}
$answer0->closeCursor();
if (isset($conn)) {
    $conn = null;
}
?>