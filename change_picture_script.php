<?php
include_once('connect_db.php');
if (isset($_POST['id']) && isset($_POST['picture'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$answer = $conn->prepare('UPDATE user SET picture = ? WHERE id = ?');
		$answer->execute(array(htmlspecialchars($_POST['picture']),$_POST['id']));
		echo 'true';
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
    $conn = null;
}
?>