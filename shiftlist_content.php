<?php
include_once('check_cookies.php');
if (isset($_POST['year']) && isset($_POST['week']) && isset($_POST['id'])) {
	$_POST['year'] = (int) $_POST['year'];
	$_POST['week'] = (int) $_POST['week'];
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['year'] > 2000 && $_POST['week'] >= 0 && $_POST['id'] > 0) {
		?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><button class="btn btn-default" onclick="writeWeek(<?php
						if ($_POST['week'] > 1) {
							echo $_POST['year'] . ',' . ($_POST['week'] - 1) . ',' . $_POST['id'];
						} else {
							//If the year has 53 weeks
							if (date("D", strtotime($_POST['year'] . '01-01')) == "Fri" || date("D", strtotime($_POST['year'] . '01-01')) == "Sat" || date("D", strtotime($_POST['year'] . '01-01')) == "Sun") {
								echo ($_POST['year'] - 1) . ',53,' . $_POST['id'];
							} else {
								echo ($_POST['year'] - 1) . ',52,' . $_POST['id'];
							}
						}
						?>)">
					<i class="glyphicon glyphicon-step-backward"></i></button></th>
					<?php
					seeWeek($_POST['year'], $_POST['week']);
					?>
					<th><button class="btn btn-default" onClick="writeWeek(<?php
						if ($_POST['week'] < 52) {
							echo $_POST['year'] . ',' . ($_POST['week'] + 1) . ',' . $_POST['id'];
						} else if ($_POST['week'] == 52) {
							//If the year has 53 weeks
							if (date("D", strtotime(($_POST['year'] + 1) . '01-01')) == "Fri" || date("D", strtotime($_POST['year'] . '01-01')) == "Sat" || date("D", strtotime($_POST['year'] . '01-01')) == "Sun") {
								echo $_POST['year'] . ',' . ($_POST['week'] + 1) . ',' . $_POST['id'];
							} else {
								echo ($_POST['year'] + 1) . ',1,' . $_POST['id'];
							}
						} else {
							echo ($_POST['year'] + 1) . ',1,' . $_POST['id'];
						}
						?>)">
					<i class="glyphicon glyphicon-step-forward"></i></button></th>
				</tr>
			</thead>
			<tbody>
			<?php
			if (isset($_POST['showall'])) {
				if ($_POST['showall'] == "true") {
					$answer0 = $conn->query('SELECT id, name, deleted, id_group FROM user ORDER BY id_group ASC, name ASC');
				} else {
					$answer0 = $conn->prepare('SELECT id, name, deleted, id_group FROM user WHERE id_group = ? ORDER BY id_group ASC, name ASC');
					$answer0->execute(array($_SESSION['user']->id_group()));
				}
			} else {
				$answer0 = $conn->prepare('SELECT id, name, deleted, id_group FROM user WHERE id_group = ? ORDER BY id_group ASC, name ASC');
				$answer0->execute(array($_SESSION['user']->id_group()));
			}
			$id_group = 0;
			while ($data0 = $answer0->fetch()) {
				if ($data0['id_group'] != $id_group) {
					$id_group = $data0['id_group'];
					if ($id_group == 1) {
						echo '<tr class="text-primary">';
							echo '<td><strong>Recruitment</strong></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';
					} else if ($id_group == 2) {
						echo '<tr class="text-primary">';
							echo '<td><strong>Translation</strong></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';
					} else if ($id_group == 3) {
						echo '<tr class="text-primary">';
							echo '<td><strong>CRM</strong></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';
					} else if ($id_group == 4) {
						echo '<tr class="text-primary">';
							echo '<td><strong>IT</strong></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';
					} else if ($id_group == 5) {
						echo '<tr class="text-primary">';
							echo '<td><strong>Marketing</strong></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';
					}
				}
				if ($data0['name'] != "Dino" && $data0['name'] != "TomNun" && $data0['name'] != "SebastienC" && $data0['name'] != "Cheryl1" && $data0['name'] != "Celine" && $data0['deleted'] != 1) {
					echo '<tr>';
						echo '<td>' . $data0['name'] . '</td>';
						$answer1 = $conn->prepare('SELECT * FROM shift_list WHERE id_user = ? AND year = ? AND week = ?');
						$answer1->execute(array($data0['id'], $_POST['year'], $_POST['week']));
						if ($data1 = $answer1->fetch()) {
							if (((($data0['id'] == $_POST['id']) || lookPermissions(0, 6)) && ((date("W") <= $_POST['week'] && date("Y") <= $_POST['year']) || date("Y") < $_POST['year'])) || $_SESSION['user']->name() == "BenoitT") {
								?>
								<form method="post" onsubmit="return changeShiftlist(<?php echo $_POST['year'] . ',' . $_POST["week"] . ',' . $data0["id"] . ',' . $_SESSION["user"]->level(); ?>)">
									<td>
										<select name="mon" class="form-control" id="mon<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['mon'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['mon'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['mon'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['mon'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['mon'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['mon'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['mon'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="tue" class="form-control" id="tue<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['tue'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['tue'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['tue'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['tue'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['tue'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['tue'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['tue'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="wed" class="form-control" id="wed<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['wed'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['wed'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['wed'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['wed'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['wed'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['wed'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['wed'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="thu" class="form-control" id="thu<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['thu'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['thu'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['thu'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['thu'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['thu'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['thu'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['thu'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="fri" class="form-control" id="fri<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['fri'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['fri'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['fri'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['fri'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['fri'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['fri'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['fri'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="sat" class="form-control" id="sat<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['sat'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['sat'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['sat'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['sat'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['sat'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['sat'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['sat'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<select name="sun" class="form-control" id="sun<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4" <?php if ($data1['sun'] == '4') { echo 'selected'; } ?>>-</option>
											<option value="1" <?php if ($data1['sun'] == '1') { echo 'selected'; } ?>>M</option>
											<option value="2" <?php if ($data1['sun'] == '2') { echo 'selected'; } ?>>E</option>
											<option value="3" <?php if ($data1['sun'] == '3') { echo 'selected'; } ?>>X</option>
											<option value="5" <?php if ($data1['sun'] == '5') { echo 'selected'; } ?>>F</option>
											<option value="6" <?php if ($data1['sun'] == '6') { echo 'selected'; } ?>>S</option>
											<option value="7" <?php if ($data1['sun'] == '7') { echo 'selected'; } ?>>C</option>
										</select>
									</td>
									<td>
										<button class="btn btn-primary" onclick="changeShiftlist(<?php echo $_POST['year'] . ',' . $_POST['week'] . ',' . $data0['id'] . ',' . $_SESSION["user"]->level(); ?>)">Change</button>
									</td>
								</form>
								<?php
							} else {
								echo '<td>' . getTypeSL($data1['mon']) . '</td>';
								echo '<td>' . getTypeSL($data1['tue']) . '</td>';
								echo '<td>' . getTypeSL($data1['wed']) . '</td>';
								echo '<td>' . getTypeSL($data1['thu']) . '</td>';
								echo '<td>' . getTypeSL($data1['fri']) . '</td>';
								echo '<td>' . getTypeSL($data1['sat']) . '</td>';
								echo '<td>' . getTypeSL($data1['sun']) . '</td>';
								echo '<td></td>';
							}
						} else {
							if ((($data0['id'] == $_POST['id']) || lookPermissions(0, 6)) && ((date("W") <= $_POST['week'] && date("Y") <= $_POST['year']) || date("Y") < $_POST['year'])) {
								?>
								<form method="post" action="#" onsubmit="return changeShiftlist(<?php echo $_POST['year'] . ',' . $_POST['week'] . ',' . $data0['id'] . ',' . $_SESSION["user"]->level(); ?>)">
									<td>
										<select name="mon" class="form-control" id="mon<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="tue" class="form-control" id="tue<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="wed" class="form-control" id="wed<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="thu" class="form-control" id="thu<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="fri" class="form-control" id="fri<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="sat" class="form-control" id="sat<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<select name="sun" class="form-control" id="sun<?php echo $data0['id']; ?>" style="width:auto">
											<option value="4">-</option>
											<option value="1">M</option>
											<option value="2">E</option>
											<option value="3">X</option>
											<option value="5">F</option>
											<option value="6">S</option>
											<option value="7">C</option>
										</select>
									</td>
									<td>
										<button class="btn btn-primary" onclick="changeShiftlist(<?php echo $_POST['year'] . ',' . $_POST['week'] . ',' . $data0['id'] . ',' . $_SESSION["user"]->level(); ?>)">Change</button>
									</td>
								</form>
								<?php
							} else {
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td>-</td>';
								echo '<td></td>';
							}
						}
						$answer1->closeCursor();
					echo '</tr>';
				}
			}
			$answer0->closeCursor();
			?>
			</tbody>
		</table>
		<?php
	} else {
		echo 'An error has occured !';
	}
} else {
	echo 'An error has occured !';
}
if (isset($conn)) {
	$conn = null;
}
?>