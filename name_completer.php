<?php
include_once('connect_db.php');
if (isset($_POST['name'])) {
	if ($_POST['name'] != "") {
		$answer = $conn->prepare('SELECT name FROM user WHERE name LIKE ? ORDER BY name');
		$answer->execute(array(htmlspecialchars($_POST['name']) . "%"));
		if ($data = $answer->fetch()) {
			echo $data['name'];
		} else {
			$answer->closeCursor();
			$answer = $conn->prepare('SELECT name FROM `group` WHERE name LIKE ? ORDER BY name');
			$answer->execute(array(htmlspecialchars($_POST['name']) . "%"));
			if ($data = $answer->fetch()) {
				echo $data['name'];
			} else {
				if (preg_match("#^a$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^al$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^all$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^t$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^to$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^to $#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^to a$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^to al$#i", htmlspecialchars($_POST['name']))
					|| preg_match("#^to all$#i", htmlspecialchars($_POST['name']))) {
					echo 'All';
				} else {
					echo 'false';
				}
			}
		}
		$answer->closeCursor();
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>