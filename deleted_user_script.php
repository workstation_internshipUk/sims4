<?php
include('connect_db.php');
if (isset($_POST['id']) && isset($_POST['type'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		if ($_POST['type'] == "delete") {
			$del = $conn->prepare('DELETE FROM user WHERE id = ?');
			$del->execute(array($_POST['id']));
			echo 'true';
		} else if ($_POST['type'] == "restaure") {
			$mod = $conn->prepare('UPDATE user SET deleted = 0 WHERE id = ?');
			$mod->execute(array($_POST['id']));
			echo 'true';
		} else {
			echo 'false';
		}
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>