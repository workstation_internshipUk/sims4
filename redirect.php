<?php
if (isset($_POST['page'])) {
	$_POST['page'] = (int) $_POST['page'];
	if ($_POST['page'] > 0) {
		header('Location: inbox.php?page=' . $_POST['page']);
	} else {
		header('Location: inbox.php');
	}
}
?>