<?php
if (isset($_POST["color_param"])) {
    setcookie('color',$_POST["color_param"], time() + (30 * 24 * 60 * 60), null, null, false, true);
    exit;
}
if (isset($_POST["image"])) {
    if ($_POST["image"] != "") {
        setcookie('backgroundimg',$_POST["image"], time() + (30 * 24 * 60 * 60), null, null, false, true);
    } else {
        setcookie('backgroundimg','', time() - 10, null, null, false, true);
    }
}
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Settings";
        require_once 'menu.php';

        function selectedgalite($arg1,$arg2){
            if(isset($arg2)){
                if($arg1 == $arg2){
                    echo "selected";
                }
            }
        }
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Personal Settings For <?php echo $_SESSION["user"]->name(); ?></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="btn-group-vertical" role="group" aria-label="Vertical button group">
                                                <button type="button" onclick="window.location='change_pass.php'" class="btn btn-primary" style="width:150px;">Change Password</button>
                                                <button type="button" onclick="window.location='change_picture.php'" class="btn btn-primary" style="width:150px;">Change Picture</button>
                                                <button type="button" onclick="window.location='change_feeling.php'" class="btn btn-primary" style="width:150px;">Change Feeling</button>
                                            </div>
                                            <br /><br />
                                        </div>
                                        <div class="col-md-3">
                                        	<?php
                                        	if ($_SESSION['user']->feeling() != "") {
                                        		$path = $_SESSION['user']->feeling();
                                        	} else {
                                        		$path = "images/useronline/1_2/online.gif";
                                        	}
                                        	?>
                                        	<br /><br />
                                            <img src="<?php echo $path; ?>" />
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <label class="control-label col-xs-6">Color theme</label>
                                                <!-- spectrum colorPicker -->
                                                <link rel="stylesheet" type="text/css" href="http://bgrins.github.io/spectrum/spectrum.css">
                                                <script type="text/javascript" src="http://bgrins.github.com/spectrum/spectrum.js"></script>
                                                <div class="col-md-6">
                                                    <input type='text' class="my_color form-control" />
                                                </div>
                                                <script>
                                                    $(".my_color").spectrum({
                                                    //color: "rgb(242, 242, 242)",
                                                        showInput: true,
                                                        className: "full-spectrum",
                                                        showInitial: true,
                                                        showPalette: true,
                                                        showPaletteOnly: true,
                                                        showSelectionPalette: true,
                                                        maxSelectionSize: 10,
                                                        preferredFormat: "hex",
                                                       // localStorageKey: "spectrum.demo",
                                                        move: function (color) {
                                                    
                                                        },
                                                        /*  show: function () {
                                                        
                                                        },*/
                                                        beforeShow: function () {
                                                        
                                                        },
                                                        hide: function () {
                                                        
                                                        },
                                                        change: function(color) {
                                                            $(".navbar-inner").css("background-image","linear-gradient(to bottom, #FFF," + color);
                                                            $(".container-fluid").css("background-image","linear-gradient(to bottom, #FFF," + color);
                                                            jQuery("body").css("background-color",color);
                                                            $.ajax({async:false,
                                                                type: "POST",
                                                                data:"color_param="+ color, 
                                                                //success:function(result){alert('done');location.reload();},
                                                                error:function(exception) { alert('Exeption:' + exception); }
                                                            });
                                                        },
                                                        palette: [
                                                        ["rgb(242, 242, 242)"],
                                                        
                                                        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
                                                        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)"]
                                                        ]
                                                    });
                                                </script>
                                                <!-- spectrum colorPicker -->
                                            </div>
                                            <div class="row">
                                                <label class="control-label col-xs-6">Menu</label>
                                                <div class="col-md-4">
                                                    <select name="selectmenu" class="form-control" id="selectmenu">
                                                        <option value="static" <?php if(isset($_COOKIE['changemenucookie'])){if($_COOKIE['changemenucookie'] == 'static'){echo 'selected';}}?>>Static</option>
                                                        <option value="dynamic" <?php if(isset($_COOKIE['changemenucookie'])){if($_COOKIE['changemenucookie'] == 'dynamic'){echo 'selected';}}?>>Dynamic</option>
                                                        <option value="dynamicex" <?php if(isset($_COOKIE['changemenucookie'])){if($_COOKIE['changemenucookie'] == 'dynamicex'){echo 'selected';}}?>>DynamicEX</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <form method="post" action="settings.php">
                                                    <input type="hidden" name="image" value="<?php
                                                    if (!isset($_COOKIE['backgroundimg'])) {
                                                        $nbFile = 0;
                                                        $arr = array();
                                                        if ($dir = opendir('./images/wallpaper')) {
                                                            while (false !== ($file = readdir($dir))) {
                                                                if ($file != '.' && $file != '..' && $file != '.listing') {
                                                                    $arr[$nbFile] = $file;
                                                                    $nbFile++;
                                                                }
                                                            }
                                                        }
                                                        closedir($dir);
                                                        $rand = rand(0,$nbFile-1);
                                                        echo $arr[$rand];
                                                    }
                                                    ?>" />
                                                    <input type="submit" class="btn btn-primary" value="<?php if (isset($_COOKIE['backgroundimg'])) { echo 'Take it back'; } else { echo 'Give me a unicorn !'; } 
                                                    ?>" />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th><img src="images/userface/01_duty_trainee.gif" /></th>
                                                        <th><img src="images/userface/02_trainee.gif" /></th>
                                                        <th><img src="images/userface/03_staff.gif" /></th>
                                                        <th><img src="images/userface/04_duty_supervisor.gif" /></th>
                                                        <th><img src="images/userface/05_supervisor.gif" /></th>
                                                        <th><img src="images/userface/06_duty_manager.gif" /></th>
                                                        <th><img src="images/userface/07_manager.gif" /></th>
                                                        <th><img src="images/userface/08_duty_office_manager.gif" /></th>
                                                        <th><img src="images/userface/09_office_manager.gif" /></th>
                                                        <th><img src="images/userface/10_office_king.gif" /></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td<?php if ($_SESSION['user']->level() == 1) { echo ' class="text-primary"'; } ?>>Level 1</td>
                                                        <td<?php if ($_SESSION['user']->level() == 2) { echo ' class="text-primary"'; } ?>>Level 2</td>
                                                        <td<?php if ($_SESSION['user']->level() == 3) { echo ' class="text-primary"'; } ?>>Level 3</td>
                                                        <td<?php if ($_SESSION['user']->level() == 4) { echo ' class="text-primary"'; } ?>>Level 4</td>
                                                        <td<?php if ($_SESSION['user']->level() == 5) { echo ' class="text-primary"'; } ?>>Level 5</td>
                                                        <td<?php if ($_SESSION['user']->level() == 6) { echo ' class="text-primary"'; } ?>>Level 6</td>
                                                        <td<?php if ($_SESSION['user']->level() == 7) { echo ' class="text-primary"'; } ?>>Level 7</td>
                                                        <td<?php if ($_SESSION['user']->level() == 8) { echo ' class="text-primary"'; } ?>>Level 8</td>
                                                        <td<?php if ($_SESSION['user']->level() == 9) { echo ' class="text-primary"'; } ?>>Level 9</td>
                                                        <td<?php if ($_SESSION['user']->level() == 10) { echo ' class="text-primary"'; } ?>>Level 10</td>
                                                    </tr>
                                                    <tr>
                                                        <td<?php if ($_SESSION['user']->level() == 1) { echo ' class="text-primary"'; } ?>>Duty Trainee</td>
                                                        <td<?php if ($_SESSION['user']->level() == 2) { echo ' class="text-primary"'; } ?>>Trainee</td>
                                                        <td<?php if ($_SESSION['user']->level() == 3) { echo ' class="text-primary"'; } ?>>Staff</td>
                                                        <td<?php if ($_SESSION['user']->level() == 4) { echo ' class="text-primary"'; } ?>>Duty Supervisor</td>
                                                        <td<?php if ($_SESSION['user']->level() == 5) { echo ' class="text-primary"'; } ?>>Supervisor</td>
                                                        <td<?php if ($_SESSION['user']->level() == 6) { echo ' class="text-primary"'; } ?>>Duty Manager</td>
                                                        <td<?php if ($_SESSION['user']->level() == 7) { echo ' class="text-primary"'; } ?>>Manager</td>
                                                        <td<?php if ($_SESSION['user']->level() == 8) { echo ' class="text-primary"'; } ?>>Duty Office Manager</td>
                                                        <td<?php if ($_SESSION['user']->level() == 9) { echo ' class="text-primary"'; }?>>Office Manager</td>
                                                        <td<?php if ($_SESSION['user']->level() == 10) { echo ' class="text-primary"'; } ?>>Office King</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
            	<p>&copy; Studio-Solution.com 2015</p>
        	</footer>
        </div>
        <script type="text/javascript">
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + "; " + expires;
            }

            function changecookie(){
                setCookie('changemenucookie',document.getElementById('selectmenu').value,30);
            }
            document.getElementById('selectmenu').addEventListener("change",changecookie);
        </script>
    	<!--/.fluid-container-->
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>