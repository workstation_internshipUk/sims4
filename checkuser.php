<?php
//Check the data before adding new user
require_once "connect_db.php";
require_once "class/user.php";
require_once "class/usermanager.php";
if (isset($_POST["name"]) && isset($_POST["group"]) && isset($_POST["level"]) && isset($_POST["user-description"]) && isset($_POST["sdate"]) && isset($_POST["edate"]) && isset($_POST["special_days"]) && isset($_POST["hr_comment"])) {
	$name = htmlspecialchars($_POST["name"]);
	$group = (int) $_POST["group"];
	$level = (int) $_POST["level"];
	$user_description = htmlspecialchars($_POST["user-description"]);
	$sdate = htmlspecialchars($_POST["sdate"]);
	$edate = htmlspecialchars($_POST["edate"]);
	$special_days = (int) $_POST["special_days"];
	$hr_comment = htmlspecialchars($_POST["hr_comment"]);

	$sday = substr($sdate,0,2);
	$smonth = substr($sdate,3,2);
	$syear = substr($sdate,6,4);

	$eday = substr($edate,0,2);
	$emonth = substr($edate,3,2);
	$eyear = substr($edate,6,4);

	$sdate = date_create($syear . "-" . $smonth . "-" . $sday);
	$edate = date_create($eyear . "-" . $emonth . "-" . $eday);
	if (($sdate < $edate) && ($group > 0) && (($level > 0) && ($level < 11)) && (($special_days < 11) && ($special_days > -11))) {
		//Generating a salt
		$chain = "abcdefghijklmnopqrstuvwxyzABCDEFJKLMNOPQRSTUVWXYZ0123456789 ";
        $rand = rand(1, 50);
        $salt = "";
        for ($i = 0; $i < $rand; $i++) {
            $shuffled = str_shuffle($chain);
            $salt = $salt . substr($shuffled, 0, 1);
        }
		$user = new User([
			'name'=>$name,
			'password'=>sha1($salt . $name . date("Y")),
            'feeling'=>"",
            'picture'=>"",
            'level'=>$level,
            'salt'=>$salt,
            'id_group'=>$group,
            'start_date'=>$sdate->format('Y-m-d'),
            'end_date'=>$edate->format('Y-m-d'),
            'special_days'=>$special_days,
            'user_description'=>$user_description,
            'hr_comment'=>$hr_comment
			]);
		$umanager = new UserManager($conn);
		$umanager->add($user);
	} else {
		echo "false";
	}
} else {
	var_dump($_POST);
}
if (isset($conn)) {
	$conn = null;
}
?>