<?php
include_once('connect_db.php');
if (isset($_POST['id']) && isset($_POST['feeling'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$answer = $conn->prepare('UPDATE user SET feeling = ? WHERE id = ?');
		$answer->execute(array(htmlspecialchars($_POST['feeling']),$_POST['id']));
		echo 'true';
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
    $conn = null;
}
?>