//This file contains functions for the form validation.
/*
This function checks if the login time is between 07:45:00 and 20:59:00
return true if the login time is true
else print a error message;
*/
function checkHour() {
	var today = new Date();
	if (today.getHours() >= 8 && today.getHours() <= 20) {
		return true;
	} else if (today.getHours() == 7) {
		if (today.getMinutes() >= 45) {
			return true;
		}
		alert("You are not authorized to enter in the SIMS, because this is not an office time.");
		return false;
	} else {
		alert("You are not authorized to enter in the SIMS, because this is not an office time.");
		return false;
	}
}
//Function that checks the index form completion
function validate() {
	var form = document.getElementById("form1");
	var uname = document.getElementById("username").value;
	var upassword = document.getElementById("password").value;
	if (uname == null || uname == "") {
		alert("Please enter username");
		return false;
	}
	if(upassword == null || upassword == ""){
		alert("please enter your password");
		return false;
	}
	if(checkHour()){
		return true;
	}
	return false;
}
/*  jQuery ready function. Specify a function to execute when the DOM is fully loaded.  */
$(document).ready(
	/* This is the function that will get executed after the DOM is fully loaded */
	function () {
		$("#datepicker").datepicker({
			changeMonth: true,//this option for allowing user to select month
			changeYear: true //this option for allowing user to select from year range
		});
	}
);
//This function is called when a user is tiping a new password in order to check the strength.
function checkStrength() {
	var newPassword;
	if (document.getElementById('newpassword').value != "") {
		newPassword = document.getElementById('newpassword').value;
		if (newPassword.match(/[^a-zA-Z0-9_=+\/\\!?$%&<>,.;:\^@(){}[\]-]+/)) {
			document.getElementById('strengthimg').src = "images/passstrength/strength0.png";
		} else if (newPassword.match(/^[a-z]*$/) || newPassword.match(/^[0-9]*$/) || newPassword.match(/^[A-Z]*$/) || newPassword.match(/^[_=+\/\\!?$%&<>,.;:\^@-]*$/)) {
			document.getElementById('strengthimg').src = "images/passstrength/strength1.png";
		} else if (newPassword.match(/^[a-z0-9]{1,9}$/) || newPassword.match(/^[A-Z0-9]{1,9}$/) || newPassword.match(/^[a-zA-Z]{1,9}$/) || newPassword.match(/^[a-z_=+\/\\!?$%&<>,.;:\^@-]{1,9}$/) || newPassword.match(/^[A-Z_=+\/\\!?$%&<>,.;:\^@-]{1,9}$/) || newPassword.match(/^[0-9_=+\/\\!?$%&<>,.;:\^@-]{1,9}$/)) {
			document.getElementById('strengthimg').src = "images/passstrength/strength2.png";
		} else if (newPassword.match(/^[a-z0-9]{10,}$/) || newPassword.match(/^[A-Z0-9]{10,}$/) || newPassword.match(/^[a-zA-Z]{10,}$/) || newPassword.match(/^[a-z_=+\/\\!?$%&<>,.;:\^@-]{10,}$/) || newPassword.match(/^[A-Z_=+\/\\!?$%&<>,.;:\^@-]{10,}$/) || newPassword.match(/^[0-9_=+\/\\!?$%&<>,.;:\^@-]{10,}$/)) {
			document.getElementById('strengthimg').src = "images/passstrength/strength3.png";
		} else if (newPassword.length < 6) {
			document.getElementById('strengthimg').src = "images/passstrength/strength2.png";
		} else if (newPassword.length < 10) {
			document.getElementById('strengthimg').src = "images/passstrength/strength3.png";
		} else {
			document.getElementById('strengthimg').src = "images/passstrength/strength4.png";
			return true;
		}
	} else {
		document.getElementById('strengthimg').src = "images/passstrength/strength0.png";
	}
	return false;
}
//This function is called when a user repeat his new password in order to check it.
function checkRepeat() {
	if ((document.getElementById('newpassword').value != "") && (document.getElementById('repeat').value != "")) {
		if (document.getElementById('newpassword').value == document.getElementById('repeat').value) {
			document.getElementById('repeatcheck').className = "text-success";
			document.getElementById('repeatcheck').innerHTML = "The new password and the repeat field are identicals.";
			document.getElementById('newpasscontrol').className = "form-group has-success";
			document.getElementById('repeatcontrol').className = "form-group has-success";
			return true;
		} else {
			document.getElementById('repeatcheck').className = "text-danger";
			document.getElementById('repeatcheck').innerHTML = "The new password and the repeat field are differents !";
			document.getElementById('newpasscontrol').className = "form-group has-error";
			document.getElementById('repeatcontrol').className = "form-group has-error";
			return false;
		}
	} else {
		document.getElementById('repeatcheck').innerHTML = "";
		document.getElementById('newpasscontrol').className = "form-group";
		document.getElementById('repeatcontrol').className = "form-group";
	}
	return false;
}
//This function is called onsubmit to check if the form can be sent.
function checkPassChange() {
	if (checkStrength()) {
		if (checkRepeat()) {
			return true;
		} else {
			alert('The two passwords are different !');
		}
	} else {
		alert('Your password needs to be "Best" !');
	}
	return false;
}
//Check the typed passwords and execute the password change if they are right.
function checkPassChangeAdmin() {
	if (checkPassChange()) {
		var pass = document.getElementById('repeat').value;
		var id = document.getElementById('userpass').value;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            	document.getElementById("success").style.display='block';
                document.getElementById("success").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("POST","update_user_admin.php",true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send("type=changepass&pass="+pass+"&id="+id);
    }
    return false;
}
//Function called when a user has to be deleted
function deleteUser() {
	var id = document.getElementById('userdelete').value;
	xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (xmlhttp.responseText == true) {
            	alert("The user has been deleted !");
            	location.reload();
            } else {
            	alert("The user has not been deleted");
            }
        }
    }
    xmlhttp.open("POST","update_user_admin.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("type=delete&id="+id);
}
//Easter egg
function hitByUnicorn() {
	if (Math.random() < 0.0001) {
		alert("You got hit by a unicorn !");
	}
}
//Function used to delete all the user's sims
function deleteSims(id) {
	if (confirm("Do you really want to delete all your sims ?")) {
		xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
        	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            	if (xmlhttp.responseText == "true") {
            		alert("Your sims have been deleted !");
            		location.reload();
            	} else {
            		alert("Your sims failed to be deleted !");
            	}
        	}
    	}
    	xmlhttp.open("POST","delete_sims.php",true);
    	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    	xmlhttp.send("id="+id);
    }
}
//Refresh every minute the right.php to check if there are new messages and to check who is connected.
function refresh() {
    setInterval(function() {
        $("#statuslist").load("status_list.php");
        $("#statusheader").load("status_header.php");
        $("#inboxbody").load("inbox_body.php");
        $("#inboxheader").load("inbox_header.php");
    }, 60000);
}
function checkInput(id) {
    if (document.getElementById(id + 'input').value == '') {
        document.getElementById(id + 'control').className = 'form-group has-error';
    } else {
        document.getElementById(id + 'control').className = 'form-group';
    }
}