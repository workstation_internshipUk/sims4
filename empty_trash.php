<?php
include('connect_db.php');
if (isset($_POST['id'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$del = $conn->prepare('DELETE FROM message WHERE id_receiver = ? AND is_deleted = 1');
		$del->execute(array($_POST['id']));
		echo 'true';
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>