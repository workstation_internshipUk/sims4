<?php
class UserManager{
  private $db; 
  private $success;

  public function __construct($db){
    $this->setDb($db);
  }
  //Add the user passed in parameters in the DataBase
  public function add(User $user) {
    
    if ($sql = $this->db->prepare("INSERT INTO user (name, password, feeling, picture, level, salt, id_group, start_date, end_date, special_days, user_description, hr_comment) 
      VALUES (:name, :password, :feeling, :picture, :level, :salt, :id_group, :start_date, :end_date, :special_days, :user_description, :hr_comment)")) {
      $sql->bindValue(":name",$user->name());
      $sql->bindValue(":password",$user->password());
      $sql->bindValue(":feeling",$user->feeling());
      $sql->bindValue(":picture",$user->picture());
      $sql->bindValue(":level",$user->level());
      $sql->bindValue(":salt",$user->salt());
      $sql->bindValue(":id_group",$user->id_group());
      $sql->bindValue(":start_date",$user->start_date());
      $sql->bindValue(":end_date",$user->end_date());
      $sql->bindValue(":special_days",$user->special_days());
      $sql->bindValue(":user_description",$user->user_description());
      $sql->bindValue(":hr_comment",$user->hr_comment());
      if ($sql->execute() === true) {
        header('Location: staff.php');     
      } else {
        //header('Location: home.php');
      }
    } else {
      print_r($this->db->errorInfo());
    }
    $user->hydrate(['id'=>$this->db->lastInsertId()]);
    //Add the basic permission for the new user
    if ($user->id_group != 7) {
      $add_permission = $this->db->prepare('INSERT INTO permissions (id_user, id_group, level) VALUES (?, ?, ?)');
      $add_permission->execute(array($user->id(), $user->id_group, 1));
    } else {
      $answer = $this->db->query('SELECT id FROM `group`');
      while ($data = $answer->fetch()) {
        $add_permission = $this->db->prepare('INSERT INTO permissions (id_user, id_group, level) VALUES (?, ?, ?)');
        $add_permission->execute(array($user->id(), $data['id'], 5));
      }
      $answer->closeCursor();
    }
  }

  public function delete($id) {
    //Set the deleted field of the user to 1
    $q = $this->db->prepare('UPDATE user SET deleted = 1 WHERE id = :id');
    $q->bindParam(":id",$id);
    if ($q->execute()) {
      $this->setSuccess(true);
    } else {
      $this->setSuccess(false);
    }
  }

  public function get($id) {
    $q = $this->db->prepare('SELECT * FROM user WHERE id = :id');
    $q->bindParam(":id",$id);
    $q->execute();
    $data = $q->fetch(PDO::FETCH_ASSOC);
    $user= new User($data);
    return $user;
  }

  public function getList($search, $department) {
    $users = [];
    if ($search == "" && $department == "") {
      $q = $this->db->prepare('SELECT id, name, start_date, end_date, id_group FROM user WHERE deleted = 0 ORDER BY name');
      $q->execute();
    } else if ($search != "" && $department == "") {
      $q = $this->db->prepare('SELECT id, name, start_date, end_date, id_group FROM user WHERE name LIKE ? AND deleted = 0 ORDER BY name');
      $q->execute(array('%' . $search . '%'));
    } else if ($search == "" && $department != "") {
      $q = $this->db->prepare('SELECT id, name, start_date, end_date, id_group FROM user WHERE id_group = ? AND deleted = 0 ORDER BY name');
      $q->execute(array($department));
    } else {
      $q = $this->db->prepare('SELECT id, name, start_date, end_date, id_group FROM user WHERE id_group = ? AND name LIKE ? AND deleted = 0 ORDER BY name');
      $q->execute(array($department, '%' . $search . '%'));
    }
    while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
      $users[] = new User($data);
    }
    return $users;
  }

  public function update(User $user) {
    $q = $this->db->prepare('UPDATE user SET level = :level, id_group = :id_group, start_date = :start_date, end_date = :end_date, special_days = :special_days, user_description = :user_description, hr_comment = :hr_comment WHERE id = :id');
    $q->bindValue(":id",$user->id());
    $q->bindValue(":level",$user->level());
    $q->bindValue(":id_group",$user->id_group());
    $q->bindValue(":start_date",$user->start_date());
    $q->bindValue(":end_date",$user->end_date());
    $q->bindValue(":special_days",$user->special_days());
    $q->bindValue(":user_description",$user->user_description());
    $q->bindValue(":hr_comment",$user->hr_comment());
    
    if ($q->execute()) {
      $user = $this->get($user->id());
      if ($user->id_group() == 7) {
        $del = $this->db->prepare('DELETE FROM permissions WHERE id_user = ?');
        $del->execute(array($user->id()));
        $answer = $this->db->query('SELECT id FROM `group`');
        while ($data = $answer->fetch()) {
          $add_permission = $this->db->prepare('INSERT INTO permissions (id_user, id_group, level) VALUES (?, ?, ?)');
          $add_permission->execute(array($user->id(), $data['id'], 5));
        }
        $answer->closeCursor();
      }
      $this->setSuccess(true);
      return $user;
    } else {
      $this->setSuccess(false);
    }
  }

  public function update_pass($id,$pass) {
    $user = new User([]);
    $user = $this->get($id);
    $chain = "abcdefghijklmnopqrstuvwxyzABCDEFJKLMNOPQRSTUVWXYZ0123456789 ";
    $rand = rand(1, 50);
    $salt = "";
    for ($i = 0; $i < $rand; $i++) {
      $shuffled = str_shuffle($chain);
      $salt = $salt . substr($shuffled, 0, 1);
    }
    $q = $this->db->prepare('UPDATE user SET password = :password, salt = :salt, pass_changed = 0 WHERE id = :id');
    $q->bindValue(':password',sha1($salt . $pass));
    $q->bindParam(':salt', $salt);
    $q->bindParam(':id',$id);
    if ($q->execute()) {
      $user = $this->get($user->id());
      $this->setSuccess(true);
    } else {
      $this->setSuccess(false);
    }
  }

  public function setDb($db) {
    $this->db = $db;
  }
  public function setSuccess($success) {
    $this->success = $success;
  }

  public function success() {
    return $this->success;
  }
}
?>