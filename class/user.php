<?php
class User{
	
	private $id;
	private $name;
	private $password;
	private $feeling;
	private $picture;
	private $level;
	private $salt;
	private $id_group;
	private $start_date;
	private $end_date;
	private $special_days;
	private $user_description;
	private $hr_comment;
	private $pass_changed;

	//Getters
	public function id() {
		return $this->id;
	}
	public function name() {
		return $this->name;
	}
	public function password() {
		return $this->password;
	}
	public function feeling() {
		return $this->feeling;
	}
	public function picture() {
		return $this->picture;
	}
	public function level() {
		return $this->level;
	}
	public function salt() {
		return $this->salt;
	}
	public function id_group() {
		return $this->id_group;
	}
	public function start_date() {
		return $this->start_date;
	}
	public function end_date() {
		return $this->end_date;
	}
	public function special_days() {
		return $this->special_days;
	}
	public function user_description() {
		return $this->user_description;
	}
	public function hr_comment() {
		return $this->hr_comment;
	}
	public function pass_changed() {
		return $this->pass_changed;
	}

	//Setters
	public function setId($id) {
		$this->id=$id;
	}
	public function setName($name) {
		$this->name=$name;
	}
	public function setPassword($password) {
		$this->password=$password;
	}
	public function setFeeling($feeling) {
		$this->feeling=$feeling;
	}
	public function setPicture($picture) {
		$this->picture=$picture;
	}
	public function setLevel($level) {
		$this->level=$level;
	}
	public function setSalt($salt) {
		$this->salt=$salt;
	}
	public function setId_group($id_group) {
		$this->id_group=$id_group;
	}
	public function setStart_date($start_date) {
		$this->start_date=$start_date;
	}
	public function setEnd_date($end_date) {
		$this->end_date=$end_date;
	}
	public function setSpecial_days($special_days) {
		$this->special_days=$special_days;
	}
	public function setUser_description($user_description) {
		$this->user_description=$user_description;
	}
	public function setHr_comment($hr_comment) {
		$this->hr_comment=$hr_comment;
	}
	public function setPass_changed($pass_changed) {
		$this->pass_changed=$pass_changed;
	}

	public function hydrate(array $data){
  		foreach ($data as $key => $value){
   			$method = 'set'.ucfirst($key);   
   			if (method_exists($this, $method)){
   	  	      	$this->$method($value);
   			}
   		}
   	}

    function __construct(array $data){
   		$this->hydrate($data);
	}
}
?>