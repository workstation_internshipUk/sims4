<?php
include('check_cookies.php');
if (isset($_POST['type']) && isset($_POST['searchuser']) && isset($_POST['searchsubject']) && isset($_POST['page']) && isset($_POST['nbmess'])) {
    $_POST['page'] = (int) $_POST['page'];
    $_POST['nbmess'] = (int) $_POST['nbmess'];
    if ($_POST['page'] > 0 && $_POST['nbmess'] > 0) {
        $nbMessPerPage = $_POST['nbmess'];
        $firstMess = ($_POST['page'] - 1) * $nbMessPerPage;
        if ($_POST['type'] == 'inbox') {
            if ($_POST['searchuser'] != "") {
                if ($_POST['searchsubject'] != "") {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                } else {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                }
            } else if ($_POST['searchsubject'] != "") {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND subject LIKE ?');
                $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? AND subject LIKE ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
            } else {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ?');
                $countMessage->execute(array($_SESSION['user']->id()));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 0 AND is_deleted = 0 AND id_receiver = ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id()));
            }
        } else if ($_POST['type'] == 'archive') {
            if ($_POST['searchuser'] != "") {
                if ($_POST['searchsubject'] != "") {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 1 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 1 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                } else {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 1 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 1 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                }
            } else if ($_POST['searchsubject'] != "") {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 1 AND id_receiver = ? AND subject LIKE ?');
                $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 1 AND id_receiver = ? AND subject LIKE ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
            } else {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_saved = 1 AND id_receiver = ?');
                $countMessage->execute(array($_SESSION['user']->id()));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_saved = 1 AND id_receiver = ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id()));
            }
        } else if ($_POST['type'] == 'trash bin') {
            if ($_POST['searchuser'] != "") {
                if ($_POST['searchsubject'] != "") {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_deleted = 1 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_deleted = 1 AND id_receiver = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                } else {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_deleted = 1 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE is_deleted = 1 AND id_receiver = ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                }
            } else if ($_POST['searchsubject'] != "") {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_deleted = 1 AND id_receiver = ? AND subject LIKE ?');
                $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_deleted = 1 AND id_receiver = ? AND subject LIKE ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
            } else {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE is_deleted = 1 AND id_receiver = ?');
                $countMessage->execute(array($_SESSION['user']->id()));
                $answer = $conn->prepare('SELECT * FROM message WHERE is_deleted = 1 AND id_receiver = ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id()));
            }
        } else if ($_POST['type'] == 'outbox') {
            if ($_POST['searchuser'] != "") {
                if ($_POST['searchsubject'] != "") {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE id_sender = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE id_sender = ? AND subject LIKE ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                } else {
                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE id_sender = ? AND (sender LIKE ? OR receiver LIKE ?)');
                    $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                    $answer = $conn->prepare('SELECT * FROM message WHERE id_sender = ? AND (sender LIKE ? OR receiver LIKE ?) ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                    $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchuser']) . '%', '%' . htmlspecialchars($_POST['searchuser']) . '%'));
                }
            } else if ($_POST['searchsubject'] != "") {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE id_sender = ? AND subject LIKE ?');
                $countMessage->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
                $answer = $conn->prepare('SELECT * FROM message WHERE id_sender = ? AND subject LIKE ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id(), '%' . htmlspecialchars($_POST['searchsubject']) . '%'));
            } else {
                $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE id_sender = ?');
                $countMessage->execute(array($_SESSION['user']->id()));
                $answer = $conn->prepare('SELECT * FROM message WHERE id_sender = ? ORDER BY id DESC LIMIT ' . $firstMess . ', ' . $nbMessPerPage);
                $answer->execute(array($_SESSION['user']->id()));
            }
        }
        if (isset($countMessage)) {
            if ($count = $countMessage->fetch()) {
                if (isset($answer)) {
                    echo 'You have <strong>' . $count['count_message'] . '</strong> messages in your ' . $_POST['type'] . '.<br />';
                    $nbPages = ceil($count['count_message'] / $nbMessPerPage);
                    //Display all the pages
                    echo '<ul class="pagination">';
                    if ($nbPages < 20) {
                        for ($i = 1 ; $i <= $nbPages ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                    } else {
                        for ($i = 1 ; $i <= 5 ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                        echo '<li class="disabled"><a href="#">...</a></li>';
                        for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                        if ($_GET['page'] < 4 || $_GET['page'] > $nbPages-3) {
                            for ($i = 1 ; $i <= 5 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else if ($_GET['page'] != 4 && $_GET['page'] != $nbPages - 3) {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $_GET['page']-2 ; $i <= $_GET['page']+2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else if ($_GET['page'] == 4) {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            for ($i = $_GET['page']-1 ; $i <= $_GET['page']+2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $_GET['page']-2 ; $i <= $_GET['page']+1 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        }
                    }
                    echo '</ul>';
                    ?>
                    <br />
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <?php if ($_POST['type'] != "outbox") { echo '<th>Select</th>'; } ?>
                                <th></th>
                                <th>From</th>
                                <th>To</th>
                                <th>Subject</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Priority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            while ($data = $answer->fetch()) {
                                ?>
                                <tr>
                                    <?php if ($_POST['type'] != 'outbox') { ?><td><input type="checkbox" name="selectmessage" id="<?php echo $data['id']; ?>" onchange="checkboxHandler(<?php if ($_POST['type'] == 'archive') { echo '1,'; } else { echo '0,'; } if ($_POST['type'] == 'trash bin') { echo '1'; } else { echo '0'; } ?>);" /></td><?php } ?>
                                    <td><?php if ($_POST['type'] != 'outbox') { if ($data['is_read'] == 0) { echo '<strong style="color:red">New!</strong>'; } } else { if ($data['is_read'] == 0) { echo '<strong style="color:red">Not read</strong>'; } } ?></td>
                                    <td><?php echo '<a href="#" onclick="window.open(\'send_sim.php?name=' . $_SESSION['user']->name() . '&subject=' . $data['subject'] . '\',\'sendSim\', \'width=1250px,height=800px\').focus()" >' . $data['sender'] . '</a>'; ?></td>
                                    <td>
                                        <?php
                                        if ($_POST['type'] != 'outbox') {
                                            echo str_replace(',',', ',$data['receiver']);
                                        } else {
                                            $answer1 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                                            $answer1->execute(array($data['id_receiver']));
                                            if ($data1 = $answer1->fetch()) {
                                                echo $data1['name'];
                                            } else {
                                                echo str_replace(',',', ',$data['receiver']);
                                            }
                                        $answer1->closeCursor();
                                        }
                                    ?>
                                    </td>
                                    <td><?php if ($_POST['type'] != 'outbox') { echo '<a href="#" onClick="window.open(\'read_sim.php?id=' . $data['id'] . '\',\'mywindow\',\'width=600px,height=600px\').focus()">' . $data['subject'] . '</a>'; } else { echo '<a href="#" onClick="window.open(\'read_sim.php?sender=true&id=' . $data['id'] . '\',\'mywindow\',\'width=600px,height=600px\').focus()">' . $data['subject'] . '</a>'; } ?></td>
                                    <td><?php echo $data['send_date']; ?></td>
                                    <td><?php echo $data['send_time']; ?></td>
                                    <td>
                                        <?php
                                        if ($data['priority'] == "high") {
                                            echo '<strong class="text-danger">High</strong>';
                                        } else if ($data['priority'] == "normal") {
                                            echo 'Normal';
                                        } else {
                                            echo '<i class="text-warning">Low</i>';
                                        }
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    if ($_POST['type'] != 'outbox') {
                                        ?>
                                        <a href="#" onClick="window.open('read_sim.php?id=<?php echo $data['id']; ?>','mywindow','width=600px,height=600px').focus()"><img src="images/small_todo.gif" /></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="#" onClick="window.open('read_sim.php?sender=true&amp;id=<?php echo $data['id']; ?>','mywindow','width=600px,height=600px').focus()"><img src="images/small_todo.gif" /></a>
                                        <?php
                                        }
                                        if ($_POST['type'] != 'outbox') {
                                            if ($_POST['type'] == 'inbox') {
                                                ?>
                                                <a href="#"><img src="images/delete_tra.gif" onclick="checkboxTreat('delete',<?php echo $data['id']; ?>)" /></a>
                                                <a href="#"><img src="images/save.gif" onclick="checkboxTreat('save',<?php echo $data['id']; ?>)" /></a>
                                                <?php
                                            } else if ($_POST['type'] != 'trash bin') {
                                                ?>
                                                <a href="#"><img src="images/predef_change.gif" onclick="checkboxTreat('normal',<?php echo $data['id']; ?>)" /></a>
                                                <a href="#"><img src="images/save.gif" onclick="checkboxTreat('save',<?php echo $data['id']; ?>)" /></a>
                                                <?php
                                            } else if ($_POST['type'] != 'archive') {
                                                ?>
                                                <a href="#"><img src="images/delete_tra.gif" onclick="checkboxTreat('delete',<?php echo $data['id']; ?>)" /></a>
                                                <a href="#"><img src="images/predef_change.gif" onclick="checkboxTreat('normal',<?php echo $data['id']; ?>)" /></a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <button onclick="window.open('send_sim.php','sendSim', 'width=1250px,height=800px').focus()" class="btn btn-primary">New</button>
                    <?php
                    if ($_POST['type'] == 'inbox') {
                        ?>
                        <button class="btn btn-primary" type="button" id="buttondel" onclick="checkboxTreat('delete', 0);" disabled>Delete</button>
                        <button class="btn btn-primary" type="button" id="buttonsav" onclick="checkboxTreat('save', 0);" disabled>Save</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'outbox'; ?>', 1)">Outbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'trash bin'; ?>', 1)">Trash Bin</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'archive'; ?>', 1)">Saved Messages</button>
                        <?php
                    } else if ($_POST['type'] == 'archive') {
                        ?>
                        <button class="btn btn-primary" type="button" id="buttonnor" onclick="checkboxTreat('normal', 0);" disabled>Put to inbox</button>
                        <button class="btn btn-primary" type="button" id="buttondel" onclick="checkboxTreat('delete', 0);" disabled>Delete</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'inbox'; ?>', 1)">Inbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'outbox'; ?>', 1)">Outbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'trash bin'; ?>', 1)">Trash Bin</button>
                        <?php
                    } else if ($_POST['type'] == 'trash bin') {
                        ?>
                        <button class="btn btn-primary" type="button" id="buttonnor" onclick="checkboxTreat('normal', 0);" disabled>Put to inbox</button>
                        <button class="btn btn-primary" type="button" id="buttonsav" onclick="checkboxTreat('save', 0);" disabled>Save</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'inbox'; ?>', 1)">Inbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'outbox'; ?>', 1)">Outbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'archive'; ?>', 1)">Saved Messages</button>
                        <button class="btn btn-primary" type="button" onclick="emptyTrash(<?php echo $_SESSION['user']->id(); ?>);">Empty Trash Bin</button>
                        <?php
                    } else if ($_POST['type'] == 'outbox') {
                        ?>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'inbox'; ?>', 1)">Inbox</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'trash bin'; ?>', 1)">Trash Bin</button>
                        <button class="btn btn-primary" onclick="writeTable('<?php echo 'archive'; ?>', 1)">Saved Messages</button>
                        <?php
                    }
                    //Display all the pages
                    echo '<div class="row">';
                    echo '<div class="col-md-12">';
                    echo '<ul class="pagination">';
                    if ($nbPages < 20) {
                        for ($i = 1 ; $i <= $nbPages ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                    } else {
                        for ($i = 1 ; $i <= 5 ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                        echo '<li class="disabled"><a href="#">...</a></li>';
                        for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                            if ($i != $_POST['page']) {
                                echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            } else {
                                echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                            }
                        }
                        if ($_GET['page'] < 4 || $_GET['page'] > $nbPages-3) {
                            for ($i = 1 ; $i <= 5 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else if ($_GET['page'] != 4 && $_GET['page'] != $nbPages - 3) {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $_GET['page']-2 ; $i <= $_GET['page']+2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else if ($_GET['page'] == 4) {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            for ($i = $_GET['page']-1 ; $i <= $_GET['page']+2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        } else {
                            for ($i = 1 ; $i <= 2 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            echo '<li class="disabled"><a href="#">...</a></li>';
                            for ($i = $_GET['page']-2 ; $i <= $_GET['page']+1 ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                            for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                if ($i != $_POST['page']) {
                                    echo '<li><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                } else {
                                    echo '<li class="active"><a href="#" onclick="writeTable(\'' . $_POST['type'] . '\',' . $i . ');">' . $i . '</a></li>';
                                }
                            }
                        }
                    }
                    echo '</div>';
                    echo '</div>';
                } else {
                    echo 'Something unexpected happened :<br />';
                    echo ' - SQL query could not be made';
                }
            } else {
                echo 'Something unexpected happened :<br />';
                echo ' - SQL query could not be made';
            }
        } else {
            echo 'Something unexpected happened :<br />';
            echo ' - SQL query could not be made';
        }
    } else {
        echo 'Something unexpected happened :<br />';
        echo ' - Wrong page format : ' . $_POST['page'];
        echo ' - Or wrong number message format : ' . $_POST['nbmess'];
    }
} else {
    echo 'Something unexpected happened :<br />';
    if (!isset($_POST['type'])) {
        echo ' - Type is not set<br />';
    }
    if (!isset($_POST['searchuser'])) {
        echo ' - Search User is not set<br />';
    }
    if (!isset($_POST['searchsubject'])) {
        echo ' - Search Subject is not set<br />';
    }
    if (!isset($_POST['page'])) {
        echo ' - Page is not set<br />';
    }
}
if (isset($conn)) {
    $conn = null;
}
?>