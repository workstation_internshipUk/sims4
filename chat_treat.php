<?php
include_once('connect_db.php');
if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['message'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		if ($_POST['message'] != "") {
			$date = date("Y-m-d");
			$time = date("H:i:s");
			//Add the new message in the Database
			$add = $conn->prepare('INSERT INTO chat (id_user, name, message_date, message_time, message) VALUES (?, ?, ?, ?, ?)');
			$add->execute(array($_POST['id'], htmlspecialchars($_POST['name']), $date, $time, htmlspecialchars($_POST['message'])));
			echo 'done';
			//If there are more than 20 messages, then delete the oldest one.
			$countMessage = $conn->query('SELECT COUNT(*) AS count_message FROM chat');
			if ($count = $countMessage->fetch()) {
				if ($count['count_message'] > 20) {
					$answer = $conn->query('SELECT id FROM chat');
					if ($data = $answer->fetch()) {
						$delete = $conn->prepare('DELETE FROM chat WHERE id = ?');
						$delete->execute(array($data['id']));
					}
					$answer->closeCursor();
				}
			}
		}
	}
}
if (isset($conn)) {
    $conn = null;
}
?>