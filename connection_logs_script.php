                                    <?php
                                    include('connect_db.php');
                                    //Cases to generate the appropriate query
                                    $page = 1;
                                    if (isset($_POST['id'])) {
                                        $_POST['id'] = (int) $_POST['id'];
                                        if ($_POST['id'] > 0) {
                                            $countLogs = $conn->prepare('SELECT COUNT(*) AS count_logs FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ?');
                                            $countLogs->execute(array($_POST['id']));
                                            if ($count = $countLogs->fetch()) {
                                                $totalLogs = $count['count_logs'];
                                            } else {
                                                $totalLogs = 0;
                                            }
                                            $countLogs->closeCursor();
                                            $nbLogPerPage = 50;
                                            $nbPages = ceil($totalLogs / $nbLogPerPage);
                                            if (!isset($_GET['page'])) {
                                                if (isset($_POST['order'])) {
                                                    if ($_POST['order'] == "name") {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    } else if ($_POST['order'] == "connection") {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    } else if ($_POST['order'] == "disconnection") {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    } else {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    }
                                                } else {
                                                    $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                    $answer->execute(array($_POST['id']));
                                                }
                                            } else {
                                                $_GET['page'] = (int) $_GET['page'];
                                                if ($_GET['page'] > 0) {
                                                    $page = $_GET['page'];
                                                    $firstLog = ($_GET['page'] -1) * $nbLogPerPage;
                                                    if (isset($_POST['order'])) {
                                                        if ($_POST['order'] == "name") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "connection") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "disconnection") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        }
                                                    } else {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    }
                                                } else {
                                                    if (isset($_POST['order'])) {
                                                        if ($_POST['order'] == "name") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "connection") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "disconnection") {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? AND connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else {
                                                            $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        }
                                                    } else {
                                                        $answer = $conn->prepare('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.id_user = ? ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    }
                                                }
                                            }
                                        } else if (isset($_POST['order'])) {
                                            $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id');
                                            if ($count = $countLogs->fetch()) {
                                                $totalLogs = $count['count_logs'];
                                            } else {
                                                $totalLogs = 0;
                                            }
                                            $countLogs->closeCursor();
                                            $nbLogPerPage = 50;
                                            $nbPages = ceil($totalLogs / $nbLogPerPage);
                                            if (!isset($_GET['page'])) {
                                                if ($_POST['order'] == "name") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "connection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "disconnection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                }
                                            } else {
                                                $_GET['page'] = (int) $_GET['page'];
                                                if ($_GET['page'] > 0) {
                                                    $page = $_GET['page'];
                                                    $firstLog = ($_GET['page'] -1) * $nbLogPerPage;
                                                    if ($_POST['order'] == "name") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                    } else if ($_POST['order'] == "connection") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                    } else if ($_POST['order'] == "disconnection") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                    } else {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                    }
                                                } else {
                                                    if ($_POST['order'] == "name") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                    } else if ($_POST['order'] == "connection") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                    } else if ($_POST['order'] == "disconnection") {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                    } else {
                                                        $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                    }
                                                }
                                            }
                                        } else {
                                            $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id');
                                            if ($count = $countLogs->fetch()) {
                                                $totalLogs = $count['count_logs'];
                                            } else {
                                                $totalLogs = 0;
                                            }
                                            $countLogs->closeCursor();
                                            $nbLogPerPage = 50;
                                            $nbPages = ceil($totalLogs / $nbLogPerPage);
                                            if (!isset($_GET['page'])) {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            } else {
                                                $_GET['page'] = (int) $_GET['page'];
                                                if ($_GET['page'] > 0) {
                                                    $page = $_GET['page'];
                                                    $firstLog = ($_GET['page'] -1) * $nbLogPerPage;
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                } else {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                }
                                            }
                                        }
                                    } else if (isset($_POST['order'])) {
                                        $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id');
                                        if ($count = $countLogs->fetch()) {
                                            $totalLogs = $count['count_logs'];
                                        } else {
                                            $totalLogs = 0;
                                        }
                                        $countLogs->closeCursor();
                                        $nbLogPerPage = 50;
                                        $nbPages = ceil($totalLogs / $nbLogPerPage);
                                        if (!isset($_GET['page'])) {
                                            if ($_POST['order'] == "name") {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            } else if ($_POST['order'] == "connection") {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            } else if ($_POST['order'] == "disconnection") {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            } else {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            }
                                        } else {
                                            $_GET['page'] = (int) $_GET['page'];
                                            if ($_GET['page'] > 0) {
                                                $page = $_GET['page'];
                                                $firstLog = ($_GET['page'] -1) * $nbLogPerPage;
                                                if ($_POST['order'] == "name") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "connection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "disconnection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                } else {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                }
                                            } else {
                                                if ($_POST['order'] == "name") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY user.name ASC, connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "connection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "connection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "disconnection") {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id WHERE connection_logs.type = "disconnection" ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                } else {
                                                    $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                                }
                                            }
                                        }
                                    } else {
                                        $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id');
                                        if ($count = $countLogs->fetch()) {
                                            $totalLogs = $count['count_logs'];
                                        } else {
                                            $totalLogs = 0;
                                        }
                                        $countLogs->closeCursor();
                                        $nbLogPerPage = 50;
                                        $nbPages = ceil($totalLogs / $nbLogPerPage);
                                        if (!isset($_GET['page'])) {
                                            $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                        } else {
                                            $_GET['page'] = (int) $_GET['page'];
                                            if ($_GET['page'] > 0) {
                                                $page = $_GET['page'];
                                                $firstLog = ($_GET['page'] -1) * $nbLogPerPage;
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                            } else {
                                                $answer = $conn->query('SELECT connection_logs.id_user, connection_logs.type, connection_logs.connection_date, connection_logs.connection_time, connection_logs.ip_address, user.name FROM connection_logs INNER JOIN user ON connection_logs.id_user = user.id ORDER BY connection_logs.id DESC LIMIT ' . $nbLogPerPage);
                                            }
                                        }
                                    }
                                    //Display the different pages
                                    echo '<ul class="pagination">';
                                    if ($nbPages < 20) {
                                        for ($i = 1 ; $i <= $nbPages ; $i++) {
                                            if ($page != $i) {
                                                echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                            } else {
                                                echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                            }
                                        }
                                    } else {
                                        if (!isset($_GET['page'])) {
                                            for ($i = 1 ; $i <= 5 ; $i++) {
                                                if ($page != $i) {
                                                    echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                } else {
                                                    echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                }
                                            }
                                            echo '<li class="disabled"><a href="#">...</a></li>';
                                            for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                                if ($page != $i) {
                                                    echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                } else {
                                                    echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                }
                                            }
                                        } else {
                                            $_GET['page'] = (int) $_GET['page'];
                                            if ($_GET['page'] < 4 || $_GET['page'] > $nbPages-3) {
                                                for ($i = 1 ; $i <= 5 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else if ($_GET['page'] != 4 && $_GET['page'] != $nbPages - 3) {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $_GET['page']-2 ; $i <= $_GET['page']+2 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else if ($_GET['page'] == 4) {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                for ($i = $_GET['page']-1 ; $i <= $_GET['page']+2 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $_GET['page']-2 ; $i <= $_GET['page']+1 ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($_GET['page'] != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    echo '</ul>';
                                    ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Log On Date</th>
                                                <th>Log On Time</th>
                                                <th>Log Off Date</th>
                                                <th>Log Off Time</th>
                                                <th>IP Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($data = $answer->fetch()) {
                                                echo '<tr>';
                                                    echo '<td>' . $data['name'] . '</td>';
                                                    if ($data['type'] == "connection") {
                                                        echo '<td>Con</td>';
                                                        echo '<td>' . $data['connection_date'] . '</td>';
                                                        echo '<td>' . $data['connection_time'] . '</td>';
                                                    } else {
                                                        echo '<td>Dis</td>';
                                                        echo '<td></td>';
                                                        echo '<td></td>';
                                                    }
                                                    if ($data['type'] == "disconnection") {
                                                        echo '<td>' . $data['connection_date'] . '</td>';
                                                        echo '<td>' . $data['connection_time'] . '</td>';
                                                    } else {
                                                        echo '<td></td>';
                                                        echo '<td></td>';
                                                    }
                                                    echo '<td>' . $data['ip_address'] . '</td>';
                                                echo '</tr>';
                                            }
                                            $answer->closeCursor();
                                            ?>
                                        </tbody>
                                    </table>