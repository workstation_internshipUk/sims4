<?php
include_once('connect_db.php');
if (isset($_POST['id'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$answer0 = $conn->prepare('SELECT name FROM user WHERE id = ?');
		$answer0->execute(array($_POST['id']));
		if ($data0 = $answer0->fetch()) {
			?>
			<h1>Communication Flows for <?php echo $data0['name']; ?></h1>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Exchanged with</th>
						<th>Sent To</th>
						<th>Received From</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$answer1 = $conn->prepare('SELECT id, name FROM user WHERE id <> ?');
					$answer1->execute(array($_POST['id']));
					while ($data1 = $answer1->fetch()) {
						echo '<tr>';
						$countSent = $conn->prepare('SELECT COUNT(*) AS count_sent FROM message WHERE id_sender = ? AND id_receiver = ?');
						$countSent->execute(array($_POST['id'], $data1['id']));
						$countReceived  = $conn->prepare('SELECT COUNT(*) AS count_received FROM message WHERE id_receiver = ? AND id_sender = ?');
						$countReceived->execute(array($_POST['id'], $data1['id']));
						echo '<td>' . $data1['name'] . '</td>';
						if ($countMessageSent = $countSent->fetch()) {
							if ($countMessageSent['count_sent'] < 10) {
								echo '<td>' . $countMessageSent['count_sent'] . '</td>';
							} else {
								echo '<td class="danger">' . $countMessageSent['count_sent'] . '</td>';
							}
						} else {
							echo '<td></td>';
						}
						if ($countMessageReceived = $countReceived->fetch()) {
							if ($countMessageReceived['count_received'] < 10) {
								echo '<td>' . $countMessageReceived['count_received'] . '</td>';
							} else {
								echo '<td class="danger">' . $countMessageReceived['count_received'] . '</td>';
							}
						} else {
							echo '<td></td>';
						}
						$countSent->closeCursor();
						$countReceived->closeCursor();
						echo '</tr>';
					}
					$answer1->closeCursor();
					?>
				</tbody>
			</table>
			<?php

		}
	}
}
if (isset($conn)) {
	$conn = null;
}
?>