<?php
include("check_cookies.php");
checkPermissions(6,2);
if (isset($_POST['id'])) {
    if ($_POST['id'] == "All") {
        $answer = $conn->prepare('SELECT id FROM user WHERE connected = 1 AND id <> ?');
        $answer->execute(array($_SESSION['user']->id()));
        while ($data = $answer->fetch()) {
            $log = $conn->prepare('INSERT INTO connection_logs (id_user, type, connection_date, connection_time, ip_address) VALUES (?, "disconnection", ?, ?, ?)');
            $log->execute(array($data['id'], date('Y-m-d'), date('H:i:s'), "By " . $_SESSION['user']->name()));
        }
        $answer->closeCursor();
        $mod = $conn->prepare('UPDATE user SET connected = 0 WHERE id <> ?');
        $mod->execute(array($_SESSION['user']->id()));
        echo '<script>alert("Everyone has been logged out.");</script>';
    } else {
        $_POST['id'] = (int) $_POST['id'];
        if ($_POST['id'] > 0) {
            $mod = $conn->prepare('UPDATE user SET connected = 0 WHERE id = ?');
            $mod->execute(array($_POST['id']));
            $log = $conn->prepare('INSERT INTO connection_logs (id_user, type, connection_date, connection_time, ip_address) VALUES (?, "disconnection", ?, ?, ?)');
            $log->execute(array($_POST['id'], date('Y-m-d'), date('H:i:s'), "By " . $_SESSION['user']->name()));
            echo '<script>alert("The selected user has been logged out.");</script>';
        } else {
            echo '<script>alert("An error has occured !");</script>';
        }
    }
}
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    
    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Logout Users</div>
                                <div class="panel-body">
                                    <form action="logout_user.php" method="POST" class="form-horizontal">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-2">User to logout</label>
                                                <div class="col-md-3">
                                                    <select name="id" class="form-control">
                                                        <option value="0">--Select User--</option>
                                                        <?php
                                                        $answer = $conn->query('SELECT id, name FROM user WHERE connected = 1 ORDER BY name');
                                                        while ($data = $answer->fetch()) {
                                                            echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>';
                                                        }
                                                        $answer->closeCursor();
                                                        if ($_SESSION['user']->name() == "Dino" || $_SESSION['user']->name() == "BenoitT") {
                                                            echo '<option value="All">Logout All</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div><br />
                                            <div class="form-group">
                                                <div class="col-md-offset-2 col-md-3">
                                                    <input class="btn btn-primary btn-block" type="submit" id="changepassbutton" value="Logout Selected">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="js/functions.js"></script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>