<?php
include("check_cookies.php");
checkPermissions(6,4);
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>

    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Users";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Add User</div>
                                <div class="panel-body">
                                    <form action="checkuser.php" method="POST" class="form-horizontal">
                                        <fieldset>
                                            <legend>SIMS settings</legend>
                                            <div class="form-group has-error" id="namecontrol">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Name<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="name" data-required="1" class="form-control" onblur="checkInput('name');" onkeyup="checkInput('name');" id="nameinput" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Group<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="group">
                                                            <?php
                                                            $answer = $conn->query('SELECT * FROM `group`');
                                                            while ($data = $answer->fetch()) {
                                                                if ($data['name'] != 'Admin') {
                                                                    echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend>User Information</legend>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Status Level<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="level">
                                                            <option value="1">Level 1</option>
                                                            <option value="2">Level 2</option>
                                                            <option value="3">Level 3</option>
                                                            <option value="4">Level 4</option>
                                                            <option value="5">Level 5</option>
                                                            <option value="6">Level 6</option>
                                                            <option value="7">Level 7</option>
                                                            <option value="8">Level 8</option>
                                                            <option value="9">Level 9</option>
                                                            <option value="10">Level 10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="textarea2">User Description:<br><small>Max:255 characters</small></label>
                                                    <div class="col-md-6">
                                                        <textarea name="user-description" class="form-control" rows="5" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend>Administration of working time</legend>
                                            <div class="form-group has-error" id="startcontrol">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="startinput">Start Date<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control datepicker" name="sdate" id="startinput" onblur="checkInput('start');" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group has-error" id="endcontrol">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="endinput">End Date<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control datepicker" name="edate" id="endinput" onblur="checkInput('end');" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Special Days<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="special_days">
                                                            <option value="0">0</option>
                                                            <option value="-10">-10</option>
                                                            <option value="-9">-9</option>
                                                            <option value="-8">-8</option>
                                                            <option value="-7">-7</option>
                                                            <option value="-6">-6</option>
                                                            <option value="-5">-5</option>
                                                            <option value="-4">-4</option>
                                                            <option value="-3">-3</option>
                                                            <option value="-2">-2</option>
                                                            <option value="-1">-1</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="textarea2">HR comments:<br><small>Max:255 characters</small></label>
                                                    <div class="col-md-6">
                                                        <textarea name="hr_comment" class="form-control" rows="5" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-offset-2 col-md-6">
                                                        <button type="submit" class="btn btn-primary btn-block">Validate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="assets/form-validation.js"></script>
        <script>
            //Wizard that manages the form
            jQuery(document).ready(function() {   
                FormValidation.init();
            });
            $(function() {
                $(".datepicker").datepicker();
            });
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>