<?php
include("check_cookies.php");
checkPermissions(0, 1);
checkPermissions(1, 1);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<!-- bootstrap -->

   	        <title>SIMS 4 - Staff Information Management System</title>
       	 	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        	<link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen"> 
        	<link href="assets/styles.css" rel="stylesheet" media="screen">
        	<link rel="shortcut icon" href="images/favicon.ico"> 
	<!--	<link rel="stylesheet" type="text/css" href="extra_hours.css"> -->
		<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script> -->
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css"> 
        	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        	<!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        	<![endif]-->
        	<!--  <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
		<!-- Include CSS for JQuery Frontier Calendar plugin (Required for calendar plugin) -->
		<link rel="stylesheet" type="text/css" href="css/frontierCalendar/jquery-frontier-cal-1.3.2.hr.css" />
		<!-- Include CSS for color picker plugin (Not required for calendar plugin. Used for example.) -->
		<link rel="stylesheet" type="text/css" href="css/colorpicker/colorpicker.css" />
		<!-- Include CSS for JQuery UI (Required for calendar plugin.) -->
		<link rel="stylesheet" type="text/css" href="css/jquery-ui/smoothness/jquery-ui-1.8.1.custom.css" />
		<!--
		Include JQuery Core (Required for calendar plugin)
		** This is our IE fix version which enables drag-and-drop to work correctly in IE. See README file in js/jquery-core folder. **
		-->
		<script type="text/javascript" src="js/jquery-core/jquery-1.4.2-ie-fix.min.js"></script>
		<!-- Include JQuery UI (Required for calendar plugin.) -->
		<script type="text/javascript" src="js/jquery-ui/smoothness/jquery-ui-1.8.1.custom.min.js"></script>
		<!-- Include color picker plugin (Not required for calendar plugin. Used for example.) -->
		<script type="text/javascript" src="js/colorpicker/colorpicker.js"></script>
		<!-- Include jquery tooltip plugin (Not required for calendar plugin. Used for example.) -->
		<script type="text/javascript" src="js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.js"></script>
		<!--
		(Required for plugin)
		Dependancies for JQuery Frontier Calendar plugin.
    		** THESE MUST BE INCLUDED BEFORE THE FRONTIER CALENDAR PLUGIN. **
		-->
		<script type="text/javascript" src="js/lib/jshashtable-2.1.js"></script>
		<!-- Include JQuery Frontier Calendar plugin -->
		<!--<script type="text/javascript" src="js/frontierCalendar/jquery-frontier-cal-1.3.2.min.js"></script>-->
		<script type="text/javascript" src="js/frontierCalendar/jquery-frontier-cal-1.3.2.hr.js"></script>
	</head>

	<!--<body style="background-color: #aaaaaa;" onload="hitByUnicorn(); refresh();">-->
	<body onload="hitByUnicorn(); refresh();">
	<?php 
       // $selected = "Dashboard";
       // require_once 'menu.php';
        ?>

	<!-- <div class="span9" id="content">
		<div class="row-fluid">
			<div class="span9">
				<!-- block 
				<div class="block"> -->
				<!--    <div class="navbar navbar-inner block-header">
				        <div class="muted pull-left">SIMS - HR Calendar</div>
					</div> -->
					
					<div class="block-content collapse in" style="text-align:center">


					<!-- Some CSS for our example. (Not required for calendar plugin. Used for example.)-->
					<style type="text/css" media="screen">
					/*
					Default font-size on the default ThemeRoller theme is set in ems, and with a value that when combined 
					with body { font-size: 62.5%; } will align pixels with ems, so 11px=1.1em, 14px=1.4em. If setting the 
					body font-size to 62.5% isn't an option, or not one you want, you can set the font-size in ThemeRoller 
					to 1em or set it to px.
					http://osdir.com/ml/jquery-ui/2009-04/msg00071.html
					*/
					/*body { font-size: 62.5%; } */
					.shadow {
						-moz-box-shadow: 3px 3px 4px #aaaaaa;
						-webkit-box-shadow: 3px 3px 4px #aaaaaa;
						box-shadow: 3px 3px 4px #aaaaaa;
						/* For IE 8 */
						-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#aaaaaa')";
						/* For IE 5.5 - 7 */
						filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#aaaaaa');
					}
					</style>

					<script type="text/javascript">
					$(document).ready(function(){
	  	//var what="fjgnflkjgnfjgnfljnfgjn";var startDateObj=new Date();var endDateObj=startDateObj;var name="jkfnglkjfng";


						var clickDate = "";
						var clickAgendaItem = "";

						var month=new Array(12);
						month[0]="January";
						month[1]="February";
						month[2]="March";
						month[3]="April";
						month[4]="May";
						month[5]="June";
						month[6]="July";
						month[7]="August";
						month[8]="September";
						month[9]="October";
						month[10]="November";
						month[11]="December";
	
						/**
						 * Initializes calendar with current year & month
						 * specifies the callbacks for day click & agenda item click events
						 * then returns instance of plugin object
						 */
						var jfcalplugin = $("#mycal").jFrontierCal({
							date: new Date(),
							dayClickCallback: myDayClickHandler,
							agendaClickCallback: myAgendaClickHandler,
							agendaDropCallback: myAgendaDropHandler,
							agendaMouseoverCallback: myAgendaMouseoverHandler,
							applyAgendaTooltipCallback: myApplyTooltip,
							agendaDragStartCallback : myAgendaDragStart,
							agendaDragStopCallback : myAgendaDragStop,
							dragAndDropEnabled: true
						}).data("plugin");
	
						/* update current month name */
						var d=new Date();
					   	// update text in div
					  	 jQuery('#date').text(month[d.getMonth()] + " " + d.getFullYear());

						/**
						 * Do something when dragging starts on agenda div
						 */
						function myAgendaDragStart(eventObj,divElm,agendaItem){
							// destroy our qtip tooltip
							if(divElm.data("qtip")){
								divElm.qtip("destroy");
							}	
						};
	
						/**
						 * Do something when dragging stops on agenda div
						 */
						function myAgendaDragStop(eventObj,divElm,agendaItem){
							//alert("drag stop");
						};
	
						/**
						 * Custom tooltip - use any tooltip library you want to display the agenda data.
						 * for this example we use qTip - http://craigsworks.com/projects/qtip/
						 *
						 * @param divElm - jquery object for agenda div element
						 * @param agendaItem - javascript object containing agenda data.
						 */
						function myApplyTooltip(divElm,agendaItem){

							// Destroy currrent tooltip if present
							if(divElm.data("qtip")){
								divElm.qtip("destroy");
							}
		
							var displayData = "";
		
							var title = agendaItem.title;
							var startDate = agendaItem.startDate;
							var endDate = agendaItem.endDate;
							var allDay = agendaItem.allDay;
							var data = agendaItem.data;
							displayData += "<br><b>" + title+ "</b><br><br>";
							if(allDay){
								displayData += "(All day event)<br><br>";
							}else{
								//displayData += "<b>Starts:</b> " + startDate + "<br>" /*+ "<b>Ends:</b> " + endDate + "<br><br>"*/;
							}
							for (var propertyName in data) {
								displayData += "<b>" + propertyName + ":</b> " + data[propertyName] + "<br>"
							}
							// use the user specified colors from the agenda item.
							var backgroundColor = agendaItem.displayProp.backgroundColor;
							var foregroundColor = agendaItem.displayProp.foregroundColor;
							var myStyle = {
								border: {
									width: 5,
									radius: 10
								},
								padding: 10, 
								textAlign: "left",
								tip: true,
								name: "dark" // other style properties are inherited from dark theme		
							};
							if(backgroundColor != null && backgroundColor != ""){
								myStyle["backgroundColor"] = backgroundColor;
							}
							if(foregroundColor != null && foregroundColor != ""){
								myStyle["color"] = foregroundColor;
							}
							// apply tooltip
							divElm.qtip({
								content: displayData,
								position: {
									corner: {
										tooltip: "bottomMiddle",
										target: "topMiddle"			
									},
									adjust: { 
										mouse: true,
										x: 0,
										y: -15
									},
									target: "mouse"
								},
								show: { 
									when: { 
										event: 'mouseover'
									}
								},
								style: myStyle
							});

						};

						/**
						 * Make the day cells roughly 3/4th as tall as they are wide. this makes our calendar wider than it is tall. 
						 */
						jfcalplugin.setAspectRatio("#mycal",0.75);

						/**
						 * Called when user clicks day cell
						 * use reference to plugin object to add agenda item
						 */
						function myDayClickHandler(eventObj){
							// Get the Date of the day that was clicked from the event object
							var date = eventObj.data.calDayDate;
							// store date in our global js variable for access later
							clickDate = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
							// open our add event dialog
							$('#add-event-form').dialog('open');
						};
	
						/**
						 * Called when user clicks and agenda item
						 * use reference to plugin object to edit agenda item
						 */
						function myAgendaClickHandler(eventObj){
							// Get ID of the agenda item from the event object
							var agendaId = eventObj.data.agendaId;		
							// pull agenda item from calendar
							var agendaItem = jfcalplugin.getAgendaItemById("#mycal",agendaId);
							clickAgendaItem = agendaItem;
							$("#display-event-form").dialog('open');
						};
	
						/**
						 * Called when user drops an agenda item into a day cell.
						 */
						function myAgendaDropHandler(eventObj){
							// Get ID of the agenda item from the event object
							var agendaId = eventObj.data.agendaId;
							// date agenda item was dropped onto
							var date = eventObj.data.calDayDate;
							// Pull agenda item from calendar
							var agendaItem = jfcalplugin.getAgendaItemById("#mycal",agendaId);		
							alert("You dropped agenda item " + agendaItem.title + 
								" onto " + date.toString() + ". Here is where you can make an AJAX call to update your database.");
						};
	
						/**
						 * Called when a user mouses over an agenda item	
						 */
						function myAgendaMouseoverHandler(eventObj){
							var agendaId = eventObj.data.agendaId;
							var agendaItem = jfcalplugin.getAgendaItemById("#mycal",agendaId);
							//alert("You moused over agenda item " + agendaItem.title + " at location (X=" + eventObj.pageX + ", Y=" + eventObj.pageY + ")");
						};
						/**
						 * Initialize jquery ui datepicker. set date format to yyyy-mm-dd for easy parsing
						 */
						$("#dateSelect").datepicker({
							showOtherMonths: true,
							selectOtherMonths: true,
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							dateFormat: 'yy-mm-dd'
						});
	
						/**
						 * Set datepicker to current date
						 */
						$("#dateSelect").datepicker('setDate', new Date());
						/**
						 * Use reference to plugin object to a specific year/month
						 */
						$("#dateSelect").bind('change', function() {
							var selectedDate = $("#dateSelect").val();
							var dtArray = selectedDate.split("-");
							var year = dtArray[0];
							// jquery datepicker months start at 1 (1=January)		
							var month = dtArray[1];
							// strip any preceeding 0's		
							month = month.replace(/^[0]+/g,"")		
							var day = dtArray[2];
							// plugin uses 0-based months so we subtrac 1
							jfcalplugin.showMonth("#mycal",year,parseInt(month-1).toString());
						});	
						/**
						 * Initialize previous month button
						 */
						$("#BtnPreviousMonth").button();
						$("#BtnPreviousMonth").click(function() {
							jfcalplugin.showPreviousMonth("#mycal");
							// update the jqeury datepicker value
							var calDate = jfcalplugin.getCurrentDate("#mycal"); // returns Date object
							var cyear = calDate.getFullYear();
							// Date month 0-based (0=January)
							var cmonth = calDate.getMonth();
							var cday = calDate.getDate();
							// jquery datepicker month starts at 1 (1=January) so we add 1
							$("#dateSelect").datepicker("setDate",cyear+"-"+(cmonth+1)+"-"+cday);
							// update month name in month name div
							jQuery('#date').text(month[cmonth] + " " + cyear);
							return false;
						});
						/**
						 * Initialize next month button
						 */
						$("#BtnNextMonth").button();
						$("#BtnNextMonth").click(function() {
							jfcalplugin.showNextMonth("#mycal");
							// update the jqeury datepicker value
							var calDate = jfcalplugin.getCurrentDate("#mycal"); // returns Date object
							var cyear = calDate.getFullYear();
							// Date month 0-based (0=January)
							var cmonth = calDate.getMonth();
							var cday = calDate.getDate();
							// jquery datepicker month starts at 1 (1=January) so we add 1
							$("#dateSelect").datepicker("setDate",cyear+"-"+(cmonth+1)+"-"+cday);
							jQuery('#date').text(month[cmonth] + " " + cyear);		
							return false;
						});
	
						/**
						 * Initialize delete all agenda items button
						 */
						$("#BtnDeleteAll").button();
						$("#BtnDeleteAll").click(function() {	
							jfcalplugin.deleteAllAgendaItems("#mycal");	
							return false;
						});		
	
						/**
						 * Initialize iCal test button
						 */
						$("#BtnICalTest").button();
						$("#BtnICalTest").click(function() {
							// Please note that in Google Chrome this will not work with a local file. Chrome prevents AJAX calls
							// from reading local files on disk.		
							jfcalplugin.loadICalSource("#mycal",$("#iCalSource").val(),"html");	
							return false;
						}); 
 												

						/**
						 * Initialize add event modal form
						 */
						$("#add-event-form").dialog({
							autoOpen: false,
							height: 350,
							width: 650,
							modal: true,
							buttons: {
								'Add Event': function() { 

									var what = jQuery.trim($("#what").val());
			
									if(what == ""){
										alert("Please enter a short event description into the \"what\" field.");
									}else{
				
										var startDate = $("#startDate").val();
										var startDtArray = startDate.split("-");
										var startYear = startDtArray[0];
										// jquery datepicker months start at 1 (1=January)		                                                         
                                                                               
										var startMonth = startDtArray[1];		
										var startDay = startDtArray[2];
										// strip any preceeding 0's		
										startMonth = startMonth.replace(/^[0]+/g,"");
										startDay = startDay.replace(/^[0]+/g,"");
										var startHour = jQuery.trim($("#startHour").val());
										var startMin = jQuery.trim($("#startMin").val());
										var startMeridiem = jQuery.trim($("#startMeridiem").val());
										startHour = parseInt(startHour.replace(/^[0]+/g,""));
										if(startMin == "0" || startMin == "00"){
											startMin = 0;
										}else{
											startMin = parseInt(startMin.replace(/^[0]+/g,""));
										}
										if(startMeridiem == "AM" && startHour == 12){
											startHour = 0;
										}else if(startMeridiem == "PM" && startHour < 12){
											startHour = parseInt(startHour) + 12;
										};

										//var endDate = $("#endDate").val();
										/*var endDtArray = endDate.split("-");
										var endYear = endDtArray[0];*/ 
										// jquery datepicker months start at 1 (1=January)		
										//var endMonth = endDtArray[1];	
										//var endDay = endDtArray[2];
										// strip any preceeding 0's		
										//endMonth = endMonth.replace(/^[0]+/g,"");
	
										//endDay = endDay.replace(/^[0]+/g,"");
										//var endHour = jQuery.trim($("#endHour").val());
										//var endMin = jQuery.trim($("#endMin").val());
										//var endMeridiem = jQuery.trim($("#endMeridiem").val());
										//endHour = parseInt(endHour.replace(/^[0]+/g,""));
										//if(endMin == "0" || endMin == "00"){
										//	endMin = 0;
										//}else{
										//	endMin = parseInt(endMin.replace(/^[0]+/g,""));
										//}
										//if(endMeridiem == "AM" && endHour == 12){
										//	endHour = 0;
										//}else if(endMeridiem == "PM" && endHour < 12){
										//	endHour = parseInt(endHour) + 12;
										//};
					
										//alert("Start time: " + startHour + ":" + startMin + " " + startMeridiem + ", End time: " + endHour + ":" + endMin + " " + endMeridiem);

										// Dates use integers
										var startDateObj = new Date(parseInt(startYear),parseInt(startMonth)-1,parseInt(startDay),startHour,startMin,0,0);
										/*var endDateObj = new Date(parseInt(endYear),parseInt(endMonth)-1,parseInt(endDay),endHour,endMin,0,0);*/
endDateObj=startDateObj;
//debugging
//alert(endDateObj);

name="<?php print_r($_SESSION['user']->name()); ?>";

										// add new event to the calendar
										jfcalplugin.addAgendaItem(
											"#mycal",
											what,
											startDateObj,
											endDateObj,
											false,
											{
												//fname: "Santa",
												//lname: "Claus",
												//leadReindeer: "Rudolph",
												on: new Date(),
                                                                                                'added by': name,
												//myNum: 42
											},
											{
												backgroundColor: $("#colorBackground").val(),
												foregroundColor: $("#colorForeground").val()
											}
										);

                                                                               var bgcolor=$("#colorBackground").val().substring(1);
									       var fgcolor=$("#colorForeground").val().substring(1);
										$.ajax({
										       type: "GET",
										       url: "insert_hr_calendar.php",
										       data: "addedBy=" + name + "&on=" + new Date().toGMTString()+
                 								      "&desc=" + what + "&start=" + startDateObj.toGMTString()+ 
                                                                                       "&bgcolor=" + bgcolor +
                                                                                       "&fgcolor=" + fgcolor,
										//success: function(result){alert(result);},
                                                               		        error:function(exception){alert('Exeption:'+exception);}
										     });
										$(this).dialog('close');
                                                                              // location.reload(); 
                                       
									}
				
								},
								Cancel: function() {
									$(this).dialog('close');
								}
							},
							open: function(event, ui){
								// initialize start date picker
								$("#startDate").datepicker({
									showOtherMonths: true,
									selectOtherMonths: true,
									changeMonth: true,
									changeYear: true,
									showButtonPanel: true,
									dateFormat: 'yy-mm-dd'
								});
								// initialize end date picker
								$("#endDate").datepicker({
									showOtherMonths: true,
									selectOtherMonths: true,
									changeMonth: true,
									changeYear: true,
									showButtonPanel: true,
									dateFormat: 'yy-mm-dd'
								});
								// initialize with the date that was clicked
								$("#startDate").val(clickDate);
								$("#endDate").val(clickDate);
								// initialize color pickers
								$("#colorSelectorBackground").ColorPicker({
									color: "#333333",
									onShow: function (colpkr) {
										$(colpkr).css("z-index","10000");
										$(colpkr).fadeIn(500);
										return false;
									},
									onHide: function (colpkr) {
										$(colpkr).fadeOut(500);
										return false;
									},
									onChange: function (hsb, hex, rgb) {
										$("#colorSelectorBackground div").css("backgroundColor", "#" + hex);
										$("#colorBackground").val("#" + hex);
									}
								});
								//$("#colorBackground").val("#1040b0");		
								$("#colorSelectorForeground").ColorPicker({
									color: "#ffffff",
									onShow: function (colpkr) {
										$(colpkr).css("z-index","10000");
										$(colpkr).fadeIn(500);
										return false;
									},
									onHide: function (colpkr) {
										$(colpkr).fadeOut(500);
										return false;
									},
									onChange: function (hsb, hex, rgb) {
										$("#colorSelectorForeground div").css("backgroundColor", "#" + hex);
										$("#colorForeground").val("#" + hex);
									}
								});
								//$("#colorForeground").val("#ffffff");				
								// put focus on first form input element
								$("#what").focus();
							},
							close: function() {
								// reset form elements when we close so they are fresh when the dialog is opened again.
								$("#startDate").datepicker("destroy");
								$("#endDate").datepicker("destroy");
								$("#startDate").val("");
								$("#endDate").val("");
								$("#startHour option:eq(0)").attr("selected", "selected");
								$("#startMin option:eq(0)").attr("selected", "selected");
								$("#startMeridiem option:eq(0)").attr("selected", "selected");
								$("#endHour option:eq(0)").attr("selected", "selected");
								$("#endMin option:eq(0)").attr("selected", "selected");
								$("#endMeridiem option:eq(0)").attr("selected", "selected");			
								$("#what").val("");
								$("#colorBackground").val("#000000");
								$("#colorForeground").val("#ffffff");
							}
						});
/**
						 * Initialize edit event form.
						 */
						$("#edit-event-form").dialog({
							autoOpen: false,
							height: 300,
							width: 700,
							modal: true,
							buttons: {		
								Cancel: function() {
									$(this).dialog('close');
								},
								'Edit': function() {
									//alert(clickAgendaItem.data.on);
                                                                        what = jQuery.trim($('#edited').val());
                                                                       // alert("IT test : you sent :"+ what );
                                                                       // alert(what);
									$.ajax({async:false,
										type: "GET",
										url: "edit_hr_calendar.php",
		                   						data: "edited=" + what + "&id=" + 
							 			      clickAgendaItem.data.id,
                                                                        // success: function(result){alert(result);},
                                                                         error:function(exception){alert('Exeption:'+exception);}   
                                                                     	        }); location.reload();      		
								}
                                                               },
							open: function(event, ui){
								if(clickAgendaItem != null){
									var title = clickAgendaItem.title;
									/*var startDate = clickAgendaItem.startDate;
									var endDate = clickAgendaItem.endDate;
									var allDay = clickAgendaItem.allDay;*/
									var data = clickAgendaItem.data;
									// in our example add agenda modal form we put some fake data in the agenda data. we can retrieve it here.
									$("#edit-event-form").append(
								"<textarea id='edited' style='FONT-SIZE: 12pt; WIDTH: 500px; height:100px;'>" + title + "</textarea>"		
									);				
									if(allDay){
										$("#display-event-form").append(
											"(All day event)<br><br>"				
										);				
									}else{
										$("#display-event-form").append(
											"<b>Starts:</b> " + startDate + "<br>" //+
											//"<b>Ends:</b> " + endDate + "<br><br>"				
										);				
									}
									for (var propertyName in data) {
										$("#display-event-form").append("<b>" + propertyName + ":</b> " + data[propertyName] + "<br>");
									}			
								}		
							},
							close: function() {
								// clear agenda data
								$("#edit-event-form").html("");
							}
						});
	
						/**
						 * Initialize display event form.
						 */
						$("#display-event-form").dialog({
							autoOpen: false,
							height: 250,
							width: 400,
							modal: true,
							buttons: {		
								Cancel: function() {
									$(this).dialog('close');
								},
								'Edit': function() {

							
									//alert("Make your own edit screen or dialog!");
									                         
                                                                $('#edit-event-form').dialog('open');
                                                                 
                                                                        },
								'Delete': function() {//alert(clickAgendaItem.data.id);

							
								
									if(confirm("Are you sure you want to delete this agenda item?")){
										if(clickAgendaItem != null){
											jfcalplugin.deleteAgendaItemByDataAttr("#mycal","id",clickAgendaItem.data.id);
										$.ajax({async:false,
										       type: "GET",
										       url: "delete_hr_calendar.php",
										       data: "id=" + clickAgendaItem.data.id,
										//success: function(result){alert(result);},
                                                               		        error:function(exception){alert('Exeption:'+exception);}
										     });

											//jfcalplugin.deleteAgendaItemByDataAttr("#mycal","myNum",42);

										};
										$(this).dialog('close');
									};
							
								}			
							},
							open: function(event, ui){
								if(clickAgendaItem != null){
									var title = clickAgendaItem.title;
									/*var startDate = clickAgendaItem.startDate;
									var endDate = clickAgendaItem.endDate;
									var allDay = clickAgendaItem.allDay;*/
									var data = clickAgendaItem.data;
									// in our example add agenda modal form we put some fake data in the agenda data. we can retrieve it here.
									$("#display-event-form").append(
										"<br><b>" + title+ "</b><br><br>"		
									);				
									if(allDay){
										$("#display-event-form").append(
											"(All day event)<br><br>"				
										);				
									}else{
										$("#display-event-form").append(
											"<b>Starts:</b> " + startDate + "<br>" //+
											//"<b>Ends:</b> " + endDate + "<br><br>"				
										);				
									}
									for (var propertyName in data) {
										$("#display-event-form").append("<b>" + propertyName + ":</b> " + data[propertyName] + "<br>");
									}			
								}		
							},
							close: function() {
								// clear agenda data
								$("#display-event-form").html("");
							}
						});	 

						/**
						 * Initialize our tabs
						 */
						$("#tabs").tabs({
							/*
							 * Our calendar is initialized in a closed tab so we need to resize it when the example tab opens.
							 */
							show: function(event, ui){
								if(ui.index == 1){
									jfcalplugin.doResize("#mycal");
								}
							}	
						});

/* working
jfcalplugin.addAgendaItem(
									"#mycal",
									what,
									new Date(),
									new Date(),
									false,
									  {
										//fname: "Santa",
										//lname: "Claus",
										//leadReindeer: "Rudolph",
										on:new Date(),
                                                                                'added by':name
										//myNum: 42
									  },
									{
									 backgroundColor: $("#colorBackground").val(),
									foregroundColor: $("#colorForeground").val()
									}
									);  */                                        	
/*in progress*/
				var cpt=0;
                                var events_js;
				$.ajax({async: false,
				type: "GET",
				url: "fill_hr_calendar.php",
				//data: "id=" + cpt ,
				success: function(result){
				events_js = JSON.parse(result);/*alert(events_js[0][0]);*/},
                                error:function(exception){alert('Exeption:'+exception);}
				});
                                while(events_js[cpt]!=null){//alert(JSON.stringify(events_js));	
                                eventId=events_js[cpt][0];					
					      	 user=events_js[cpt][1];//alert(events_js[cpt][0]);
						 startDateObj=events_js[cpt][2];//alert(startDateObj);
                                                 endDateObj=startDateObj;
						 what=events_js[cpt][3];
						 date_added=events_js[cpt][4];
                                                 bgcolor=events_js[cpt][5];
						 fgcolor=events_js[cpt][6]; 
						 jfcalplugin.addAgendaItem(
									"#mycal",
									what,
									new Date(startDateObj),
									new Date(startDateObj),
									false,
									  {  id:eventId,
										//fname: "Santa",
										//lname: "Claus",
										//leadReindeer: "Rudolph",
										on:date_added,
                                                                                'added by': user
										//myNum: 42
									  },
									{
									 backgroundColor: bgcolor,
									 foregroundColor: fgcolor
									}
									);
						cpt++;
						};
					});
                                        
					</script>

					<!--<h1 style="font-size: 30px; font-weight: bold;">jQuery Frontier Calendar</h1> -->

					<!--<div id="tabs">-->
						

							<div id="toolbar" class="ui-widget-header ui-corner-all" style="padding:3px; vertical-align: middle; white-space:nowrap; overflow: hidden;">
								<button id="BtnPreviousMonth">Previous Month</button>
								<button id="BtnNextMonth">Next Month</button>
								&nbsp;&nbsp;&nbsp;
								Today: <input type="text" id="dateSelect" size="20" style="border:none" disabled/>
								&nbsp;&nbsp;&nbsp;
							</div>

							<br>

							<!--
							You can use pixel widths or percentages. Calendar will auto resize all sub elements.
							Height will be calculated by aspect ratio. Basically all day cells will be as tall
							as they are wide.
							-->
							<h1 id="date"></h1>
							<div id="mycal"></div>

							</div>

							<!-- debugging-->
							<div id="calDebug"></div>

							<!-- Add event modal form -->
							<style type="text/css">
								label, input.text, select { display:block; }
								fieldset { padding:0; border:0; margin-top:25px; }
								.ui-dialog .ui-state-error { padding: .3em; }
								.validateTips { border: 1px solid transparent; padding: 0.3em; }
							</style>
							<div id="add-event-form" title="Add New Event">
								<!--<p class="validateTips">All form fields are required.</p> -->
								<form style="height: 100px">
									<label for="name">What?</label>
									<textarea id="what" class="text ui-widget-content ui-corner-all"  style="margin-bottom:12px; width:95%; padding: .4em;"></textarea>
		<div style="visibility: hidden">				<table style="width:100%; padding:5px;">
										<tr>
											<td>
												<label>Start Date</label>
												<input type="text" name="startDate" id="startDate" value="" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;" disabled hidden/>				
											</td>
											<td>&nbsp;</td>
											<td>
												<label>Start Hour</label> 
												<select id="startHour" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;" hidden>
													
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12" SELECTED>12</option>
												</select>				
											<td>
											<td>
												<label>Start Minute</label> 
												<select id="startMin" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;" hidden>
													<option value="00" SELECTED>00</option>
													<option value="10">10</option>
													<option value="20">20</option>
													<option value="30">30</option>
													<option value="40">40</option>
													<option value="50">50</option>
												</select>				
											<td>
											<td>
												<label>Start AM/PM</label> 
												<select id="startMeridiem" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;" hidden>
													<option value="AM" SELECTED>AM</option>
													<option value="PM">PM</option>
												</select>				
											</td>
										</tr>
									<!--	<tr>
											<td>
												<label>End Date</label>
												<input type="text" name="endDate" id="endDate" value="" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;"/>				
											</td>
											<td>&nbsp;</td>
											<td>
												<label>End Hour</label>
												<select id="endHour" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;">
													<option value="12" SELECTED>12</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
												</select>				
											<td>
											<td>
												<label>End Minute</label>
												<select id="endMin" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;">
													<option value="00" SELECTED>00</option>
													<option value="10">10</option>
													<option value="20">20</option>
													<option value="30">30</option>
													<option value="40">40</option>
													<option value="50">50</option>
												</select>				
											<td>
											<td>
												<label>End AM/PM</label>
												<select id="endMeridiem" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;">
													<option value="AM" SELECTED>AM</option>
													<option value="PM">PM</option>
												</select>				
											</td>				
										</tr>-->			
									</table> </div>
									<table>
										<tr>
											<td>
												<label>Background Color</label>
											</td>
											<td>
												<div id="colorSelectorBackground"><div style="background-color: #333333; width:30px; height:30px; border: 1px solid #000000;"></div></div>
												<input type="hidden" id="colorBackground" value="#333333">
											</td>
											<td>&nbsp;&nbsp;&nbsp;</td>
											<td>
												<label>Text Color</label>
											</td>
											<td>
												<div id="colorSelectorForeground"><div style="background-color: #ffffff; width:30px; height:30px; border: 1px solid #000000;"></div></div>
												<input type="hidden" id="colorForeground" value="#ffffff">
											</td>						
										</tr>				
									</table>
								</fieldset>
								</form>
		</div>
		
		<div id="display-event-form" title="View Agenda Item">
			
		</div>	
		<div id="edit-event-form" title="Edit Agenda Item">
			
		</div>	

		<p>&nbsp;</p>

	<!--</div>--><!-- end example tab --> 
	
	<!--<div id="tabs-3"> -->
	

<style type="text/css">
a {
 /* font: 11px/14px georgia, times, verdana, arial, helvetica, sans-serif;*/
  text-decoration:none;
}
a:hover { background-color:#ccc; }
/*a:link { color:royalblue; }
a:visited { color:royalblue; }*/
.code, .info, .codeHead, .apiHead, .api {
	background-color: #dddddd;
	color: #000000;
	font: 11px/14px verdana, georgia, times, arial, helvetica, sans-serif;
	line-height: 18px;
	padding: 3px;
	margin: 0px;
}
.codeHead {
	background-color: #bbbbbb;
}
.info {
	background-color: #ffffff;
	color: #444444;
}
.apiHead{
	background-color: #dedede;
	color: #000000;
}
.api {
	background-color: #ffffff;
	color: #333333;
}

textarea.code {
	width: 100%; height: 300px; padding:0px; margin:0px; font-size:1.2em; font-family:monospace; background-color: #efefef; color: #222222;
}
	
table.apiTable {
	border-width: 0px 0px 0px 0px;
	border-spacing: 2px;
	border-style: outset outset outset outset;
	border-color: gray gray gray gray;
	border-collapse: collapse;
	background-color: #ffffff;
	width: 100%;
}
table.apiTable th {
	font: 12px/12px verdana, georgia, times, arial, helvetica, sans-serif;
	font-weight: bold;
	text-align: left;
	color: #555555;
	border-width: 1px 1px 1px 1px;
	padding: 3px 3px 3px 3px;
	border-style: inset inset inset inset;
	border-color: rgb(200,200,200);
	background-color: #ffffff;
	-moz-border-radius: 0px 0px 0px 0px;
}
table.apiTable td {
	font: 12px/12px verdana, georgia, times, arial, helvetica, sans-serif;
	color: #555555;
	border-width: 1px 1px 1px 1px;
	padding: 3px 3px 3px 3px;
	border-style: inset inset inset inset;
	border-color: rgb(200,200,200);
	background-color: #ffffff;
	-moz-border-radius: 0px 0px 0px 0px;
}
</style>	
	


<p>&nbsp;</p>

					<!--</div> -->

			<!--	</div>
				<!--end of Block-->

			</div>
			<?php 
			//include('right.php');                              
			?> 
			<!--</div>
             <!--   </div>
	    <footer>
                <p> &copy;&nbsp;Studio-Solution.com 2015</p>
            </footer> 
           </div>
</body>
</html>
