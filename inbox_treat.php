<?php
include_once('connect_db.php');
if (isset($_POST['post']) && isset($_POST['type'])) {
	$isSaved = 0;
	$isDeleted = 0;
	if ($_POST['type'] == "delete" || $_POST['type'] == "save" || $_POST['type'] == "normal") {
		if ($_POST['type'] == "delete") {
			$isDeleted = 1;
		} else if ($_POST['type'] == "save") {
			$isSaved = 1;
		}
		$arr = explode(",",htmlspecialchars($_POST['post']));
		$check = true;
		$arrLength = count($arr);
		for ($i = 0; $i < $arrLength; $i++) {
			$arr[$i] = (int) $arr[$i];
			if ($arr[$i] <= 0) {
				$check = false;
			}
		}
		if ($check) {
			for ($i = 0; $i < $arrLength; $i++) {
				$del = $conn->prepare('UPDATE message SET is_deleted = ?, is_saved = ? WHERE id = ?');
				$del->execute(array($isDeleted, $isSaved, $arr[$i]));
			}
			echo 'true';
		} else {
			echo 'false';
		}
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>