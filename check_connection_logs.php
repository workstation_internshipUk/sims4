<?php
include_once('connect_db.php');
$answer0 = $conn->prepare('SELECT last_used FROM secure_logs WHERE name = ?');
$answer0->execute(array("check_connection_logs"));
if ($data0 = $answer0->fetch()) {
	//Only delete the old logs once per day.
	if ($data0['last_used'] < date("Y-m-d")) {
		$del = $conn->prepare('DELETE FROM shift_logs WHERE connection_date < ?');
		$del->execute(array(date("Y-m-d", time() - (90 * 24 * 60 * 60))));
		$mod = $conn->prepare('UPDATE secure_logs SET last_used = ? WHERE name = ?');
		$mod->execute(array(date("Y-m-d"),"check_connection_logs"));
		echo 'true';
	} else {
		echo 'true';
	}
} else {
	echo 'false';
}
$answer0->closeCursor();
if (isset($conn)) {
    $conn = null;
}
?>