<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); refresh(); writeTable('inbox', 0)">
        <?php 
        $selected = "Mysims";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Inbox</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">Messages per page : </div>
                                        <div class="col-md-3">
                                            <select class="form-control" id="nbmess" onchange="writeTable(document.getElementById('hiddentype').value, 1)">
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="13">13</option>
                                                <option value="21">21</option>
                                                <option value="26">26</option>
                                                <option value="33">33</option>
                                                <option value="42">42</option>
                                                <option value="52">52</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" id="searchuser" class="form-control" placeholder="Search User" onkeyup="writeTable(document.getElementById('hiddentype').value, 0)" />
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" id="searchsubject" class="form-control" placeholder="Search Subject" onkeyup="writeTable(document.getElementById('hiddentype').value, 0)" />
                                        </div>
                                        <input type="hidden" id="hiddennbmess" value="10" />
                                        <input type="hidden" id="hiddenpage" value="1" />
                                        <input type="hidden" id="hiddentype" value="inbox" />
                                    </div>
                                    <div id="inboxcontent">
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
            	<p>&copy; Studio-Solution.com 2015</p>
        	</footer>
        </div>
    	<!--/.fluid-container-->
        <script>
            //Function called onload or when the user interact with button and links in the inbox. It changes the content displayed in the inbox
            function writeTable(type, pageclicked) {
                var xmlhttp = new XMLHttpRequest();
                var nbmess = 10;
                document.getElementById('hiddentype').value = type;
                document.getElementById('hiddennbmess').value = document.getElementById('nbmess').value;
                nbmess = document.getElementById('hiddennbmess').value;
                var page = 0;
                if (pageclicked != 0) {
                    page = pageclicked;
                    document.getElementById('hiddenpage').value = page;
                } else {
                    page = document.getElementById('hiddenpage').value;
                }
                var searchUser = document.getElementById('searchuser').value;
                var searchSubject = document.getElementById('searchsubject').value;
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("inboxcontent").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST","inbox_content.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("type="+type+"&searchuser="+searchUser+"&searchsubject="+searchSubject+"&page="+page+"&nbmess="+nbmess);
            }
            //Function called when a box is checked or unchecked. It enables/disables the button.
            function checkboxHandler(saved, deleted) {
                var objCheckBoxes = document.getElementsByName("selectmessage");
                var check = false;
                for (var i = 0; i < objCheckBoxes.length; i++) {
                    if (objCheckBoxes[i].checked) {
                        check = true;
                    }
                }
                if ((saved == 0) && (deleted == 0)) {
                    if (check) {
                        if (document.getElementById("buttondel").disabled) {
                            document.getElementById("buttondel").disabled = false;
                        }
                        if (document.getElementById("buttonsav").disabled) {
                            document.getElementById("buttonsav").disabled = false;
                        }
                    } else {
                        if (!document.getElementById("buttondel").disabled) {
                            document.getElementById("buttondel").disabled = true;
                        }
                        if (!document.getElementById("buttonsav").disabled) {
                            document.getElementById("buttonsav").disabled = true;
                        }
                    }
                } else if (saved == 1) {
                    if (check) {
                        if (document.getElementById("buttondel").disabled) {
                            document.getElementById("buttondel").disabled = false;
                        }
                        if (document.getElementById("buttonnor").disabled) {
                            document.getElementById("buttonnor").disabled = false;
                        }
                    } else {
                        if (!document.getElementById("buttondel").disabled) {
                            document.getElementById("buttondel").disabled = true;
                        }
                        if (!document.getElementById("buttonnor").disabled) {
                            document.getElementById("buttonnor").disabled = true;
                        }
                    }
                } else {
                    if (check) {
                        if (document.getElementById("buttonsav").disabled) {
                            document.getElementById("buttonsav").disabled = false;
                        }
                        if (document.getElementById("buttonnor").disabled) {
                            document.getElementById("buttonnor").disabled = false;
                        }
                    } else {
                        if (!document.getElementById("buttonsav").disabled) {
                            document.getElementById("buttonsav").disabled = true;
                        }
                        if (!document.getElementById("buttonnor").disabled) {
                            document.getElementById("buttonnor").disabled = true;
                        }
                    }
                }
            }
            //Function called when a button linked to the checkboxes is clicked and then treat the request
            function checkboxTreat(type, id) {
                var objCheckBoxes = document.getElementsByName("selectmessage");
                var post = "";
                var confirmMess = "";
                var succeedMess = "";
                var failedMess = "";
                if (type == "delete") {
                    confirmMess = "Do you really want to " + type + " the selected message(s) ?";
                    succeedMess = "The message(s) have/has been successfully put into the trash bin.";
                    failedMess = "The message(s) failed to be put into the trash bin !";
                } else if (type == "save") {
                    confirmMess = "Do you really want to " + type + " the selected message(s) ?";
                    succeedMess = "The message(s) have/has been successfully saved.";
                    failedMess = "The message(s) failed to be saved !";
                } else if (type == "normal") {
                    confirmMess = "Do you really want to put the selected message(s) in your inbox ?";
                    succeedMess = "The message(s) have/has been successfully returned to your inbox.";
                    failedMess = "The message(s) failed to be placed into your inbox !";
                } else if (type == "deletedraft") {
                    confirmMess = "Do you really want to delete the selected draft(s) ?";
                    succeedMess = "The draft(s) have/has been successfully deleted.";
                    failedMess = "The draft(s) failed to be deleted !";
                }
                if (id <= 0) {
                    if (confirm(confirmMess)) {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "true") {
                                    alert(succeedMess);
                                    location.reload();
                                } else {
                                    alert(failedMess);
                                }
                            }
                        }
                        for (var i = 0; i < objCheckBoxes.length; i++) {
                            if (objCheckBoxes[i].checked) {
                                post = post + objCheckBoxes[i].id + ",";
                            }
                        }
                        if (post != "") {
                            post = post.substr(0, post.length -1);
                        }
                        xmlhttp.open("POST","inbox_treat.php",true);
                        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xmlhttp.send("post="+post+"&type="+type);
                    }
                } else {
                    if (confirm(confirmMess)) {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "true") {
                                    alert(succeedMess);
                                    location.reload();
                                } else {
                                    alert(failedMess);
                                }
                            }
                        }
                        for (var i = 0; i < objCheckBoxes.length; i++) {
                            if (objCheckBoxes[i].checked) {
                                post = post + objCheckBoxes[i].id + ",";
                            }
                        }
                        if (post != "") {
                            post = post.substr(0, post.length -1);
                        }
                        xmlhttp.open("POST","inbox_treat.php",true);
                        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xmlhttp.send("post="+id+"&type="+type);
                    }
                }
            }
            //Function called when clicking the empty trash bin button, delete definitively the messages in the trash bin
            function emptyTrash(id) {
                if (confirm("Do you really want to empty your trash bin ?")) {
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText == "true") {
                                alert("Your trash bin has been emptied");
                                location.reload();
                            } else {
                                alert("Your trash bin failed to be emptied !");
                            }
                        }
                    }
                    xmlhttp.open("POST","empty_trash.php",true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("id="+id);
                }
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}