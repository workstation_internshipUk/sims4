<?php
include_once('connect_db.php');
if (isset($_POST['id'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$answer = $conn->prepare('UPDATE message SET is_deleted = 1 WHERE id_receiver = ?');
		$answer->execute(array($_POST['id']));
		echo 'true';
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>