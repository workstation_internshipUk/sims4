<?php
include('connect_db.php');
$page = 1;
$allLogs = "SELECT log.id, user.`name` , change_date, change_time, week, `year`, m.short_desc AS monday , t.short_desc AS tuesday, w.short_desc AS wednesday, th.short_desc AS thursday , f.short_desc AS friday, sa.short_desc AS saturday, su.short_desc AS sunday FROM (((((((`shift_logs` log INNER JOIN `shift_type` m ON log.mon = m.id) INNER JOIN `shift_type` t ON log.tue = t.id)INNER JOIN `shift_type` w ON log.wed = w.id) INNER JOIN `shift_type` th ON log.thu = th.id) INNER JOIN `shift_type` f ON log.fri = f.id) INNER JOIN `shift_type` sa ON log.sat = sa.id) INNER JOIN `shift_type` su ON log.sun = su.id) INNER JOIN `user` ON `user`.id = log.id_user";


if (isset($_POST['id'])) {
  $_POST['id'] = (int) $_POST['id'];
  if ($_POST['id'] > 0) {
						 $countLogs = $conn->prepare('SELECT COUNT(*) AS count_logs FROM shift_logs INNER JOIN user ON shift_logs.id_user = 												user.id WHERE shift_logs.id_user = ?');		
                                            $countLogs->execute(array($_POST['id']));
                                            if ($count = $countLogs->fetch()) {
                                                $totalLogs = $count['count_logs'];
                                            } else {
                                                $totalLogs = 0;
                                            }
                                            $countLogs->closeCursor();
                                            $nbLogPerPage = 50;
                                            $nbPages = ceil($totalLogs / $nbLogPerPage);

                                            if (!isset($_POST['page'])) {
                                                if (isset($_POST['order'])) {
                                                   
													    if ($_POST['order'] == "name") {
                                                        $answer = $conn->prepare($allLogs.' WHERE log.id_user = ? ORDER BY user.name 															ASC,log.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
  														} else if ($_POST['order'] == "date_time") {
                                                        $answer = $conn->prepare($allLogs.' WHERE log.id_user = ?
														 ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                      } else {
                                                        $answer = $conn->prepare($allLogs.' WHERE log.id_user = ? ORDER BY 															log.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
														}
                                           
												} else { $answer = $conn->prepare($allLogs. ' WHERE log.id_user = ? ORDER BY 															 log.id DESC LIMIT ' . $nbLogPerPage);
														 $answer->execute(array($_POST['id']));
                                                		}
                                            } else {
											$_POST['page'] = (int) $_POST['page'];
                                            if ($_POST['page'] > 0) {
                                                $page = $_POST['page'];
												$firstLog = ($_POST['page'] -1) * $nbLogPerPage;
                                                    if (isset($_POST['order'])) {

                                                        if ($_POST['order'] == "name") {
                                                            $answer = $conn->prepare($allLogs. ' WHERE log.id_user = ? ORDER BY 															user.name ASC, log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "date_time") {
                                                            $answer = $conn->prepare($allLogs. ' WHERE log.id_user = ?
															ORDER BY log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));                    
                                                        } else {
                                                            $answer = $conn->prepare($allLogs. ' WHERE log.id_user = ? ORDER BY 																log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        }

												 	} else {
                                                        $answer = $conn->prepare($allLogs. ' WHERE log.id_user = ? ORDER BY 															log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    }
										} else {//alternative to post page
											if (isset($_POST['order'])) {
                                                        
													      if ($_POST['order'] == "name") {
                                                           $answer = $conn->prepare($allLogs. 'WHERE log.id_user = ? ORDER BY user.name 															ASC, log.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        } else if ($_POST['order'] == "date_time") {
                                                            $answer = $conn->prepare($allLogs. 'WHERE log.id_user = ? 
															ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                         }
														 else {
                                                            $answer = $conn->prepare($allLogs.' WHERE log.id_user = ? ORDER BY 																log.id DESC LIMIT ' . $nbLogPerPage);
                                                            $answer->execute(array($_POST['id']));
                                                        }
                                              } else {
                                                        $answer = $conn->prepare($allLogs.' WHERE log.id_user = ? ORDER BY 															log.id DESC LIMIT ' . $nbLogPerPage);
                                                        $answer->execute(array($_POST['id']));
                                                    } // end of isset($_POST['order'])
                            					}//end of $_POST['page'] > 0
										} // end of !isset post page
									} // end of the id>0 case

	else if (isset($_POST['order'])) {
                                            $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM shift_logs INNER JOIN user ON 												shift_logs.id_user = user.id');
                                            if ($count = $countLogs->fetch()) {
                                                $totalLogs = $count['count_logs'];
                                            } else {
                                                $totalLogs = 0;
                                            }
                                            $countLogs->closeCursor();
                                            $nbLogPerPage = 50;
                                            $nbPages = ceil($totalLogs / $nbLogPerPage);
									if (!isset($_POST['page'])) {
                                                if ($_POST['order'] == "name") {
                                         $answer = $conn->query($allLogs.' ORDER BY user.name ASC, log.id DESC LIMIT ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "date_time") {
                                                    $answer = $conn->query($allLogs.'  ORDER BY 														log.id DESC LIMIT ' . $nbLogPerPage);

                                                } else {
                                                    $answer = $conn->query($allLogs.' ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                }
                                            }
									else {
                                           $_POST['page'] = (int) $_POST['page'];
                                           if ($_POST['page'] > 0) {
                                            $page = $_POST['page'];
                                               $firstLog = ($_POST['page'] -1) * $nbLogPerPage;
                                                    if ($_POST['order'] == "name") {
                                                      $answer = $conn->query($allLogs . ' ORDER BY user.name ASC, log.id DESC LIMIT ' . 													  $firstLog . ', ' . $nbLogPerPage);
                                                    } else if ($_POST['order'] == "date_time") {
                                                        $answer = $conn->query($allLogs .'  ORDER BY 															log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                    }
											} else {
                                                    if ($_POST['order'] == "name") {
                                                        $answer = $conn->query($allLogs.' ORDER BY user.name ASC, log.id DESC LIMIT ' . 														$nbLogPerPage);
                                                    } else if ($_POST['order'] == "date_time") {
                                                        $answer = $conn->query($allLogs .' ORDER BY 															log.id DESC LIMIT ' . $nbLogPerPage);
                                                    } else {
                                                        $answer = $conn->query($allLogs.' ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                    }
											}
										}
							        }// end of the id case*****************************************************************


else if (isset($_POST['order'])) {
                                        $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM shift_logs INNER JOIN user ON 											shift_logs.id_user = user.id');
                                        if ($count = $countLogs->fetch()) {
                                            $totalLogs = $count['count_logs'];
                                        } else {
                                            $totalLogs = 0;
                                        }
                                        $countLogs->closeCursor();
                                        $nbLogPerPage = 50;
                                        $nbPages = ceil($totalLogs / $nbLogPerPage);
                                        if (!isset($_POST['page'])) {
                                            if ($_POST['order'] == "name") {
                                                $answer = $conn->query($allLogs. ' ORDER BY user.name ASC, log.id DESC LIMIT ' . 													$nbLogPerPage);
                                            } else if ($_POST['order'] == "date_time") {
                                                $answer = $conn->query($allLogs. 'ORDER BY 															    log.id DESC LIMIT ' . $nbLogPerPage);
                                            } else {
                                                $answer = $conn->query($allLogs .' ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                            }
                                        }

									  else {
                                            $_POST['page'] = (int) $_POST['page'];
                                            if ($_POST['page'] > 0) {
                                                $page = $_POST['page'];
                                                $firstLog = ($_POST['page'] -1) * $nbLogPerPage;
                                                if ($_POST['order'] == "name") {
                                                    $answer = $conn->query($allLogs .' ORDER BY user.name ASC, log.id DESC LIMIT ' . 														$firstLog . ', ' . $nbLogPerPage);
                                                } else if ($_POST['order'] == "date_time") {
                                                    $answer = $conn->query($allLogs.' ORDER BY 															log.id DESC LIMIT ' . $firstLog . ', ' . $nbLogPerPage);
                                                }  else {
                                                    $answer = $conn->query($allLogs.' ORDER BY log.id DESC LIMIT ' . $firstLog . ', ' . 													$nbLogPerPage);
                                                }
                                            } else {
                                                if ($_POST['order'] == "name") {
                                                    $answer = $conn->query($allLogs .' ORDER BY user.name ASC, log.id DESC LIMIT ' . 														$nbLogPerPage);
                                                } else if ($_POST['order'] == "date_time") {
                                                    $answer = $conn->query($allLogs .'ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                }  else {
                                                    $answer = $conn->query($allLogs .' ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                                }
                                            }
                                        }
                                    } // end of the order case ***********************************

else {
                                        $countLogs = $conn->query('SELECT COUNT(*) AS count_logs FROM shift_logs INNER JOIN user ON 											shift_logs.id_user = user.id');
                                        if ($count = $countLogs->fetch()) {
                                            $totalLogs = $count['count_logs'];
                                        } else {
                                            $totalLogs = 0;
                                        }
                                        $countLogs->closeCursor();
                                        $nbLogPerPage = 50;
                                        $nbPages = ceil($totalLogs / $nbLogPerPage);
                                        
										if (!isset($_POST['page'])) {
                                            $answer = $conn->query($allLogs .' ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                        } else {
                                            $_POST['page'] = (int) $_POST['page'];
                                            if ($_POST['page'] > 0) {
                                                $page = $_POST['page'];
                                                $firstLog = ($_POST['page'] -1) * $nbLogPerPage;
                                                $answer = $conn->query($allLogs .'ORDER BY log.id DESC LIMIT ' . $firstLog . ', ' . 												$nbLogPerPage);
                                            } else {
                                                $answer = $conn->query($allLogs .'ORDER BY log.id DESC LIMIT ' . $nbLogPerPage);
                                            }
                                        }
                                    }
}

							

							
											


/*


$answer = $conn->prepare('SELECT log.id, user.`name` , change_date, change_time, week, `year`, m.description AS monday , t.description AS tuesday, w.description AS wednesday, th.description AS thursday , f.description AS friday, sa.description AS saturday, su.description AS sunday FROM (((((((`shift_logs` log INNER JOIN `shift_type` m ON log.mon = m.id) INNER JOIN `shift_type` t ON log.tue = t.id)INNER JOIN `shift_type` w ON log.wed = w.id) INNER JOIN `shift_type` th ON log.thu = th.id) INNER JOIN `shift_type` f ON log.fri = f.id) INNER JOIN `shift_type` sa ON log.sat = sa.id) INNER JOIN `shift_type` su ON log.sun = su.id) INNER JOIN `user` ON `user`.id = log.id_user');
$answer->execute();*/

								//Display the different pages
                                    echo '<ul class="pagination">';
                                    if ($nbPages < 20) {
                                        for ($i = 1 ; $i <= $nbPages ; $i++) {
                                            if ($page != $i) {
                                                echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                            } else {
                                                echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                            }
                                        }
                                    } else {
                                        if (!isset($_POST['page'])) {
                                            for ($i = 1 ; $i <= 5 ; $i++) {
                                                if ($page != $i) {
                                                    echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                } else {
                                                    echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                }
                                            }
                                            echo '<li class="disabled"><a href="#">...</a></li>';
                                            for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                                if ($page != $i) {
                                                    echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                } else {
                                                    echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                }
                                            }
                                        } else {
                                            $_POST['page'] = (int) $_POST['page'];
                                            if ($_POST['page'] < 4 || $_POST['page'] > $nbPages-3) {
                                                for ($i = 1 ; $i <= 5 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-4 ; $i <= $nbPages ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else if ($_POST['page'] != 4 && $_POST['page'] != $nbPages - 3) {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $_POST['page']-2 ; $i <= $_POST['page']+2 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else if ($_POST['page'] == 4) {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                for ($i = $_POST['page']-1 ; $i <= $_POST['page']+2 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            } else {
                                                for ($i = 1 ; $i <= 2 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                echo '<li class="disabled"><a href="#">...</a></li>';
                                                for ($i = $_POST['page']-2 ; $i <= $_POST['page']+1 ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                                for ($i = $nbPages-1 ; $i <= $nbPages ; $i++) {
                                                    if ($page != $i) {
                                                        echo '<li><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    } else {
                                                        echo '<li class="active"><a href="#" onclick="writeTable(2,' . $i . ')">' . $i . '</a></li>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    echo '</ul>';
                                    ?>

<table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Date Of Change</th>
                                                <th>Time Of Change</th>
                                                <th>Week</th>
                                                <th>Year</th>
                                                <th>Mon</th>
												<th>Tue</th>
												<th>Wed</th>
												<th>Thu</th>
												<th>Fri</th>
												<th>Sat</th>
												<th>Sun</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($data = $answer->fetch()) {
                                                echo '<tr>';
                                                    echo '<td>' . $data['id'] . '</td>';
						  						    echo '<td>' . $data['name'] . '</td>';
													echo '<td>' . $data['change_date'] . '</td>';
													echo '<td>' . $data['change_time'] . '</td>';
													echo '<td>' . $data['week'] . '</td>';
													echo '<td>' . $data['year'] .'</td>';
													echo '<td>' . $data['monday'] . '</td>';
													echo '<td>' . $data['tuesday'] . '</td>';
													echo '<td>' . $data['wednesday'] . '</td>';
													echo '<td>' . $data['thursday'] . '</td>';
													echo '<td>' . $data['friday'] . '</td>';
													echo '<td>' . $data['saturday'] . '</td>';
													echo '<td>' . $data['sunday'] . '</td>';
                                              	    echo '</tr>';
                                            }
                                            $answer->closeCursor();
                                            ?>
                                        </tbody>
                                    </table>


