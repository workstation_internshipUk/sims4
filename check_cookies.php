<?php
include_once('connect_db.php');
include_once("class/user.php");
session_start();
//If the user is on the index
if (endsWith($_SERVER['REQUEST_URI'], "index.php") || endsWith($_SERVER['REQUEST_URI'], "sims4/") || endsWith($_SERVER['REQUEST_URI'], "sims4")) {
	//If there is a session set
	if (isset($_SESSION['user'])) {
		$answer = $conn->prepare('SELECT connected FROM user WHERE id = ?');
		$answer->execute(array($_SESSION['user']->id()));
		if ($data = $answer->fetch()) {
			if ($data['connected'] == 1) {
				if ($_SESSION['user']->pass_changed() == 1) {
					header('Location: home.php');
					exit;
				} else {
					header('Location: change_pass.php');
					exit;
				}
			} else {
				$_SESSION['user'] = "";
				$_SESSION = array();
				setcookie('name', '', time(), null, null, false, true);
				setcookie('password', '', time(), null, null, false, true);
				session_destroy();
			}
		} else {
			$_SESSION['user'] = "";
			$_SESSION = array();
			setcookie('name', '', time(), null, null, false, true);
			setcookie('password', '', time(), null, null, false, true);
			session_destroy();
		}
		$answer->closeCursor();
	} else {
		//If cookies are set
		if (isset($_COOKIE['name']) && isset($_COOKIE['password'])) {
			$answer = $conn->prepare('SELECT * FROM `user` WHERE name = ? AND password = ?');
			$answer->execute(array(htmlspecialchars($_COOKIE['name']),htmlspecialchars($_COOKIE['password'])));
			//If the datas stored in the cookies correspond to a user
			if ($data = $answer->fetch()) {
				if ($data['connected'] == 1) {
					//Store the session
					$_SESSION['user'] = new User([
	                                    'id'=>$data["id"],
	                                    'name'=>$data["name"],
	                                    'password'=>$data["password"],
	                                    'feeling'=>$data["feeling"],
	                                    'picture'=>$data["picture"],
	                                    'level'=>$data["level"],
	                                    'salt'=>$data["salt"],
	                                    'id_group'=>$data["id_group"],
	                                    'start_date'=>$data["start_date"],
	                                    'end_date'=>$data["end_date"],
	                                    'special_days'=>$data["special_days"],
	                                    'user_description'=>$data["user_description"],
	                                    'hr_comment'=>$data["hr_comment"],
	                                    'pass_changed'=>$data["pass_changed"]
	                                    ]);
					if ($data['pass_changed'] == 1) {
						header('Location: home.php');
						exit;
					} else {
						header('Location: change_pass.php');
						exit;
					}
				} else {
					setcookie('name', '', time(), null, null, false, true);
					setcookie('password', '', time(), null, null, false, true);
				}
			} else {
				setcookie('name', '', time(), null, null, false, true);
				setcookie('password', '', time(), null, null, false, true);
			}
			$answer->closeCursor();
		}
	}
} else {
	//If there is no session set
	if (!(isset($_SESSION['user']))) {
		//If cookies are set
		if (isset($_COOKIE['name']) && isset($_COOKIE['password'])) {
			$answer = $conn->prepare('SELECT * FROM `user` WHERE name = ? AND password = ?');
			$answer->execute(array(htmlspecialchars($_COOKIE['name']),htmlspecialchars($_COOKIE['password'])));
			//If the datas stored in the cookies correspond to a user
			if ($data = $answer->fetch()) {
				if ($data['connected'] == 1) {
					//Store the session
					$_SESSION['withcookies'] = "true";
					$_SESSION['user'] = new User([
	                                    'id'=>$data['id'],
	                                    'name'=>$data['name'],
	                                    'password'=>$data['password'],
	                                    'feeling'=>$data['feeling'],
	                                    'picture'=>$data['picture'],
	                                    'level'=>$data['level'],
	                                    'salt'=>$data['salt'],
	                                    'id_group'=>$data['id_group'],
	                                    'start_date'=>$data['start_date'],
	                                    'end_date'=>$data['end_date'],
	                                    'special_days'=>$data['special_days'],
	                                    'user_description'=>$data['user_description'],
	                                    'hr_comment'=>$data['hr_comment'],
	                                    'pass_changed'=>$data['pass_changed']
	                                    ]);
					if ($data['pass_changed'] == 0) {
						$answer->closeCursor();
						header('Location: change_pass.php');
						exit;
					} else {
						$answer->closeCursor();
						header('Location: home.php');
						exit;
					}
				} else {
					$answer->closeCursor();
					setcookie('name', '', time(), null, null, false, true);
					setcookie('password', '', time(), null, null, false, true);
					header('Location: index.php');
					exit;
				}
			} else {
				$answer->closeCursor();
				setcookie('name', '', time(), null, null, false, true);
				setcookie('password', '', time(), null, null, false, true);
				header('Location: index.php');
				exit;
			}
		} else {
			header('Location: index.php');
			exit;
		}
	} else {
		$answer = $conn->prepare('SELECT connected FROM user WHERE id = ?');
		$answer->execute(array($_SESSION['user']->id()));
		if ($data = $answer->fetch()) {
			if ($data['connected'] == 0) {
				$_SESSION['user'] = "";
				$_SESSION = array();
				setcookie('name', '', time(), null, null, false, true);
				setcookie('password', '', time(), null, null, false, true);
				session_destroy();
				$answer->closeCursor();
				header('Location: index.php');
				exit;
			}
		} else {
			$_SESSION['user'] = "";
			$_SESSION = array();
			setcookie('name', '', time(), null, null, false, true);
			setcookie('password', '', time(), null, null, false, true);
			session_destroy();
			$answer->closeCursor();
			header('Location: index.php');
			exit;
		}
		$answer->closeCursor();
	}
}
?>