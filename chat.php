<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); refresh(); refreshChat();">
        <?php 
        $selected = "Chat";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Chat</div>
                                <div class="panel-body">
                                    <form method="post" action="#" onsubmit="return chatSend('<?php echo $_SESSION['user']->id() . '\',\'' . $_SESSION['user']->name() ?>')">
                                        Say : 
                                        <input type="text" name="MESSAGE" id="messagechat">
                                        <input type="submit" id="chatbutton" value="Submit">
                                    </form>
                                    <br />
                                    <?php
                                    include('chat_body.php');
                                    ?>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called onsubmit to send the message
            function chatSend(id, name) {
                var message = document.getElementById("messagechat").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("messagechat").value = "";
                        $("#chatbody").load("chat_body.php");
                        return false;
                    }
                }
                xmlhttp.open("POST","chat_treat.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("id="+id+"&name="+name+"&message="+message);
                return false;
            }
            //Refresh the Chat content every 5 seconds
            function refreshChat() {
                setInterval(function() {
                    $("#chatbody").load("chat_body.php");
                }, 5000);
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>