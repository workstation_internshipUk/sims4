<?php 
include("check_cookies.php");
checkPermissions(6,3);
require_once'class/user.php';
require_once'class/usermanager.php';
if (isset($_GET["uid"])) {
    $uid = (int) $_GET["uid"];
    if ($uid < 1) {
        header("Location:staff.php");
    }
} else {
    header("Location:staff.php");
}
if (isset($_POST["uid"]) && isset($_POST["id_group"]) && isset($_POST["level"]) && isset($_POST["user_description"]) && isset($_POST["sdate"]) && isset($_POST["edate"]) && isset($_POST["special_days"]) && isset($_POST["hr_comment"])) {
    $id = (int) $_POST["uid"];
    $id_group = (int) $_POST["id_group"];
    $level = (int) $_POST["level"];
    $user_description = htmlspecialchars($_POST["user_description"]);
    $start_date = htmlspecialchars($_POST["sdate"]);
    $end_date = htmlspecialchars($_POST["edate"]);
    $special_days = (int) $_POST["special_days"];
    $hr_comment = htmlspecialchars($_POST["hr_comment"]);

    $sday = substr($start_date,0,2);
    $smonth = substr($start_date,3,2);
    $syear = substr($start_date,6,4);

    $eday = substr($end_date,0,2);
    $emonth = substr($end_date,3,2);
    $eyear = substr($end_date,6,4);

    $sdate = date_create($syear."-".$smonth."-".$sday);
    $edate = date_create($eyear."-".$emonth."-".$eday);
    //Check the data from the form
    if (($sdate < $edate) && ($id_group > 0) && (($level > 0) && ($level < 11)) && (($special_days < 11) && ($special_days > -11))) {
        //Update the user
        $user = new User([
            "id"=>$id,
            "level"=>$level,
            "id_group"=>$id_group,
            "start_date"=>$sdate->format('Y-m-d'),
            "end_date"=>$edate->format('Y-m-d'),
            "special_days"=>$special_days,
            "user_description"=>$user_description,
            "hr_comment"=>$hr_comment
            ]);
        $umanager = new UserManager($conn);
        $user = $umanager->update($user);
    }
} else if (isset($uid)) {
    //Retrieve the informations about a user for update
    if ($uid > 0) {
        $manager = new UserManager($conn);
        $user = $manager->get($uid);
        $sdate = $user->start_date();
        $edate = $user->end_date();
    } else {
        header("Location:staff.php");
    }
} else {
    header("Location:staff.php");
}
$answer0 = $conn->query('SELECT id FROM `group`');
while ($data0 = $answer0->fetch()) {
    if (isset($_POST['permission_level' . $data0['id']])) {
        $_POST['permission_level' . $data0['id']] = (int) $_POST['permission_level' . $data0['id']];
        $answer1 = $conn->prepare('SELECT id FROM permissions WHERE id_user = ? AND id_group = ?');
        $answer1->execute(array($uid, $data0['id']));
        if ($data1 = $answer1->fetch()) {
            $mod = $conn->prepare('UPDATE permissions SET level = ? WHERE id = ?');
            $mod->execute(array($_POST['permission_level' . $data0['id']], $data1['id']));
        } else {
            $add = $conn->prepare('INSERT INTO permissions (id_user, id_group, level) VALUES (?, ?, ?)');
            $add->execute(array($uid, $data0['id'], $_POST['permission_level' . $data0['id']]));
        }
        $answer1->closeCursor();
    }
}
$answer0->closeCursor();
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- Load jQuery from Google s CDN -->
        <!-- Load jQuery UI CSS  -->
        <!-- Load jQuery JS -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <!-- Load jQuery UI Main JS  -->
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>    
    </head>

    <body onload="hitByUnicorn(); refresh();">
        <?php
        $selected = "Users";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Settings</div>
                                <div class="panel-body">
                                    <form action="administration.php?uid=<?php echo $uid; ?>" method="POST" class="form-horizontal">
                                        <input type="hidden" value='<?php echo $uid; ?>' name="uid" id="uid" >
                                        <?php
                                        if (isset($umanager)) {
                                            if ($umanager->success() == true) {      
                                                echo '<div class="alert alert-success ">
                                                        <button class="close" data-dismiss="alert"></button>
                                                        The changes have been saved!
                                                    </div>';
                                            } else if ($umanager->success() == false) {
                                                echo '<div class="alert alert-danger">
                                                    <button class="close" data-dismiss="alert"></button>
                                                    The changes have not been saved. Please try again.
                                                    </div>';
                                            }
                                        }
                                        ?>
                                        <fieldset>
                                            <legend>SIMS settings for <?php echo $user->name(); ?></legend>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Group<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="id_group">
                                                            <?php
                                                            $answer = $conn->query('SELECT * FROM `group`');
                                                            while ($data = $answer->fetch()) {
                                                                echo '<option value="' . $data['id'] . '" ';
                                                                isSelectedGroup($user,$data['id']);
                                                                echo '>' . $data['name'] . '</option>';
                                                            }
                                                            $answer->closeCursor();
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $answer0 = $conn->query('SELECT * FROM `group`');
                                            while ($data0 = $answer0->fetch()) {
                                                $answer1 = $conn->prepare('SELECT level FROM permissions WHERE id_user = ? AND id_group = ?');
                                                $answer1->execute(array($uid, $data0['id']));
                                                $level = 0;
                                                if ($data1 = $answer1->fetch()) {
                                                    $level = $data1['level'];
                                                }
                                                $answer1->closeCursor();
                                                ?>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-md-2"><?php echo $data0['name']; ?> Permission</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="permission_level<?php echo $data0['id']; ?>">
                                                                <option value="0" >Level 0</option>
                                                                <option value="1" <?php if ($level == 1) { echo 'selected'; } ?>>Level 1</option>
                                                                <option value="2" <?php if ($level == 2) { echo 'selected'; } ?>>Level 2</option>
                                                                <option value="3" <?php if ($level == 3) { echo 'selected'; } ?>>Level 3</option>
                                                                <option value="4" <?php if ($level == 4) { echo 'selected'; } ?>>Level 4</option>
                                                                <option value="5" <?php if ($level == 5) { echo 'selected'; } ?>>Level 5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            $answer0->closeCursor();
                                            ?>
                                        </fieldset>
                                        <fieldset>
                                            <legend>User Information</legend>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Status Level<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="level">
                                                            <option value="1" <?php isSelectedLevel($user,"1");?>>Level 1</option>
                                                            <option value="2" <?php isSelectedLevel($user,"2");?>>Level 2</option>
                                                            <option value="3" <?php isSelectedLevel($user,"3");?>>Level 3</option>
                                                            <option value="4" <?php isSelectedLevel($user,"4");?>>Level 4</option>
                                                            <option value="5" <?php isSelectedLevel($user,"5");?>>Level 5</option>
                                                            <option value="6" <?php isSelectedLevel($user,"6");?>>Level 6</option>
                                                            <option value="7" <?php isSelectedLevel($user,"7");?>>Level 7</option>
                                                            <option value="8" <?php isSelectedLevel($user,"8");?>>Level 8</option>
                                                            <option value="9" <?php isSelectedLevel($user,"9");?>>Level 9</option>
                                                            <option value="10" <?php isSelectedLevel($user,"10");?>>Level 10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="textarea2">User description:<br><small>Max:255 characters</small></label>
                                                    <div class="col-md-6">
                                                        <textarea name="user_description" class="form-control" rows="5" placeholder="Enter text ..."><?php echo $user->user_description(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend>Administration of working time</legend>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="sdate">Start Date<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" value='<?php echo date("d-m-Y",strtotime($user->start_date()));?>' class="form-control datepicker" name="sdate" id="sdate" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="edate">End Date<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" value='<?php echo date("d-m-Y",strtotime($user->end_date()));?>' class="form-control datepicker" name="edate" id="edate" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2">Special Days<span class="required">*</span></label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="special_days">
                                                            <option value="0">0</option>
                                                            <option value="-10" <?php if ($user->special_days() == "-10") { echo "selected"; } ?>>-10</option>
                                                            <option value="-9" <?php if ($user->special_days() == "-9") { echo "selected"; } ?>>-9</option>
                                                            <option value="-8" <?php if ($user->special_days() == "-8") { echo "selected"; } ?>>-8</option>
                                                            <option value="-7" <?php if ($user->special_days() == "-7") { echo "selected"; } ?>>-7</option>
                                                            <option value="-6" <?php if ($user->special_days() == "-6") { echo "selected"; } ?>>-6</option>
                                                            <option value="-5" <?php if ($user->special_days() == "-5") { echo "selected"; } ?>>-5</option>
                                                            <option value="-4" <?php if ($user->special_days() == "-4") { echo "selected"; } ?>>-4</option>
                                                            <option value="-3" <?php if ($user->special_days() == "-3") { echo "selected"; } ?>>-3</option>
                                                            <option value="-2" <?php if ($user->special_days() == "-2") { echo "selected"; } ?>>-2</option>
                                                            <option value="-1" <?php if ($user->special_days() == "-1") { echo "selected"; } ?>>-1</option>
                                                            <option value="1" <?php if ($user->special_days() == "1") { echo "selected"; } ?>>1</option>
                                                            <option value="2" <?php if ($user->special_days() == "2") { echo "selected"; } ?>>2</option>
                                                            <option value="3" <?php if ($user->special_days() == "3") { echo "selected"; } ?>>3</option>
                                                            <option value="4" <?php if ($user->special_days() == "4") { echo "selected"; } ?>>4</option>
                                                            <option value="5" <?php if ($user->special_days() == "5") { echo "selected"; } ?>>5</option>
                                                            <option value="6" <?php if ($user->special_days() == "6") { echo "selected"; } ?>>6</option>
                                                            <option value="7" <?php if ($user->special_days() == "7") { echo "selected"; } ?>>7</option>
                                                            <option value="8" <?php if ($user->special_days() == "8") { echo "selected"; } ?>>8</option>
                                                            <option value="9" <?php if ($user->special_days() == "9") { echo "selected"; } ?>>9</option>
                                                            <option value="10" <?php if ($user->special_days() == "10") { echo "selected"; } ?>>10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-md-2" for="textarea2">HR comments:<br>Max:255 characters</label>
                                                    <div class="col-md-6">
                                                        <textarea name="hr_comment" class="form-control" rows="5" placeholder="Enter text ..."><?php echo $user->hr_comment(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-offset-2 col-md-6">
                                                        <button type="submit" class="btn btn-primary btn-block">Validate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="assets/form-validation.js"></script>
        <script>
            //Wizard that manages the form
            jQuery(document).ready(function() {   
                FormValidation.init();
            });
            $(function() {
                $(".datepicker").datepicker();
            });
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>