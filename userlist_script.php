<?php
include('connect_db.php');
$id = "0";
$order = "level";

if (isset($_POST['id'])) {
	$id = (int) $_POST['id'];
}
if (isset($_POST['order'])) {
	$order = $_POST['order'];
}
$query = 'SELECT user.id AS id, user.name AS name, level, start_date, end_date, connected, group.name AS groupname, group.id AS groupid FROM user INNER JOIN `group` ON `user`.id_group = `group`.id ';

if ($id != "0") {
	$answer = $conn->prepare($query . " WHERE user.id = ? AND deleted = 0");
	$answer->execute(array($id));
} else if ($order != "") {
    switch ($order){
        case "start_date" :
            $strsuite = ", start_date DESC, user.name ASC";
            break;
        case "end_date" :
            $strsuite = ", end_date DESC, user.name ASC";
            break;
        case "level" :
            $strsuite = ", level DESC, user.name ASC";
            break;
        default :
            $strsuite = ", user.name ASC";
        
    }
        $answer = $conn->prepare($query . " WHERE deleted = 0 ORDER BY group.name ASC" . $strsuite);
        $answer->execute();
	
} else {
	$answer = $conn->prepare($query . " WHERE deleted = 0 ORDER BY group.name DESC");
	$answer->execute();

}
?>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Level</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Log in</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $currentgroup = "";
        while ($data = $answer->fetch()) {
        	if ($currentgroup != $data['groupname']) {
        		$currentgroup = $data['groupname'];
        		echo '<tr class="text-primary">';
            		echo '<td ><strong> ' . $data['groupname'] . '</strong></td>';
            		echo '<td></td><td></td><td></td><td></td>';
                echo '</tr>';
        	}
            echo '<tr>';
                echo '<td>' . $data['name'] . '</td>';
                echo '<td>' . $data['level'] . '</td>';
                echo '<td>' . $data['start_date'] . '</td>';
                echo '<td>' . $data['end_date'] . '</td>';
                echo '<td>' . $data['connected'] . '</td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>
<?php
$conn = null;
?> 