<?php
if (isset($_POST["type"])) {
	$type = $_POST["type"];
	require_once 'connect_db.php';
	require_once 'class/user.php';
	require_once 'class/usermanager.php';
	if($type == "changepass"){
		$id = (int) $_POST['id'];
		if ($id > 0) {
			$pass = $_POST['pass'];
			$manager = new UserManager($conn);
			$manager->update_pass($id,$pass);
			if ($manager->success() == true) {
				echo '<button class="close" data-dismiss="alert"></button>
		                The changes have been saved!';
			} else {
				echo '<button class="close" data-dismiss="alert"></button>
						The changes have not been saved. Please try again.';
			}
		}
	} else if ($type == "delete") {
		$id = (int) $_POST['id'];
		if ($id > 0) {
			$manager = new UserManager($conn);
			$manager->delete($id);
			echo $manager->success();
		}
	}
}
if (isset($conn)) {
	$conn = null;
}
?>