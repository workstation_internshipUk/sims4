<?php
include_once('check_cookies.php');
$disconnection = $conn->prepare('UPDATE user SET connected = 0 WHERE id = ?');
$disconnection->execute(array($_SESSION['user']->id()));
if ($_SESSION['user']->name() != "Dino" && $_SESSION['user']->name() != "Celine" && $_SESSION['user']->name() != "Cheryl1" && $_SESSION['user']->name() != "SebastienC" && $_SESSION['user']->name() != "TomNun") {
	$log = $conn->prepare('INSERT INTO connection_logs (id_user, type, connection_date, connection_time, ip_address) VALUES (?, "disconnection", ?, ?, ?)');
	$log->execute(array($_SESSION['user']->id(), date('Y-m-d'), date('H:i:s'), $_SERVER['REMOTE_ADDR']));
}
if (isset($conn)) {
	$conn = null;
}
$_SESSION['user'] = "";
$_SESSION = array();
setcookie('name', '', time(), null, null, false, true);
setcookie('password', '', time(), null, null, false, true);
session_destroy();
header("Location: index.php");
?>