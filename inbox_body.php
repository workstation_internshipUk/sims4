                                <?php
                                include_once('check_cookies.php');
                                ?>
                                <div id="inboxbody">
                                    <?php
                                    include_once('connect_db.php');
                                    $countMessage = $conn->prepare('SELECT COUNT(*) AS count_message FROM message WHERE id_receiver = ? AND is_read = 0');
                                    $countMessage->execute(array($_SESSION['user']->id()));
                                    if ($data = $countMessage->fetch()) {
                                        if ($data['count_message'] > 0) {
                                            echo '<script>document.getElementsByTagName("title")[0].innerHTML = "' . $data['count_message'] . ' NEW SIMS - SIMS 4 - Staff Information Management System"</script>';
                                            echo '<button class="btn btn-primary" onClick="window.open(\'read_sim.php\',\'mywindow\',\'width=600px,height=600px\').focus()" style="width :100% ">You have<strong> ' . $data['count_message'] . ' </strong>new sims</button><br /><br />';
                                        } else {
                                            echo '<script>document.getElementsByTagName("title")[0].innerHTML = "SIMS 4 - Staff Information Management System"</script>';
                                            echo 'You have<strong> 0 </strong>new sims<br /><br />';
                                        }
                                    } else {
                                        echo 'You have<strong> 0 </strong>new sims<br /><br />';
                                    }
                                    $countMessage->closeCursor();
                                    ?>
                                    <button class="btn btn-primary btn-block" onclick="window.open('send_sim.php','sendSim', 'width=1250px,height=800px').focus()">Send Sims</button><br />
                                    <a href="#" onclick="deleteSims(<?php echo $_SESSION['user']->id(); ?>)"><button class="btn btn-primary btn-block">Delete All Sims</button></a>
                                </div>