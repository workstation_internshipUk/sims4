<?php
include_once('connect_db.php');
require_once'class/usermanager.php';
require_once'class/user.php';
$manager = new UserManager($conn);
if (isset($_POST['page']) && isset($_POST['nbentries']) && isset($_POST['searchuser']) && isset($_POST['department'])) {
    $_POST['page'] = (int) $_POST['page'];
    $_POST['nbentries'] = (int) $_POST['nbentries'];
    if ($_POST['nbentries'] > 0 && $_POST['page'] > 0) {
        $users = $manager->getList(htmlspecialchars($_POST['searchuser']), htmlspecialchars($_POST['department']));
        $nbUsers = count($users);
        $firstUser = ($_POST['page'] - 1) * $_POST['nbentries'];
        ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="example">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Departement</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = $firstUser; $i < ($firstUser + $_POST['nbentries']); $i++) {
                    if ($i >= $nbUsers) {
                        break;
                    }
                    $answer = $conn->prepare('SELECT name FROM `group` WHERE id = ?');
                    $answer->execute(array($users[$i]->id_group()));
                    echo '<tr id="' . $users[$i]->id() . '"class="odd gradeX">';
                    echo '<td><a href="administration.php?uid=' . $users[$i]->id() . '" target="_blank">' . $users[$i]->name() . '</a></td>';
                    echo '<td>' . $users[$i]->start_date() . '</td>';
                    echo '<td>' . $users[$i]->end_date() . '</td>';
                    if ($data = $answer->fetch()) { echo '<td>' . $data['name'] . '</td>'; } else { echo '<td></td>'; }
                    echo '<td><a class="passuser" data-toggle="modal" data-id="' . $users[$i]->id() . '" id="user' . $users[$i]->id() . '" href="#ChangePassword">Change password</a></td>';
                    echo '<td><a class="deleteuser" data-id="' . $users[$i]->id() . '" data-name="' . $users[$i]->name() . '" href="#delete" data-toggle="modal">Delete</a></td>';
                    $answer->closeCursor();
                }
                ?>
            </tbody>
        </table>
        <?php
        $nbPages = ceil($nbUsers / $_POST['nbentries']);
        echo '<ul class="pagination pull-right">';
        for ($i = 1; $i <= $nbPages; $i++) {
            if ($_POST['page'] != $i) {
                echo '<li><a href="#" onclick="writeTable(' . $i .  ');">' . $i . '</a>';
            } else {
                echo '<li class="active"><a href="#" onclick="writeTable(' . $i .  ');">' . $i . '</a>';
            }
        }
        echo '</ul>';
    }
}
if (isset($conn)) {
    $conn = null;
}
?>