                                <div id="statuslist">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Name</th>
                                                <th>Feeling</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <div>
                                                <?php
                                                require_once 'connect_db.php';
                                                $users = $conn->query('SELECT level, name, feeling FROM user WHERE connected = 1 ORDER BY name');
                                                while ($data = $users->fetch()) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            if ($data['name'] == "Dino") {
                                                                ?>
                                                                <img src="images/userface/10_dino.gif" alt="Dino" title="Dino" />
                                                                <?php
                                                            } else if ($data['name'] == "Cheryl1") {
                                                                ?>
                                                                <img src="images/userface/10_office_queen.gif" alt="Office Queen" title="Office Queen" />
                                                                <?php
                                                            } else if ($data['level'] == 1) {
                                                                ?>
                                                                <img src="images/userface/01_duty_trainee.gif" alt="Duty Trainee" title="Duty Trainee" />
                                                                <?php
                                                            } else if ($data['level'] == 2) {
                                                                ?>
                                                                <img src="images/userface/02_trainee.gif" alt="Trainee" title="Trainee" />
                                                                <?php
                                                            } else if ($data['level'] == 3) {
                                                                ?>
                                                                <img src="images/userface/03_staff.gif" alt="Staff" title="Staff" />
                                                                <?php
                                                            } else if ($data['level'] == 4) {
                                                                ?>
                                                                <img src="images/userface/04_duty_supervisor.gif" alt="Duty Supervisor" title="Duty Supervisor" />
                                                                <?php
                                                            } else if ($data['level'] == 5) {
                                                                ?>
                                                                <img src="images/userface/05_supervisor.gif" alt="Supervisor" title="Supervisor" />
                                                                <?php
                                                            } else if ($data['level'] == 6) {
                                                                ?>
                                                                <img src="images/userface/06_duty_manager.gif" alt="Duty Manager" title="Duty Manager" />
                                                                <?php
                                                            } else if ($data['level'] == 7) {
                                                                ?>
                                                                <img src="images/userface/07_manager.gif" alt="Manager" title="Manager" />
                                                                <?php
                                                            } else if ($data['level'] == 8) {
                                                                ?>
                                                                <img src="images/userface/08_duty_office_manager.gif" alt="Duty Office Manager" title="Duty Office Manager" />
                                                                <?php
                                                            } else if ($data['level'] == 9) {
                                                                ?>
                                                                <img src="images/userface/09_office_manager.gif" alt="Office Manager" title="Office Manager" />
                                                                <?php
                                                            } else if ($data['level'] == 10) {
                                                                ?>
                                                                <img src="images/userface/10_office_king.gif" alt="Office King" title="Office King" />
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><?php echo '<a href="#" onclick="window.open(\'send_sim.php?name=' . $data['name'] . '\',\'sendSim\', \'width=1250px,height=800px\').focus()" >' . $data['name'] . '</a>'; ?></td>
                                                        <td>
                                                            <?php
                                                            if ((int) date("His") < 120000 || (int) date("His") > 133000) {
                                                                if ($data['feeling'] != "") {
                                                                    $cut = explode("/", $data['feeling']);
                                                                    $cut[3] = substr($cut[3], 0, -4);
                                                                    echo '<img src="' . $data['feeling'] . '"';
                                                                    list($width, $height) = getimagesize($data['feeling']);
                                                                    if ($width > 50 && $height > 50) {
                                                                        echo ' height="50" width="50"';
                                                                    } else if ($width > 50) {
                                                                        echo ' width="50"';
                                                                    } else if ($height > 50) {
                                                                        echo ' height="50"';
                                                                    }
                                                                    echo ' alt="' . $cut[3] . '" title="' . $cut[3] . '" />';
                                                                } else {
                                                                    echo '<img src="images/useronline/1_2/online.gif" alt="Online" title="Online"/>';
                                                                }
                                                            } else {
                                                                echo '<img src="images/useronline/5/starvingSmiley.gif" alt="Starving Smiley" title="Lunch is ready ?"/>';
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </tbody>
                                    </table>
                                </div>