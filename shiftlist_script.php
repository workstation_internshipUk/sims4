<?php
include('functions.php');
if (isset($_POST['id']) && isset($_POST['year']) && isset($_POST['week']) && isset($_POST['level']) && isset($_POST['mon']) && isset($_POST['tue']) && isset($_POST['wed']) && isset($_POST['thu']) && isset($_POST['fri']) && isset($_POST['sat']) && isset($_POST['sun'])) {
	$_POST['id'] = (int) $_POST['id'];
	$_POST['year'] = (int) $_POST['year'];
	$_POST['week'] = (int) $_POST['week'];
	$_POST['level'] = (int) $_POST['level'];
	$_POST['mon'] = (int) $_POST['mon'];
	$_POST['tue'] = (int) $_POST['tue'];
	$_POST['wed'] = (int) $_POST['wed'];
	$_POST['thu'] = (int) $_POST['thu'];
	$_POST['fri'] = (int) $_POST['fri'];
	$_POST['sat'] = (int) $_POST['sat'];
	$_POST['sun'] = (int) $_POST['sun'];
	if (($_POST['id'] > 0) && ($_POST['year'] > 2000) && (0 < $_POST['week'] && $_POST['week'] < 54) && (0 < $_POST['level'] && $_POST['level'] < 11) && (0 < $_POST['mon'] && $_POST['mon'] < 8) && (0 < $_POST['tue'] && $_POST['tue'] < 8) && (0 < $_POST['wed'] && $_POST['wed'] < 8) && (0 < $_POST['thu'] && $_POST['thu'] < 8) && (0 < $_POST['fri'] && $_POST['fri'] < 8) && (0 < $_POST['sat'] && $_POST['sat'] < 8) && (0 < $_POST['sun'] && $_POST['sun'] < 8)) {
		require_once 'connect_db.php';
		$log = $conn->prepare('INSERT INTO shift_logs (id_user, week, year, mon, tue, wed, thu, fri, sat, sun, change_date, change_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
		$log->execute(array($_POST['id'],$_POST['week'], $_POST['year'], $_POST['mon'], $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], date('Y-m-d'), date('H:i:s')));
		$check = $conn->prepare('SELECT id FROM shift_list WHERE id_user = ? AND week = ? AND year = ?');
		$check->execute(array($_POST['id'],$_POST['week'],$_POST['year']));
		//If the update is for the next week or later
		if (($_POST['week'] > date("W") && $_POST['year'] == date("Y")) || $_POST['year'] > date("Y")) {
			if ($result = $check->fetch()) {
				$mod = $conn->prepare('UPDATE shift_list SET mon = ?, tue = ?, wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
				$mod->execute(array($_POST['mon'], $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
				echo 'true';
			} else {
				$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
				$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], $_POST['mon'], $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
				echo 'true';
			}
			//If the update is for the current week, only a level 5 can update the current day
		} else if ($_POST['week'] == date("W") && $_POST['year'] == date("Y")) {
			if (date("D") == "Mon") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET mon = ?, tue = ?, wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['mon'], $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], $_POST['mon'], $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET tue = ?, wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Tue") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET tue = ?, wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", $_POST['tue'], $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Wed") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET wed = ?, thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", $_POST['wed'], $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Thu") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET thu = ?, fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", $_POST['thu'], $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Fri") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET fri = ?, sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['fri'], $_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", $_POST['fri'], $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", "4", $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Sat") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET sat = ?, sun = ? WHERE id = ?');
						$mod->execute(array($_POST['sat'], $_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", "4", $_POST['sat'], $_POST['sun']));
						echo 'true';
					}
				} else {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET sun = ? WHERE id = ?');
						$mod->execute(array($_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", "4", "4", $_POST['sun']));
						echo 'true';
					}
				}
			} else if (date("D") == "Sun") {
				if ($_POST['level'] > 5) {
					if ($result = $check->fetch()) {
						$mod = $conn->prepare('UPDATE shift_list SET sun = ? WHERE id = ?');
						$mod->execute(array($_POST['sun'], $result['id']));
						echo 'true';
					} else {
						$add = $conn->prepare('INSERT INTO shift_list (id_user, year, week, mon, tue, wed, thu, fri, sat, sun) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
						$add->execute(array($_POST['id'], $_POST['year'], $_POST['week'], "4", "4", "4", "4", "4", "4", $_POST['sun']));
						echo 'true';
					}
				} else {
					echo 'false';
				}
			}
		} else {
			echo 'false';
		}
		$check->closeCursor();
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>