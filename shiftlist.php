<?php
include('check_cookies.php');
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    
    <body onload="hitByUnicorn(); refresh(); writeWeek(<?php echo date("Y") . ',' . date("W") . ',' . $_SESSION['user']->id(); ?>);">
        <?php
        $selected = "Mysims";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content" >
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Shiftlist</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" onsubmit="return false" class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-md-2">Show all Departments </label>
                                                        <div class="col-md-2">
                                                            <input type="checkbox" id="showall" name="showall" onchange="writeWeek(<?php echo date("Y") . ',' . date("W") . ',' . $_SESSION['user']->id(); ?>);">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                            	<div id="weekDays">
                                            	</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                $daysOnExtra = 0;
                                                $answer = $conn->prepare('SELECT total FROM extra_total WHERE id_user = ?');
                                                $answer->execute(array($_SESSION['user']->id()));
                                                if ($data = $answer->fetch()) {
                                                    $daysOnExtra = floor($data['total'] / 480);
                                                }
                                                $answer->closeCursor();
                                                ?>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Day off with extra hours</th>
                                                            <th><?php echo $daysOnExtra; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Special Days</td>
                                                            <td><?php echo $_SESSION['user']->special_days(); ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Current</th>
                                                            <th>Contract</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $contractDuration = (strtotime($_SESSION['user']->end_date()) - strtotime($_SESSION['user']->start_date())) / (24 * 60 * 60) + 1;
                                                        $daysPast = (strtotime(date('Y-m-d')) - strtotime($_SESSION['user']->start_date())) / (24 * 60 * 60) + 1;
                                                        $contractDayOn = 0;
                                                        $contractDayOff = 0;
                                                        $dayOn = 0;
                                                        $dayOff = 0;
                                                        //Counting the total number of days on and off in the contract
                                                        for ($i = 0; $i < $contractDuration; $i++) {
                                                            if (date("D", strtotime($_SESSION['user']->start_date()) + $i * (24 * 60 * 60)) == "Sat" || date("D", strtotime($_SESSION['user']->start_date()) + $i * (24 * 60 * 60)) == "Sun") {
                                                                $contractDayOff++;
                                                            } else {
                                                                $contractDayOn++;
                                                            }
                                                        }
                                                        $answer = $conn->prepare('SELECT * FROM shift_list WHERE id_user = ? AND year < ? AND week < ?');
                                                        $answer->execute(array($_SESSION['user']->id(), (date("Y") + 1), (date("W") + 1)));
                                                        //Counting the number of days on and off passed
                                                        while ($data = $answer->fetch()) {
                                                            if ($data['week'] != date("W") || $data['year'] != date("Y")) {
                                                                if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['thu'] != 3 && $data['thu'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['fri'] != 3 && $data['fri'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['sat'] != 3 && $data['sat'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                                if ($data['sun'] != 3 && $data['sun'] != 4) {
                                                                    $dayOn++;
                                                                }
                                                            } else {
                                                                if (date("D") == "Mon") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Tue") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Wed") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Thu") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['thu'] != 3 && $data['thu'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Fri") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['thu'] != 3 && $data['thu'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['fri'] != 3 && $data['fri'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Sat") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['thu'] != 3 && $data['thu'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['fri'] != 3 && $data['fri'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['sat'] != 3 && $data['sat'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                } else if (date("D") == "Sun") {
                                                                    if ($data['mon'] != 3 && $data['mon'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['tue'] != 3 && $data['tue'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['wed'] != 3 && $data['wed'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['thu'] != 3 && $data['thu'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['fri'] != 3 && $data['fri'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['sat'] != 3 && $data['sat'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                    if ($data['sun'] != 3 && $data['sun'] != 4) {
                                                                        $dayOn++;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        $answer->closeCursor();
                                                        $dayOn = $dayOn + $_SESSION['user']->special_days() + $daysOnExtra;
                                                        $dayOff = $daysPast - $dayOn;
                                                        ?>
                                                        <tr>
                                                            <td><strong>Day ON</strong></td>
                                                            <td><?php echo $dayOn; ?></td>
                                                            <td><?php echo $contractDayOn; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Day OFF</strong></td>
                                                            <td><?php echo $dayOff; ?></td>
                                                            <td><?php echo $contractDayOff; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called on load or when navigate through the weeks to display the main table
        	function writeWeek(year, week, id) {
        		var xmlhttp = new XMLHttpRequest();
        		xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("weekDays").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST","shiftlist_content.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                if (document.getElementById("showall").checked) {
                    xmlhttp.send("year="+year+"&week="+week+"&id="+id+"&showall=true");
                } else {
                    xmlhttp.send("year="+year+"&week="+week+"&id="+id);
                }
        	}
            //Function called to update the shiftlist when sending the form
            function changeShiftlist(year, week, id, level) {
            	var xmlhttp = new XMLHttpRequest();
            	var mon = document.getElementById("mon"+id).value;
            	var tue = document.getElementById("tue"+id).value;
            	var wed = document.getElementById("wed"+id).value;
            	var thu = document.getElementById("thu"+id).value;
            	var fri = document.getElementById("fri"+id).value;
            	var sat = document.getElementById("sat"+id).value;
            	var sund = document.getElementById("sun"+id).value;
        		xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "false") {
                            writeWeek(year, week, id);
                            alert('An error has occured !');
                        } else {
                        	writeWeek(year, week, id);
                            alert('Shiftlist updated !');
                        }
                    }
                }
                xmlhttp.open("POST","shiftlist_script.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("id="+id+"&year="+year+"&week="+week+"&level="+level+"&mon="+mon+"&tue="+tue+"&wed="+wed+"&thu="+thu+"&fri="+fri+"&sat="+sat+"&sun="+sund);
                return false;
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}