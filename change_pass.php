<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">

    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>

    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Settings";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Change your Password</div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <form method="post" action="change_pass.php" class="form-horizontal" onsubmit="return checkPassChange();">
                                            <div class="form-group">
                                                <div class="row">
                                                	<label class="control-label col-md-6" for="OLD_PASSWORD">Current Password</label>
                                                    <div class=" col-md-6">
            										    <input type="password" class="form-control" name="OLD_PASSWORD" id="oldpassword"><br /><br />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="newpasscontrol">
                                                <div class="row">
            										<label class="control-label col-md-6" for="NEW_PASSWORD">New Password</label>
                                                    <div class=" col-md-6">
            										    <input type="password" class="form-control" name="NEW_PASSWORD" id="newpassword" onkeyup="checkStrength(); checkRepeat()" /><br />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="repeatcontrol">
                                                <div class="row">
            										<label class="control-label col-md-6" for="REPEAT">Repeat</label>
                                                    <div class="col-md-6">
            										    <input type="password" class="form-control" name="REPEAT" id="repeat" onkeyup="checkRepeat()" /><br />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
            										<label class="control-label col-md-6" for="strength">Strength Of The Password</label>
                                                    <div class="col-md-6">
                                                        <img src="images/passstrength/strength0.png" style="height:36; width:213;" id="strengthimg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-offset-2 col-md-6">
                                                        <input class="btn btn-primary btn-block" type="submit" id="changepassbutton" value="Change">
                                                    </div>
                                                </div>
                                            </div>
    										<p>
    											Your password must be "BEST". For that, you can : 
    											<ul>
    												<li>Use a password with 10 characters minimum,</li>
    												<li>Use uppercase characters,</li>
    												<li>Use numbers,</li>
    												<li>Use special characters : </li>
    												<ul>
    													<li>-_=+/\</li>
    													<li>!?</li>
    													<li>$%</li>
    													<li>&lt;&gt;(){}</li>
    													<li>,.;:</li>
    													<li>^@</li>
    												</ul>
    											</ul>
    										</p>
                                        </form>
                                    </div>
                                    <div class="col-md-3">
                                    	<br />
                                    	<p id="passcheck"></p>
                                    	<br /><br />
                                    	<p id="repeatcheck"></p>
                                    </div>
                                    <div class="col-md-3">
                                    	<p id="errorpass">
                                    		<?php
                                    		if (isset($_POST['OLD_PASSWORD']) && isset($_POST['NEW_PASSWORD']) && isset($_POST['REPEAT'])) {
                                                //Check the repetition
                                    			if ($_POST['NEW_PASSWORD'] == $_POST['REPEAT']) {
                                                    //Check the strength of the password
                                    				if (strlen($_POST['NEW_PASSWORD']) > 9) {
    													if (preg_match("#[^a-zA-Z0-9_=+/\\!?$%<>,.;:\^@(){}[\]-]+#",$_POST['NEW_PASSWORD'])) {
    														echo 'The password must be rated as "BEST"';
    													} else if (preg_match("#^[a-z]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[0-9]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[A-Z]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[_=+\/\\!?$%<>,.;:\^@-]*$#",$_POST['NEW_PASSWORD'])) {
    														echo 'The password must be rated as "BEST"';
    													} else if (preg_match("#^[a-z0-9]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[A-Z0-9]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[a-zA-Z]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[a-z_=+\/\\!?$%<>,.;:\^@-]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[A-Z_=+\/\\!?$%<>,.;:\^@-]*$#",$_POST['NEW_PASSWORD']) || preg_match("#^[0-9_=+\/\\!?$%<>,.;:\^@-]*$#",$_POST['NEW_PASSWORD'])) {
    														echo 'The password must be rated as "BEST"';
    													} else {
    														$answer = $conn->prepare('SELECT password,salt FROM user WHERE id = ?');
    														$answer->execute(array($_SESSION['user']->id()));
                                                            //If we found the user
    														if ($data = $answer->fetch()) {
                                                                //If the old password is correct
    															if ($data['password'] == sha1($data['salt'] . $_POST['OLD_PASSWORD'])) {
                                                                    //Generating a new salt
                                                                    $chain = "abcdefghijklmnopqrstuvwxyzABCDEFJKLMNOPQRSTUVWXYZ0123456789 ";
                                                                    $rand = rand(1, 50);
                                                                    $salt = "";
                                                                    for ($i = 0; $i < $rand; $i++) {
                                                                        $shuffled = str_shuffle($chain);
                                                                        $salt = $salt . substr($shuffled, 0, 1);
                                                                    }
                                                                    //Update the password
                                                                    $mod = $conn->prepare('UPDATE user SET password = ?, salt = ?, pass_changed = 1 WHERE id = ?');
    																$mod->execute(array(sha1($salt . $_POST['NEW_PASSWORD']), $salt, $_SESSION['user']->id()));
    																$_SESSION['user']->setPassword(sha1($salt . $_POST['NEW_PASSWORD']));
                                                                    $_SESSION['user']->setSalt($salt);
                                                                    $_SESSION['user']->setPass_changed(1);
    																echo "Your password has been successfully changed.";
    															} else {
    																echo "Your old password is not correct !";
    															}
    														} else {
    															echo "An error has occured !";
    														}
                                                            $answer->closeCursor();
    													}
    												} else {
    													echo "Your new password is too short !";
    												}
                                    			} else {
                                    				echo "The new password and the repeat field are differents !";
                                    			}
                                    		} else {
                                                if ($_SESSION['user']->pass_changed() == 0) {
                                                    echo '<script>alert("You have to change your password !");</script>';
                                                }
                                            }
                                    		?>
                                    	</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <footer>
            	<p>&copy; Studio-Solution.com 2015</p>
        	</footer>
        </div>
    	<!--/.fluid-container-->
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>