<?php
include("check_cookies.php");
if (isset($_POST['RECIPIENT_HIDDEN']) && isset($_POST['PRIORITY']) && isset($_POST['SUBJECT']) && isset($_POST['MESSAGE'])) {
    if ($_POST['RECIPIENT_HIDDEN'] != "") {
        if ($_POST['SUBJECT'] != "") {
            if ($_POST['MESSAGE'] != "") {
                if ($_POST['PRIORITY'] == "low" || $_POST['PRIORITY'] == "normal" || $_POST['PRIORITY'] == "high") {
                    //If the message is sent to all
                    $message = htmlentities($_POST['MESSAGE']);
                    $message = preg_replace('#([^=])(https?://[?=a-z0-9._/-]+)#i', '$1<a href="$2" target="_blank">$2</a>', $message);
                    $message = preg_replace('#^(https?://[?=a-z0-9._/-]+)#i', '<a href="$1" target="_blank">$1</a>', $message);
                    $message = preg_replace('#\[b\](.*)\[/b\]#isU', '<strong>$1</strong>', $message);
                    $message = preg_replace('#\[i\](.*)\[/i\]#isU', '<i>$1</i>', $message);
                    $message = preg_replace('#\[u\](.*)\[/u\]#isU', '<u>$1</u>', $message);
                    $message = preg_replace('#\[s\](.*)\[/s\]#isU', '<s>$1</s>', $message);
                    $message = preg_replace('#\[url=(.+)\](.+)\[/url\]#isU', '<a href="$1" target="_blank">$2</a>', $message);
                    $message = preg_replace('#\[redim=([0-9]{1,3})-([0-9]{1,3})\]\[img=(.+)\](.*)\[/img\]\[/redim\]#isU', '<img src="$3" alt="$4" title="$4" width="$1" height="$2" />', $message);
                    $message = preg_replace('#\[img=(.+)\](.*)\[/img\]#isU', '<img src="$1" alt="$2" title="$2" />', $message);
                    $message = preg_replace('#\[size=([1-3]?[0-9])\](.*)\[/size\]#isU', '<span style="font-size:$1px">$2</span>', $message);
                    $message = preg_replace('#\[color=(\#[A-Fa-f0-9]{6})\](.*)\[/color\]#isU', '<span style="color:$1">$2</span>', $message);
                    if ($_POST['RECIPIENT_HIDDEN'] == "All") {
                        $answer0 = $conn->query('SELECT id FROM user');
                        while ($data0 = $answer0->fetch()) {
                            $send = $conn->prepare('INSERT INTO message (id_sender, id_receiver, sender, receiver, subject, message, send_date, send_time, priority) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
                            $send->execute(array($_SESSION['user']->id(), $data0['id'], $_SESSION['user']->name(), "All", htmlentities($_POST['SUBJECT']), $message, date("Y-m-d"), date("H:i:s"), $_POST['PRIORITY']));
                        }
                        $answer0->closeCursor();
                        echo '<script>alert("Your message has been sent successfully !");</script>';
                    } else {
                        $recipients = explode(",",htmlspecialchars($_POST['RECIPIENT_HIDDEN']));
                        //Looping on each recipient to send the message
                        foreach ($recipients as &$recipient) {
                            $answer1 = $conn->prepare('SELECT id FROM user WHERE name = ?');
                            $answer1->execute(array($recipient));
                            //If the recipient is a user
                            if ($data1 = $answer1->fetch()) {
                                $send = $conn->prepare('INSERT INTO message (id_sender, id_receiver, sender, receiver, subject, message, send_date, send_time, priority) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
                                $send->execute(array($_SESSION['user']->id(), $data1['id'], $_SESSION['user']->name(), htmlspecialchars($_POST['RECIPIENT_HIDDEN']), htmlentities($_POST['SUBJECT']), $message, date("Y-m-d"), date("H:i:s"), $_POST['PRIORITY']));
                            } else {
                                $answer2 = $conn->prepare('SELECT id FROM `group` WHERE name = ?');
                                $answer2->execute(array($recipient));
                                //If the recipient is a group
                                if ($data2 = $answer2->fetch()) {
                                    $answer3 = $conn->prepare('SELECT id FROM user WHERE id_group = ?');
                                    $answer3->execute(array($data2['id']));
                                    while ($data3 = $answer3->fetch()) {
                                        $send = $conn->prepare('INSERT INTO message (id_sender, id_receiver, sender, receiver, subject, message, send_date, send_time, priority) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
                                        $send->execute(array($_SESSION['user']->id(), $data3['id'], $_SESSION['user']->name(), htmlspecialchars($_POST['RECIPIENT_HIDDEN']), htmlentities($_POST['SUBJECT']), $message, date("Y-m-d"), date("H:i:s"), $_POST['PRIORITY']));
                                    }
                                    $answer3->closeCursor();
                                } else {
                                    echo '<script>alert("An error has occured !"); location.reload();</script>';
                                }
                                $answer2->closeCursor();
                            }
                            $answer1->closeCursor();
                        }
                        unset($recipient);
                        echo '<script>alert("Your message has been sent successfully !");</script>';
                    }
                } else {
                    echo '<script>alert("An error has occured !"); location.reload();</script>';
                }
            } else {
                echo '<script>alert("Please type your message !"); location.reload();</script>';
            }
        } else {
            echo '<script>alert("Please type a subject !"); location.reload();</script>';
        }
    } else {
        echo '<script>alert("Please select at least one recipient !"); location.reload();</script>';
    }
}
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); window.resizeTo(1250,800); checkInput('subject'); checkInput('message');">
        <div class="container-fluid" id="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">Send a Sim</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <form method="post" action="send_sim.php" onsubmit="return checkSend();" id="messageform" class="form-horizontal">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-xs-1">To</label>
                                                <div class="col-xs-2">
                                                    <select name="TO_SELECT" class="form-control" onchange="addRecipient();" id="selected">
                                                        <option value="select">Select</option>
                                                        <option value="staff">---Staff---</option>
                                                        <?php
                                                        $answer = $conn->query('SELECT name FROM user ORDER BY name');
                                                        while ($data = $answer->fetch()) {
                                                            echo '<option value="' . $data["name"] . '">' . $data["name"] . '</option>';
                                                        }
                                                        $answer->closeCursor();
                                                        ?>
                                                        <option value="groups">---Groups---</option>
                                                        <option value="all">To All</option>
                                                        <?php
                                                        $answer = $conn->query('SELECT name FROM `group` ORDER BY name');
                                                        while ($data = $answer->fetch()) {
                                                            if ($data['name'] != "Admin") {
                                                                echo '<option value="' . $data["name"] . '">' . $data["name"] . '</option>';
                                                            }
                                                        }
                                                        $answer->closeCursor();
                                                        ?>
                                                    </select>
                                                    <input type="text" name="TO_TYPED" class="form-control" id="totyped" onkeyup="autoComplete(event.keyCode);" onblur="autoComplete(13);"><br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-xs-1">Recipient(s)</label>
                                                <div class="col-xs-2">
                                                    <span id="recipient"><?php
                                                        if (isset($_GET['name'])) {
                                                            $answer = $conn->prepare('SELECT name FROM user WHERE name = ?');
                                                            $answer->execute(array(htmlspecialchars($_GET['name'])));
                                                            if ($data = $answer->fetch()) {
                                                                echo '<a href="#" id="' . $data['name'] . '" onclick="removeRecipient(' . $data['name'] . ')">' . $data['name'] . ',</a>';
                                                            }
                                                            $answer->closeCursor();
                                                        }
                                                        ?></span>
                                                </div>
                                            </div>
                                            <input type="hidden" value="<?php
                                                if (isset($_GET['name'])) {
                                                    $answer = $conn->prepare('SELECT name FROM user WHERE name = ?');
                                                    $answer->execute(array(htmlspecialchars($_GET['name'])));
                                                    if ($data = $answer->fetch()) {
                                                        echo $data['name'];
                                                    }
                                                    $answer->closeCursor();
                                                }
                                                ?>"
                                                name="RECIPIENT_HIDDEN" id="recipienthidden">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-xs-1">Priority</label>
                                                <div class="col-xs-2">
                                                    <select name="PRIORITY" class="form-control" id="priority">
                                                        <option value="low">Low</option>
                                                        <option value="normal" selected="selected">Normal</option>
                                                        <option value="high">High</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group has-error" id="subjectcontrol">
                                            <div class="row">
                                                <label class="control-label col-xs-1">Subject</label>
                                                <div class="col-xs-2">
                                                    <input type="text" name="SUBJECT" class="form-control" id="subjectinput" onkeyup="checkInput('subject');" value="<?php
                                                    if (isset($_POST['subject'])) {
                                                        echo htmlspecialchars($_POST['subject']);
                                                    } else if (isset($_GET['subject'])) {
                                                        echo 'Re : ' . htmlspecialchars($_GET['subject']);
                                                    }
                                                    ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group has-error" id="messagecontrol">
                                            <div class="row">
                                                <label class="control-label col-xs-1">Message</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" class="btn btn-primary" onclick="addTags('b');"><strong>B</strong></button>
                                                <button type="button" class="btn btn-primary" onclick="addTags('i');"><i>i</i></button>
                                                <button type="button" class="btn btn-primary" onclick="addTags('u');"><u>U</u></button>
                                                <button type="button" class="btn btn-primary" onclick="addTags('s');"><s>S</s></button>
                                                <button type="button" class="btn btn-primary" onclick="addTags('url');">URL</button>
                                                <div class="col-xs-3">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <select style="height:50px;" id="selectimg" class="form-control" onchange="imgBackground();">
                                                            	<option value="http://" selected></option>
                                                            	<?php
                            			                        $nbFile = 0;
                            			                        //For everyone
                            			                        if ($dir = opendir('./images/useronline/1_2')) {
                            			                        	?>
                            			                        	<option disabled>---Trainee Smiley---</option>
                            			                        	<?php
                            			                            while (false !== ($file = readdir($dir))) {
                            			                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            			                                    $nbFile++;
                            			                                    ?>
                            			                                    <option value="images/useronline/1_2/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/1_2/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    <?php
                            			                                    
                            			                                }
                            			                            }
                            			                            closedir($dir);
                            			                        }
                            			                        //Feelings only upon level 3
                            			                        if ($_SESSION['user']->level() > 2) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/3')) {
                            				                        	?>
                            				                        	<option disabled>---Staff Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/3/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/3/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 4
                            			                        if ($_SESSION['user']->level() > 3) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/4')) {
                            				                        	?>
                            				                        	<option disabled>---Duty Supervisor Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/4/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/4/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 5
                            			                        if ($_SESSION['user']->level() > 4) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/5')) {
                            				                        	?>
                            				                        	<option disabled>---Supervisor Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/5/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/5/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 6
                            			                        if ($_SESSION['user']->level() > 5) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/6')) {
                            				                        	?>
                            				                        	<option disabled>---Duty Manager Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/6/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/6/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 7
                            			                        if ($_SESSION['user']->level() > 6) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/7')) {
                            				                        	?>
                            				                        	<option disabled>---Manager Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/7/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/7/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 8
                            			                        if ($_SESSION['user']->level() > 7) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/8')) {
                            				                        	?>
                            				                        	<option disabled>---Duty Office Manager Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/8/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/8/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        //Feelings only upon level 9
                            			                        if ($_SESSION['user']->level() > 8) {
                            			                            $nbFile = 0;
                            				                        if ($dir = opendir('./images/useronline/9_10')) {
                            				                        	?>
                            				                        	<option disabled>---Office Manager/King Smiley---</option>
                            				                        	<?php
                            				                            while (false !== ($file = readdir($dir))) {
                            				                                if ($file != '.' && $file != '..' && $file != '.listing') {
                            				                                    $nbFile++;
                            				                                    ?>
                            			                                    	<option value="images/useronline/9_10/<?php echo $file; ?>" style="height:50px;<?php if (preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { ?>background-image:url(images/useronline/9_10/<?php echo $file; ?>);background-repeat:no-repeat;background-position:center;<?php } ?>"><?php if (!preg_match('#Firefox#i',$_SERVER['HTTP_USER_AGENT'])) { echo 'images/useronline/1_2/' . $file; } ?></option>
                            			                                    	<?php
                            				                                }
                            				                            }
                            				                            closedir($dir);
                            				                        }
                            			                        }
                            			                        ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <button type="button" class="btn btn-primary" onclick="addTags('img');">Img</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <select id="selectsize" class="form-control">
                                                            	<?php
                                                            	for ($i = 1; $i < 40; $i++) {
                                                            		if ($i != 14) {
                                                            			echo '<option value="' . $i . '">' . $i . '</option>';
                                                            		} else {
                                                            			echo '<option value="' . $i . '" selected>' . $i . '</option>';
                                                            		}
                                                            	}
                                                            	?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <button type="button" class="btn btn-primary" onclick="addTags('size');">Size</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <select id="selectcolor" class="form-control" onchange="colorBackground();">
                                                            	<option value="#000000" style="background-color:#000000" selected>Black : #000000</option>
                                                            	<option disabled>---Red To Blue---</option>
                                                            	<option value="#FF0000" style="background-color:#FF0000">Red : #FF0000</option>
                                                            	<option value="#DC143C" style="background-color:#DC143C">Crimson : #DC143C</option>
                                                            	<option value="#FFC0CB" style="background-color:#FFC0CB">Pink : #FFC0CB</option>
                                                            	<option value="#DB7093" style="background-color:#DB7093">Pale Violet Red : #DB7093</option>
                                                            	<option value="#D02090" style="background-color:#D02090">Violet Red : #D02090</option>
                                                            	<option value="#872657" style="background-color:#872657">Raspberry : #872657</option>
                                                            	<option value="#DA70D6" style="background-color:#DA70D6">Orchid : #DA70D6</option>
                                                            	<option value="#EE82EE" style="background-color:#EE82EE">Violet : #EE82EE</option>
                                                            	<option value="#FF00FF" style="background-color:#FF00FF">Magenta : #FF00FF</option>
                                                            	<option value="#800080" style="background-color:#800080">Purple : #800080</option>
                                                            	<option value="#9400D3" style="background-color:#9400D3">Dark Violet : #9400D3</option>
                                                            	<option value="#4B0082" style="background-color:#4B0082">Indigo : #4B0082</option>
                                                            	<option value="#8470FF" style="background-color:#8470FF">Light Slate Blue : #8470FF</option>
                                                            	<option value="#6A5ACD" style="background-color:#6A5ACD">Slate Blue : #6A5ACD</option>
                                                            	<option disabled>---Blue To Green---</option>
                                                            	<option value="#0000FF" style="background-color:#0000FF">Blue : #0000FF</option>
                                                            	<option value="#000080" style="background-color:#000080">Navy : #000080</option>
                                                            	<option value="#3D59AB" style="background-color:#3D59AB">Cobalt : #3D59AB</option>
                                                            	<option value="#000080" style="background-color:#4169E1">Royal Blue : #4169E1</option>
                                                            	<option value="#3D59AB" style="background-color:#3D59AB">Cobalt : #3D59AB</option>
                                                            	<option value="#6495ED" style="background-color:#6495ED">Corn Flower Blue : #6495ED</option>
                                                            	<option value="#B0C4DE" style="background-color:#B0C4DE">Light Steel Blue : #B0C4DE</option>
                                                            	<option value="#708090" style="background-color:#708090">Slate Gray : #708090</option>
                                                            	<option value="#1E90FF" style="background-color:#1E90FF">Dodger Blue : #1E90FF</option>
                                                            	<option value="#4682B4" style="background-color:#4682B4">Steel Blue : #4682B4</option>
                                                            	<option value="#87CEFA" style="background-color:#87CEFA">Light Sky Blue : #87CEFA</option>
                                                            	<option value="#00BFFF" style="background-color:#00BFFF">Deep Sky Blue : #00BFFF</option>
                                                            	<option value="#ADD8E6" style="background-color:#ADD8E6">Light Blue : #ADD8E6</option>
                                                            	<option value="#98F5FF" style="background-color:#98F5FF">Cadet Blue : #98F5FF</option>
                                                            	<option value="#00F5FF" style="background-color:#00F5FF">Turquoise : #00F5FF</option>
                                                            	<option value="#00CED1" style="background-color:#00CED1">Dark Turquoise : #00CED1</option>
                                                            	<option value="#E0FFFF" style="background-color:#E0FFFF">Light Cyan : #E0FFFF</option>
                                                            	<option value="#00FFFF" style="background-color:#00FFFF">Cyan : #00FFFF</option>
                                                            	<option value="#008080" style="background-color:#008080">Teal : #008080</option>
                                                            	<option value="#20B2AA" style="background-color:#20B2AA">Light Sea Green : #20B2AA</option>
                                                            	<option value="#00C78C" style="background-color:#00C78C">Turquoise Blue : #00C78C</option>
                                                            	<option value="#00FF7F" style="background-color:#00FF7F">Spring Green : #00FF7F</option>
                                                            	<option value="#98FB98" style="background-color:#98FB98">Pale Green : #98FB98</option>
                                                            	<option value="#32CD32" style="background-color:#32CD32">Lime Green : #32CD32</option>
                                                            	<option value="#228B22" style="background-color:#228B22">Forest Green : #228B22</option>
                                                            	<option disabled>---Green To Red---</option>
                                                            	<option value="#008000" style="background-color:#008000">Green : #008000</option>
                                                            	<option value="#00FF00" style="background-color:#00FF00">Lime : #00FF00</option>
                                                            	<option value="#006400" style="background-color:#006400">Dark Green : #006400</option>
                                                            	<option value="#308014" style="background-color:#308014">Sap Green : #308014</option>
                                                            	<option value="#7CFC00" style="background-color:#7CFC00">Lawn Green : #7CFC00</option>
                                                            	<option value="#ADFF2F" style="background-color:#ADFF2F">Green Yellow : #ADFF2F</option>
                                                            	<option value="#556B2F" style="background-color:#556B2F">Dark Olive Green : #556B2F</option>
                                                            	<option value="#FFFF00" style="background-color:#FFFF00">Yellow : #FFFF00</option>
                                                            	<option value="#808000" style="background-color:#808000">Olive : #808000</option>
                                                            	<option value="#F0E68C" style="background-color:#F0E68C">Khakhi : #F0E68C</option>
                                                            	<option value="#E3CF57" style="background-color:#E3CF57">Banana : #E3CF57</option>
                                                            	<option value="#FFD700" style="background-color:#FFD700">Gold : #FFD700</option>
                                                            	<option value="#DAA520" style="background-color:#DAA520">Golden Rod : #DAA520</option>
                                                            	<option value="#B8860B" style="background-color:#B8860B">Dark Golden Rod : #B8860B</option>
                                                            	<option value="#FFA500" style="background-color:#FFA500">Orange : #FFA500</option>
                                                            	<option value="#F5DEB3" style="background-color:#F5DEB3">Wheat : #F5DEB3</option>
                                                            	<option value="#CD6600" style="background-color:#CD6600">Dark Orange : #CD6600</option>
                                                            	<option value="#F4A460" style="background-color:#F4A460">Sandy Brown : #F4A460</option>
                                                            	<option value="#8B4513" style="background-color:#8B4513">Chocolate : #8B4513</option>
                                                            	<option value="#FF7F50" style="background-color:#FF7F50">Coral : #FF7F50</option>
                                                            	<option value="#EE4000" style="background-color:#EE4000">Orange Red : #EE4000</option>
                                                            	<option value="#FA8072" style="background-color:#FA8072">Salmon : #FA8072</option>
                                                            	<option value="#8A3324" style="background-color:#8A3324">Burn Tumber : #8A3324</option>
                                                            	<option value="#FF6347" style="background-color:#FF6347">Tomato : #FF6347</option>
                                                            	<option value="#EEB4B4" style="background-color:#EEB4B4">Rosy Brown : #EEB4B4</option>
                                                            	<option value="#F08080" style="background-color:#F08080">Light Coral : #F08080</option>
                                                            	<option value="#A52A2A" style="background-color:#A52A2A">Brown : #A52A2A</option>
                                                            	<option value="#B22222" style="background-color:#B22222">Fire Brick : #B22222</option>
                                                            	<option value="#800000" style="background-color:#800000">Maroon : #800000</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <button type="button" class="btn btn-primary" onclick="addTags('color');">Color</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <textarea name="MESSAGE" id="messageinput" class="form-control" rows="5" onkeyup="checkInput('message');" onchange="checkInput('message');"><?php
                                                        if (isset($_POST['message']) && isset($_POST['sender']) && isset($_POST['date']) && isset($_POST['time'])) {
                                                            echo htmlspecialchars($_POST['sender']) . ' Wrote (' . htmlspecialchars($_POST['date']) . ', ' . htmlspecialchars($_POST['time']) . ') : 
';
                                                            echo htmlspecialchars($_POST['message']);
                                                        }
                                                        ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-offset-1 col-xs-1">
                                                <button type="button" class="btn btn-primary" onclick="checkSend();">Submit</button>
                                            </div>
                                            <div class="col-xs-1">
                                                <button type="button" class="btn btn-primary" onclick="preview();">Preview before sending</button>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="previewform" method="post" action="preview.php" target="previewWindow">
                                        <input id="previewmessage" name="message" type="hidden" value="" />
                                        <input id="previewsubject" name="subject" type="hidden" value="" />
                                        <input id="previewreceiver" name="receiver" type="hidden" value="" />
                                        <input id="previewpriority" name="priority" type="hidden" value="" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /panel -->
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called on keyup to find a recipient that matches the beginning of what the user is tiping (auto-completion)
            function autoComplete(keypressed) {
                var typed = document.getElementById("totyped").value;
                var length = typed.length;
                var re;
                var currentRecipient;
                if (keypressed != 8 && keypressed != 13 && keypressed != 37 && keypressed != 46) { //keyCode(8) = return/backspace; keyCode(13) = enter; keyCode(37) = left arrow; keyCode(46) = delete;
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText != "false") {
                                document.getElementById("totyped").value = xmlhttp.responseText;
                                document.getElementById("totyped").select();
                                document.getElementById("totyped").selectionStart = length;
                            }
                        }
                    }
                    xmlhttp.open("POST","name_completer.php",true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("name="+typed);
                } else {
                    if (keypressed == 13) {
                        xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText != "false") {
                                    if (xmlhttp.responseText == "All") {
                                        document.getElementById("recipient").innerHTML = "";
                                        document.getElementById("recipient").innerHTML = document.getElementById("recipient").innerHTML + '<a href="#" id="' + xmlhttp.responseText + '" onclick="removeRecipient(' + xmlhttp.responseText + ')">' + xmlhttp.responseText + '</a>';
                                        document.getElementById("recipienthidden").value = "All";
                                        document.getElementById("totyped").value = "";
                                    } else {
                                        re = new RegExp(xmlhttp.responseText,"");
                                        currentRecipient = document.getElementById("recipient").innerHTML;
                                        if (!(currentRecipient.match(/All/))) {
                                            if (!(currentRecipient.match(re))) {
                                                document.getElementById("recipient").innerHTML = document.getElementById("recipient").innerHTML + '<a href="#" id="' + xmlhttp.responseText + '" onclick="removeRecipient(' + xmlhttp.responseText + ')">' + xmlhttp.responseText + ',</a>';
                                                if (document.getElementById("recipienthidden").value != "") {
                                                    document.getElementById("recipienthidden").value = document.getElementById("recipienthidden").value + "," + xmlhttp.responseText;
                                                } else {
                                                    document.getElementById("recipienthidden").value = xmlhttp.responseText;
                                                }
                                                document.getElementById("totyped").value = "";
                                            } else {
                                                document.getElementById("totyped").value = "";
                                            }
                                        } else {
                                            document.getElementById("totyped").value = "";
                                        }
                                    }
                                } else {
                                    document.getElementById("totyped").value = "";
                                }
                            }
                        }
                        xmlhttp.open("POST","name_checker.php",true);
                        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xmlhttp.send("name="+typed);
                        return false;
                    } else if (keypressed == 37) {
                        document.getElementById("totyped").selectionStart = document.getElementById("totyped").selectionStart - 1;
                        document.getElementById("totyped").selectionEnd = document.getElementById("totyped").value.length;
                    }
                }
                return true;
            }
            //Function called when clicking on a recipient to remove it from the list
            function removeRecipient(name) {
                var nodeList;
                $(name).remove();
                nodeList = document.getElementById("recipient").childNodes;
                if (document.getElementById("recipient").innerHTML == "") {
                    document.getElementById("recipienthidden").value = "";
                } else if (nodeList.length == 1) {
                    document.getElementById("recipienthidden").value = nodeList[0].text.substr(0, nodeList[0].text.length - 1);
                } else {
                    document.getElementById("recipienthidden").value = "";
                    for (i = 0; i < nodeList.length; i++) {
                        document.getElementById("recipienthidden").value = document.getElementById("recipienthidden").value + nodeList[i].text;
                    }
                    document.getElementById("recipienthidden").value = document.getElementById("recipienthidden").value.substr(0, document.getElementById("recipienthidden").value.length - 1);
                }
            }
            //Function called onsubmit to ensure the required fields are filled
            function checkSend() {
                if (document.getElementById("recipient").innerHTML == "") {
                    alert("Please select at least one recipient !");
                    return false;
                } else if (document.getElementById("subjectinput").value == "") {
                    alert("Please type a subject !");
                    return false;
                } else if (document.getElementById("messageinput").value == "") {
                    alert("Please type your message !");
                    return false;
                }
                document.getElementById('messageform').submit();
            }
            //Function called when selecting a user or pressing enter in the corresponding input to add the corresponding recipient in the list
            function addRecipient() {
                var selected = document.getElementById("selected").value;
                var re;
                var currentRecipient;
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText != "false") {
                            if (xmlhttp.responseText == "All") {
                                document.getElementById("recipient").innerHTML = "";
                                document.getElementById("recipient").innerHTML = document.getElementById("recipient").innerHTML + '<a href="#" id="' + xmlhttp.responseText + '" onclick="removeRecipient(' + xmlhttp.responseText + ')">' + xmlhttp.responseText + '</a>';
                                document.getElementById("recipienthidden").value = "All";
                                document.getElementById("selected").value = "select";
                                document.getElementById("selected").focus();
                            } else {
                                re = new RegExp(xmlhttp.responseText,"");
                                currentRecipient = document.getElementById("recipient").innerHTML;
                                if (!(currentRecipient.match(/All/))) {
                                    if (!(currentRecipient.match(re))) {
                                        document.getElementById("recipient").innerHTML = document.getElementById("recipient").innerHTML + '<a href="#" id="' + xmlhttp.responseText + '" onclick="removeRecipient(' + xmlhttp.responseText + ')">' + xmlhttp.responseText + ',</a>';
                                        if (document.getElementById("recipienthidden").value != "") {
                                            document.getElementById("recipienthidden").value = document.getElementById("recipienthidden").value + "," + xmlhttp.responseText;
                                        } else {
                                            document.getElementById("recipienthidden").value = xmlhttp.responseText;
                                        }
                                        document.getElementById("selected").value = "select";
                                        document.getElementById("selected").focus();
                                    } else {
                                        document.getElementById("selected").value = "select";
                                        document.getElementById("selected").focus();
                                    }
                                } else {
                                    document.getElementById("selected").value = "select";
                                    document.getElementById("selected").focus();
                                }
                            }
                        } else {
                            document.getElementById("selected").value = "select";
                            document.getElementById("selected").focus();
                        }
                    }
                }
                xmlhttp.open("POST","name_checker.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("name="+selected);
            }
            //Function called to add BBCode tags in the textarea
            function addTags(tagType) {
                var selStart = document.getElementById("messageinput").selectionStart;
                var selEnd = document.getElementById("messageinput").selectionEnd;
                var textBegin = document.getElementById("messageinput").value.substr(0, selStart);
                var textBetween = document.getElementById("messageinput").value.substr(selStart, selEnd - selStart);
                var textEnd = document.getElementById("messageinput").value.substr(selEnd, document.getElementById("messageinput").value.length);
                if (tagType == 'b') {
                    document.getElementById("messageinput").value = textBegin + "[b]" + textBetween + "[/b]" + textEnd;
                } else if (tagType == 'i') {
                    document.getElementById("messageinput").value = textBegin + "[i]" + textBetween + "[/i]" + textEnd;
                } else if (tagType == 'u') {
                    document.getElementById("messageinput").value = textBegin + "[u]" + textBetween + "[/u]" + textEnd;
                } else if (tagType == 's') {
                    document.getElementById("messageinput").value = textBegin + "[s]" + textBetween + "[/s]" + textEnd;
                } else if (tagType == 'url') {
                    document.getElementById("messageinput").value = textBegin + "[url=http://]" + textBetween + "[/url]" + textEnd;
                } else if (tagType == 'img') {
                	var selectImg = document.getElementById("selectimg").value;
                    document.getElementById("messageinput").value = textBegin + "[img="+selectImg+"]" + textBetween + "[/img]" + textEnd;
                } else if (tagType == 'size') {
                	var selectSize = document.getElementById("selectsize").value;
                    document.getElementById("messageinput").value = textBegin + "[size="+selectSize+"]" + textBetween + "[/size]" + textEnd;
                } else if (tagType == 'color') {
                	var selectColor = document.getElementById("selectcolor").value;
                	document.getElementById("messageinput").value = textBegin + "[color="+selectColor+"]" + textBetween + "[/color]" + textEnd;
                }
            }
            //Function called on change image selected. It changes the background of the select image
            function imgBackground() {
            	document.getElementById("selectimg").style = "height:50px;background-image:url(" + document.getElementById("selectimg").value + ");background-repeat:no-repeat;background-position:center;";
            }
            //Function called on change color selected. It changes the background of the select color
            function colorBackground() {
            	document.getElementById("selectcolor").style = "background-color:" + document.getElementById("selectcolor").value + ";";
            }
            //Function called when the user click the preview button. It displays the preview of the message
            function preview() {
                window.open('','previewWindow','width=600px,height=600px').focus();
                document.getElementById('previewmessage').value = document.getElementById('messageinput').value;
                document.getElementById('previewsubject').value = document.getElementById('subjectinput').value;
                document.getElementById('previewreceiver').value = document.getElementById('recipienthidden').value;
                document.getElementById('previewpriority').value = document.getElementById('priority').value;
                document.getElementById('previewform').submit();
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>