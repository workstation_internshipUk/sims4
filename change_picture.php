<?php
include("check_cookies.php");
//Updating the picture in the session
$answer = $conn->prepare('SELECT picture FROM user WHERE id = ?');
$answer->execute(array($_SESSION['user']->id()));
if ($data = $answer->fetch()) {
	$_SESSION['user']->setPicture($data['picture']);
}
$answer->closeCursor();
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Settings";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Change Your Picture : <img src="<?php if ($_SESSION['user']->picture() != "") { echo $_SESSION['user']->picture(); } else { echo 'images/userpicture/default.gif'; } ?>" height="50" width="50" /></div>
                                <div class="panel-body">
                                    <?php
                                    $nbFile = 0;
                                    $columnCount = 0;
                                    if ($dir = opendir('./images/userpicture')) {
                                        while (false !== ($file = readdir($dir))) {
                                        	if ($file != '.' && $file != '..' && $file != '.listing') {
                                        		$nbFile++;
                                        		if ($columnCount == 0) {
                                        			echo '<div class="row">';
                                        		}
                                        		if ($columnCount == 3) {
                                        			$columnCount = 0;
                                        			echo '<div class="col-md-3">';
                                        			echo '<a href="#">';
                                        			?>
                                        			<img src="images/userpicture/<?php echo $file; ?>" class="img-thumbnail col-md-12" style="max-height:120px;" onclick="changePicture('<?php echo $_SESSION['user']->id(); ?>', 'images/userpicture/<?php echo $file; ?>');" />
                                        			<?php
                                        			echo '</a>';
                                        			echo '</div>';
                                        			echo '</div>';
                                        		} else {
                                        			$columnCount++;
                                        			echo '<div class="col-md-3">';
                                        			echo '<a href="#">';
                                        			?>
                                        			<img src="images/userpicture/<?php echo $file; ?>" class="img-thumbnail col-md-12" style="max-height:120px;" onclick="changePicture('<?php echo $_SESSION['user']->id(); ?>', 'images/userpicture/<?php echo $file; ?>');" />
                                        			<?php
                                        			echo '</a>';
                                        			echo '</div>';
                                        		}
                                        	}
                                        }
                                        if ($columnCount != 3) {
                                            echo '</div>';
                                        }
                                    }
                                    closedir($dir);
                                    ?>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
            	<p>&copy; Studio-Solution.com 2015</p>
        	</footer>
        </div>
    	<!--/.fluid-container-->
        <script>
            //Function called onclick to update the picture of the user
            function changePicture(id, picture) {
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "true") {
                            alert("Your picture has been successfully changed !");
                            location.reload();
                        } else {
                            alert("Your picture failed to be changed !");
                        }
                    }
                }
                xmlhttp.open("POST","change_picture_script.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("id="+id+"&picture="+picture);
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>