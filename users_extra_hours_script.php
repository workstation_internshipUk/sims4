<?php
include('connect_db.php');
?>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID User</th>
            <th>Name</th>
            <th>Total</th>
            <th>Days Off earned</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($_POST['id'])) {
            $_POST['id'] = (int) $_POST['id'];
            if ($_POST['id'] > 0) {
                $answer0 = $conn->prepare('SELECT * FROM extra_total WHERE id_user = ?');
                $answer0->execute(array($_POST['id']));
                if ($data0 = $answer0->fetch()) {
                    $answer1 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                    $answer1->execute(array($_POST['id']));
                    if ($data1 = $answer1->fetch()) {
                        $daysOnExtra = floor($data0['total'] / 480);
                        $hours = floor($data0['total'] / 60);
                        $minutes = $data0['total'] - $hours * 60;
                        if ($hours < 10) {
                            $hours = "0" . $hours;
                        }
                        if ($minutes < 10) {
                            $minutes = "0" . $minutes;
                        }
                        echo '<tr>';
                            echo '<td>' . $_POST['id'] . '</td>';
                            echo '<td>' . $data1['name'] . '</td>';
                            echo '<td>' . $hours . 'h' . $minutes . '</td>';
                            echo '<td>' . $daysOnExtra . '</td>';
                        echo '</tr>';
                    }
                    $answer1->closeCursor();
                }
                $answer0->closeCursor();
            } else {
                $answer0 = $conn->query('SELECT id, name FROM user ORDER BY name');
                while ($data0 = $answer0->fetch()) {
                    if ($data0['name'] != "SebastienC" && $data0['name'] != "Dino" && $data0['name'] != "Celine" && $data0['name'] != "TomNun" && $data0['name'] != "Cheryl1") {
                        $answer1 = $conn->prepare('SELECT total FROM extra_total WHERE id_user = ?');
                        $answer1->execute(array($data0['id']));
                        if ($data1 = $answer1->fetch()) {
                            $daysOnExtra = floor($data1['total'] / 480);
                            $hours = floor($data1['total'] / 60);
                            $minutes = $data1['total'] - $hours * 60;
                            if ($hours < 10) {
                                $hours = "0" . $hours;
                            }
                            if ($minutes < 10) {
                                $minutes = "0" . $minutes;
                            }
                            echo '<tr>';
                                echo '<td>' . $data0['id'] . '</td>';
                                echo '<td>' . $data0['name'] . '</td>';
                                echo '<td>' . $hours . 'h' . $minutes . '</td>';
                                echo '<td>' . $daysOnExtra . '</td>';
                            echo '</tr>';
                        }
                        $answer1->closeCursor();
                    }
                }
                $answer0->closeCursor();
            }
        }
        ?>
    </tbody>
</table>
<?php
if (isset($conn)) {
    $conn = null;
}
?>