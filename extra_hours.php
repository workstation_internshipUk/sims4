<?php
include("check_cookies.php");
checkPermissions(0,3);
if (isset($_POST['EXTRA_DATE']) && isset($_POST['EXTRA_TIME']) && isset($_POST['REFERENCE']) && isset($_POST['TASK'])) {
	if (date('H') >= 17) {
		$_POST['EXTRA_TIME'] = (int) $_POST['EXTRA_TIME'];
		if ($_POST['EXTRA_DATE'] == date('Y-m-d')) {
			if ($_POST['EXTRA_TIME'] >= 15 && $_POST['EXTRA_TIME'] <= 120) {
				$answer0 = $conn->prepare('SELECT id, level FROM user WHERE name = ?');
				$answer0->execute(array(htmlspecialchars($_POST['REFERENCE'])));
				if ($data0 = $answer0->fetch()) {
					if ($data0['level'] >= 5) {
						$answer1 = $conn->prepare('SELECT id, extra_hours FROM extra_hours WHERE extra_date = ? AND id_user = ?');
						$answer1->execute(array(date('Y-m-d'), $_SESSION['user']->id()));
						if ($data1 = $answer1->fetch()) {
							$mod0 = $conn->prepare('UPDATE extra_hours SET id_reference = ?, extra_hours = ?, comment = ? WHERE id = ?');
							$mod0->execute(array($data0['id'], $_POST['EXTRA_TIME'], htmlspecialchars($_POST['TASK']), $data1['id']));
							$answer2 = $conn->prepare('SELECT total FROM extra_total WHERE id_user = ?');
							$answer2->execute(array($_SESSION['user']->id()));
							if ($data2 = $answer2->fetch()) {
								$mod1 = $conn->prepare('UPDATE extra_total SET total = ? WHERE id_user = ?');
								$mod1->execute(array($data2['total'] - $data1['extra_hours'] + $_POST['EXTRA_TIME']));
							} else {
								echo '<script>alert("An unexpected error has occured ! Please contact an IT member.");</script>';
							}
							$answer2->closeCursor();
							echo '<script>alert("Your today\'s extra hours have been updated !");</script>';
						} else {
							$add0 = $conn->prepare('INSERT INTO extra_hours (id_user, id_reference, extra_hours, extra_date, comment) VALUES (?, ?, ?, ?, ?)');
							$add0->execute(array($_SESSION['user']->id(), $data0['id'], $_POST['EXTRA_TIME'], $_POST['EXTRA_DATE'], htmlspecialchars($_POST['TASK'])));
							$answer2 = $conn->prepare('SELECT id, total FROM extra_total WHERE id_user = ?');
							$answer2->execute(array($_SESSION['user']->id()));
							if ($data2 = $answer2->fetch()) {
								$mod = $conn->prepare('UPDATE extra_total SET total = ? WHERE id = ?');
								$mod->execute(array($data2['total'] + $_POST['EXTRA_TIME'], $data2['id']));
							} else {
								$add1 = $conn->prepare('INSERT INTO extra_total (id_user, total) VALUES (?, ?)');
								$add1->execute(array($_SESSION['user']->id(), $_POST['EXTRA_TIME']));
							}
							$answer2->closeCursor();
							echo '<script>alert("Extra hours successfully added !");</script>';
						}
						$answer1->closeCursor();
					} else {
						echo '<script>alert("An error has occured !");</script>';
					}
				} else {
					echo '<script>alert("An error has occured !");</script>';
				}
				$answer0->closeCursor();
			} else {
				echo '<script>alert("An error has occured !");</script>';
			}
		} else {
			echo '<script>alert("An error has occured !");</script>';
		}
	} else {
		echo '<script>alert("You can only add extra hours after 5 p.m. !");</script>';
	}
}
?>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    
	<body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Mysims"; 
        require_once 'menu.php';
        ?>
				<div class="col-md-10" id="content">
					<div class="row">
						<div class="col-md-9">
							<!-- panel -->
							<div class="panel">
				    			<div class="panel-heading">SIMS - Extra hours</div>
								<div class="panel-body">
									<p class="text-center"><img src='images/16762-illustration-of-a-clock-pv.png' alt='Smiley face' height='50' width='50' /></p>
									<h1 class="text-center">EXTRA HOURS</h1>
									<p class="text-center">
										<strong>Hello <?php echo $_SESSION['user']->name(); ?> !</strong><br />
										<?php
										$answer = $conn->prepare('SELECT total FROM extra_total WHERE id_user = ?');
										$answer->execute(array($_SESSION['user']->id()));
										if ($data = $answer->fetch()) {
											$daysOnExtra = floor($data['total'] / 480);
											$hours = floor($data['total'] / 60);
											$minutes = $data['total'] - $hours * 60;
											if ($hours < 10) {
												$hours = "0" . $hours;
											}
											if ($minutes < 10) {
												$minutes = "0" . $minutes;
											}
											echo 'Extra Hours done : ' . $hours . 'h' . $minutes . ' minutes. Day(s) Off earned with extra hours : ' . $daysOnExtra;
										} else {
											echo 'Extra Hours done : 0 minute. Day Off earned with extra hours : 0';
										}
										$answer->closeCursor();
										?>
									</p>
									<form method="post" action="extra_hours.php" class="form-horizontal">
										<div class="form-group">
											<div class="row">
												<label class="control-label col-md-2">Day selected<span class="required">*</span></label>
												<div class=" col-md-6">
													<input type="text" id="datepicker" class="form-control" value="<?php echo date('Y-m-d'); ?>" disabled>
													<input type="hidden" name="EXTRA_DATE" value="<?php echo date('Y-m-d'); ?>">
												</div>
											</div>
										</div>
										<div class="form-group has-error" id="timecontrol">
											<div class="row">
												<div data-tip="change the value with the up or down keys of your keyboard">
													<label class="control-label col-md-2">Extra time<span class="required">*</span> (minutes)</label>
													<div class="col-md-6">
														<input id="timeinput" type="number" class="form-control" name="EXTRA_TIME" min="15" max="120" step="5" onchange="checkInput('time');">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-md-2">Reference of approval<span class="required">*</span></label>
												<div class="col-md-6">
													<select name="REFERENCE" class="form-control" id="ref">
			                                      		<?php
			                                      		$answer = $conn->query("SELECT name FROM user WHERE level >= 5;");
			                                      		while ($data = $answer->fetch()) {
			                                      			echo "<option value=" . $data['name'] . ">" . $data['name'] . "</option>";
			                                      		}
			                                      		$answer->closeCursor();
			                                      		?>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group has-error" id="taskcontrol">
											<div class="row">
												<label class="control-label col-md-2">Tasks<span class="required">*</span></label>
												<div class="col-md-6">
													<textarea name="TASK" class="form-control" rows="5" id="taskinput" onkeyup="checkInput('task');" onblur="checkInput('task');"></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-offset-2 col-md-6">
													<?php
													$answer = $conn->prepare('SELECT id FROM extra_hours WHERE extra_date = ? AND id_user = ?');
													$answer->execute(array(date('Y-m-d'), $_SESSION['user']->id()));
													if ($data = $answer->fetch()) {
														echo '<input class="btn btn-primary btn-block" type="submit" value="Update Extra" />';
													} else {
														echo '<input class="btn btn-primary btn-block" type="submit" value="Add Extra" />';
													}
													$answer->closeCursor();
													?>
												</div>
											</div>
                                        </div>
									</form>
								</div>
							</div>
						<!--end of Block-->
						</div>
						<?php
						include('right.php');                               
						?> 
					</div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
    </body>
</html>
<?php
if (isset($conn)) {
	$conn = null;
}
?>