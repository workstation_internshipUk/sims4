<?php
include("check_cookies.php");
if (!(isset($_POST['message']) && isset($_POST['subject']) && isset($_POST['receiver']) && isset($_POST['priority']))) {
    echo '<script>window.close();</script>';
}
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn();">
        <div class="container-fluid" id="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">Sim</div>
                        <div class="panel-body">
                            <button class="btn btn-primary" onclick="window.close();">Close</button><br /><br />
                            <div class="row">
                                <?php
        	                    echo '<div class="col-xs-4 col-md-6">';
        	                        echo '<strong>From : </strong><a href="send_sim.php?name=' . $_SESSION['user']->name() . '&subject=' . $_POST['subject'] . '">' . $_SESSION['user']->name() . '</a><br />';
        	                        echo '<strong>Date : </strong>' . date('Y-m-d') . '<br />';
        	                        echo '<strong>Subject : </strong>' . $_POST['subject'] . '<br />';
        	                    echo '</div>';
        	                    echo '<div class="col-xs-4 col-md-6">';
        	                        echo '<strong>To : </strong>' . $_POST['receiver'] . '<br />';
        	                        echo '<strong>Time : </strong>' . date('H:i:s') . '<br />';
        	                        ?>
                                    <strong>Priority : </strong>
                                    <?php
                                    if ($_POST['priority'] == "high") {
                                        echo '<strong class="text-danger">High</strong>';
                                    } else if ($_POST['priority'] == "normal") {
                                        echo 'Normal';
                                    } else {
                                        echo '<i class="text-warning">Low</i>';
                                    }
                                    ?>
                                    <br />
                                    <?php
        	                    echo '</div>';
                                ?>
                            </div>
                            <div class="row">
                                <?php
    	                        echo '<div class="col-md-12">';
                                    $message = htmlentities($_POST['message']);
                                    $message = preg_replace('#([^=])(https?://[?=a-z0-9._/-]+)#i', '$1<a href="$2" target="_blank">$2</a>', $message);
                                    $message = preg_replace('#^(https?://[?=a-z0-9._/-]+)#i', '<a href="$1" target="_blank">$1</a>', $message);
                                    $message = preg_replace('#\[b\](.*)\[/b\]#isU', '<strong>$1</strong>', $message);
                                    $message = preg_replace('#\[i\](.*)\[/i\]#isU', '<i>$1</i>', $message);
                                    $message = preg_replace('#\[u\](.*)\[/u\]#isU', '<u>$1</u>', $message);
                                    $message = preg_replace('#\[s\](.*)\[/s\]#isU', '<s>$1</s>', $message);
                                    $message = preg_replace('#\[url=(.+)\](.+)\[/url\]#isU', '<a href="$1" target="_blank">$2</a>', $message);
                                    $message = preg_replace('#\[redim=([0-9]{1,3})-([0-9]{1,3})\]\[img=(.+)\](.*)\[/img\]\[/redim\]#isU', '<img src="$3" alt="$4" title="$4" width="$1" height="$2" />', $message);
                                    $message = preg_replace('#\[img=(.+)\](.*)\[/img\]#isU', '<img src="$1" alt="$2" title="$2" />', $message);
                                    $message = preg_replace('#\[size=([1-3]?[0-9])\](.*)\[/size\]#isU', '<span style="font-size:$1px">$2</span>', $message);
                                    $message = preg_replace('#\[color=(\#[A-Fa-f0-9]{6})\](.*)\[/color\]#isU', '<span style="color:$1">$2</span>', $message);
    	                        	echo '<br /><strong>Message : </strong><br /><p style="width :95%; height:250px; overflow:auto; border: 1px solid black; border-radius:3px">' . str_replace('
', '<br />', $message) . '</p>';
    	                        echo '</div>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- /panel -->
                </div>
                <hr>
                <footer>
                    <p>&copy; Studio-Solution.com 2015</p>
                </footer>
            </div>
        </div>
        <!--/.fluid-container-->
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>