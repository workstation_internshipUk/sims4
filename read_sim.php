<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn()">
        <div class="container-fluid" id="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">Sim</div>
                        <div class="panel-body">
                            <button class="btn btn-primary" onclick="window.close();">Close</button>
                            <?php
                            //If an ID is given, try to read a specific message
                            if (isset($_GET['id'])) {
                            	$_GET['id'] = (int) $_GET['id'];
                            	if ($_GET['id'] > 0) {
                            		$answer = $conn->prepare('SELECT * FROM message WHERE id = ?');
                            		$answer->execute(array($_GET['id']));
                            		if ($data = $answer->fetch()) {
                                        ?>
                                        <button class="btn btn-primary" onclick="checkboxTreat('delete', <?php echo $data['id']; ?>); window.opener.location.reload(); window.close();">Delete</button>
                                        <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=previous"><button class="btn btn-primary">Previous Sims</button></a>
                                        <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=next"><button class="btn btn-primary" onclick="">Next Sims</button></a>
                                        <button class="btn btn-primary" onclick="document.getElementById('replyform').submit();">Reply</button>
                                        <button class="btn btn-primary" onclick="document.getElementById('forwardform').submit();">Forward</button>
                                        <form method="post" action="send_sim.php?name=<?php echo $data['sender']; ?>" id="replyform">
                                            <input type="hidden" value="<?php
                                            $message = htmlspecialchars($data['message']);
                                            $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                            $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                            $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                            $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                            $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                            $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                            $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                            $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                            $message = preg_replace('#&amp;#isU', '&', $message);
                                            echo $message;
                                            ?>" name="message" />
                                            <input type="hidden" value="<?php echo 'Re : ' . $data['subject']; ?>" name="subject" />
                                            <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                            <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                            <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                        </form>
                                        <form method="post" action="send_sim.php" id="forwardform">
                                            <input type="hidden" value="<?php
                                            $message = htmlspecialchars($data['message']);
                                            $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                            $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                            $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                            $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                            $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                            $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                            $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                            $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                            $message = preg_replace('#&amp;#isU', '&', $message);
                                            echo $message;
                                            ?>" name="message" />
                                            <input type="hidden" value="<?php echo 'Fw : ' . $data['subject']; ?>" name="subject" />
                                            <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                            <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                            <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                        </form>
                                        <div class="row">
                                            <?php
                                            //You can only read a message if it is yours or if you are the sender
                                			if ($data['id_receiver'] == $_SESSION['user']->id() || $data['id_sender'] == $_SESSION['user']->id()) {
        	                        			echo '<div class="col-xs-4 col-md-6">';
        	                        				echo '<strong>From : </strong><a href="send_sim.php?name=' . $data['sender'] . '&subject=' . $data['subject'] . '">' . $data['sender'] . '</a><br />';
        	                        				echo '<strong>Date : </strong>' . $data['send_date'] . '<br />';
        	                        				echo '<strong>Subject : </strong>' . $data['subject'] . '<br />';
        	                        			echo '</div>';
        	                        			echo '<div class="col-xs-4 col-md-6">';
        	                        				echo '<strong>To : </strong>' . $data['receiver'] . '<br />';
        	                        				echo '<strong>Time : </strong>' . $data['send_time'] . '<br />';
        	                        				?>
                                                    <strong>Priority : </strong>
                                                    <?php
                                                    if ($data['priority'] == "high") {
                                                        echo '<strong class="text-danger">High</strong>';
                                                    } else if ($data['priority'] == "normal") {
                                                        echo 'Normal';
                                                    } else {
                                                        echo '<i class="text-warning">Low</i>';
                                                    }
                                                    ?>
                                                    <br />
                                                    <?php
        	                        			echo '</div>';
                                                ?>
                                            </div>
                                            <div class="row">
                                                <?php
    	                        				echo '<div class="col-xs-12">';
                                                    echo '<br /><strong>Message : </strong><br /><p style="width :95%; height:250px; overflow:auto; border: 1px solid black; border-radius:3px">' . str_replace('
', '<br />', $data['message']) . '</p>';
                                                echo '</div>';
                                                ?>
    	                        			</div>
                                            <?php
                                            //If the message was unread, set it to read but only if you are the receiver
                            				if ($data['is_read'] == 0) {
                                                if (isset($_GET['sender'])) {
                                                    if ($_GET['sender'] != "true") {
                                                        echo '<script>window.close();</script>';
                                                    }
                                                } else {
                                                    $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                    $read->execute(array($_GET['id']));
                                                }
                            				}
                            			} else {
                            				echo '<script>window.close();</script>';
                            			}
                            		} else {
                            			echo '<script>window.close();</script>';
                            		}
                            		$answer->closeCursor();
                            	} else {
                            		echo '<script>window.close();</script>';
                            	}
                            } else { //If there is no ID, fetch the first unread message with the highest priority
                            	$answer = $conn->prepare('SELECT * FROM message WHERE id_receiver = ? AND is_read = 0 AND priority = "high"');
                            	$answer->execute(array($_SESSION['user']->id()));
                            	if ($data = $answer->fetch()) {
                                    ?>
                                    <button class="btn btn-primary" onclick="checkboxTreat('delete', <?php echo $data['id']; ?>); window.opener.location.reload(); window.close();">Delete</button>
                                    <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=previous"><button class="btn btn-primary">Previous Sims</button></a>
                                    <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=next"><button class="btn btn-primary" onclick="">Next Sims</button></a>
                                    <button class="btn btn-primary" onclick="document.getElementById('replyform').submit();">Reply</button>
                                    <button class="btn btn-primary" onclick="document.getElementById('forwardform').submit();">Forward</button>
                                    <form method="post" action="send_sim.php?name=<?php echo $data['sender']; ?>" id="replyform">
                                        <input type="hidden" value="<?php
                                        $message = htmlspecialchars($data['message']);
                                        $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                        $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                        $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                        $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                        $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                        $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                        $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                        $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                        $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                        $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                        $message = preg_replace('#&amp;#isU', '&', $message);
                                        echo $message;
                                        ?>" name="message" />
                                        <input type="hidden" value="<?php echo 'Re : ' . $data['subject']; ?>" name="subject" />
                                        <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                        <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                        <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                    </form>
                                    <form method="post" action="send_sim.php" id="forwardform">
                                        <input type="hidden" value="<?php
                                        $message = htmlspecialchars($data['message']);
                                        $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                        $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                        $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                        $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                        $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                        $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                        $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                        $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                        $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                        $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                        $message = preg_replace('#&amp;#isU', '&', $message);
                                        echo $message;
                                        ?>" name="message" />
                                        <input type="hidden" value="<?php echo 'Fw : ' . $data['subject']; ?>" name="subject" />
                                        <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                        <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                        <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                    </form>
                                    <div class="row">
                                        <?php
        	                        	echo '<div class="col-xs-4 col-md-6">';
        	                        		echo '<strong>From : </strong><a href="send_sim.php?name=' . $data['sender'] . '&subject=' . $data['subject'] . '">' . $data['sender'] . '</a><br />';
        	                        		echo '<strong>Date : </strong>' . $data['send_date'] . '<br />';
        	                        		echo '<strong>Subject : </strong>' . $data['subject'] . '<br />';
        	                        	echo '</div>';
        	                        	echo '<div class="col-xs-4 col-md-6">';
        	                        		echo '<strong>To : </strong>' . $data['receiver'] . '<br />';
        	                        		echo '<strong>Time : </strong>' . $data['send_time'] . '<br />';
        	                        		?>
                                            <strong>Priority : </strong>
                                            <?php
                                            if ($data['priority'] == "high") {
                                                echo '<strong style="text-danger">High</strong>';
                                            } else if ($data['priority'] == "normal") {
                                                echo 'Normal';
                                            } else {
                                                echo '<i style="text-warning">Low</i>';
                                            }
                                            ?>
                                            <br />
                                            <?php
                                        echo '</div>';
                                    ?>
                                    </div>
                                    <?php
    	                        	echo '<div class="row">';
                                        echo '<div class="col-xs-12">';
                                            echo '<br /><strong>Message : </strong><br /><p style="width :95%; height:250px; overflow:auto; border: 1px solid black; border-radius:3px">' . str_replace('
', '<br />', $data['message']) . '</p>';
                                        echo '</div>';
                                    echo '</div>';
                            		if ($data['is_read'] == 0) {
                                        if (isset($_GET['sender'])) {
                                            if ($_GET['sender'] != "true") {
                                                $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                $read->execute(array($data['id']));
                                            } else {
                                                echo '<script>window.close();</script>';
                                            }
                                        } else {
                                            $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                            $read->execute(array($data['id']));
                                        }
                                    }
                            	} else {
                            		$answer->closeCursor();
                            		$answer = $conn->prepare('SELECT * FROM message WHERE id_receiver = ? AND is_read = 0 AND priority = "normal"');
                            		$answer->execute(array($_SESSION['user']->id()));
                            		if ($data = $answer->fetch()) {
                                        ?>
                                        <button class="btn btn-primary" onclick="checkboxTreat('delete', <?php echo $data['id']; ?>); window.opener.location.reload(); window.close();">Delete</button>
                                        <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=previous"><button class="btn btn-primary">Previous Sims</button></a>
                                        <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=next"><button class="btn btn-primary" onclick="">Next Sims</button></a>
                                        <button class="btn btn-primary" onclick="document.getElementById('replyform').submit();">Reply</button>
                                        <button class="btn btn-primary" onclick="document.getElementById('forwardform').submit();">Forward</button>
                                        <form method="post" action="send_sim.php?name=<?php echo $data['sender']; ?>" id="replyform">
                                            <input type="hidden" value="<?php
                                            $message = htmlspecialchars($data['message']);
                                            $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                            $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                            $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                            $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                            $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                            $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                            $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                            $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                            $message = preg_replace('#&amp;#isU', '&', $message);
                                            echo $message;
                                            ?>" name="message" />
                                            <input type="hidden" value="<?php echo 'Re : ' . $data['subject']; ?>" name="subject" />
                                            <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                            <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                            <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                        </form>
                                        <form method="post" action="send_sim.php" id="forwardform">
                                            <input type="hidden" value="<?php
                                            $message = htmlspecialchars($data['message']);
                                            $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                            $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                            $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                            $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                            $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                            $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                            $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                            $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                            $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                            $message = preg_replace('#&amp;#isU', '&', $message);
                                            echo $message;
                                            ?>" name="message" />
                                            <input type="hidden" value="<?php echo 'Fw : ' . $data['subject']; ?>" name="subject" />
                                            <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                            <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                            <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                        </form>
                                        <div class="row">
                                            <?php
        		                        	echo '<div class="col-xs-4 col-md-6">';
        		                        		echo '<strong>From : </strong><a href="send_sim.php?name=' . $data['sender'] . '&subject=' . $data['subject'] . '">' . $data['sender'] . '</a><br />';
        		                        		echo '<strong>Date : </strong>' . $data['send_date'] . '<br />';
        		                        		echo '<strong>Subject : </strong>' . $data['subject'] . '<br />';
        		                        	echo '</div>';
        		                        	echo '<div class="col-xs-4 col-md-6">';
        		                        		echo '<strong>To : </strong>' . $data['receiver'] . '<br />';
        		                        		echo '<strong>Time : </strong>' . $data['send_time'] . '<br />';
        		                        		?>
                                                <strong>Priority : </strong>
                                                <?php
                                                if ($data['priority'] == "high") {
                                                    echo '<strong style="color:red">High</strong>';
                                                } else if ($data['priority'] == "normal") {
                                                    echo 'Normal';
                                                } else {
                                                    echo '<i style="color:#FFD700">Low</i>';
                                                }
                                                ?>
                                                <br />
                                                <?php
        		                        	echo '</div>';
                                            ?>
                                        </div>
                                        <?php
    		                        	echo '<div class="row">';
                                            echo '<div class="col-xs-12">';
                                                echo '<br /><strong>Message : </strong><br /><p style="width :95%; height:250px; overflow:auto; border: 1px solid black; border-radius:3px">' . str_replace('
', '<br />', $data['message']) . '</p>';
                                            echo '</div>';
                                        echo '</div>';
    	                        		if ($data['is_read'] == 0) {
                                            if (isset($_GET['sender'])) {
                                                if ($_GET['sender'] != "true") {
                                                    $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                    $read->execute(array($_GET['id']));
                                                } else {
                                                    echo '<script>window.close();</script>';
                                                }
                                            } else {
                                                $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                $read->execute(array($data['id']));
                                            }
                                        }
    	                        	} else {
    	                        		$answer->closeCursor();
    	                        		$answer = $conn->prepare('SELECT * FROM message WHERE id_receiver = ? AND is_read = 0 AND priority = "low"');
    	                        		$answer->execute(array($_SESSION['user']->id()));
    	                        		if ($data = $answer->fetch()) {
                                            ?>
                                            <button class="btn btn-primary" onclick="checkboxTreat('delete', <?php echo $data['id']; ?>); window.opener.location.reload(); window.close();">Delete</button>
                                            <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=previous"><button class="btn btn-primary">Previous Sims</button></a>
                                            <a href="redirect_read_sim.php?id=<?php echo $data['id']; ?>&amp;id_user=<?php echo $_SESSION['user']->id(); ?>&amp;type=next"><button class="btn btn-primary" onclick="">Next Sims</button></a>
                                            <button class="btn btn-primary" onclick="document.getElementById('replyform').submit();">Reply</button>
                                            <button class="btn btn-primary" onclick="document.getElementById('forwardform').submit();">Forward</button>
                                            <form method="post" action="send_sim.php?name=<?php echo $data['sender']; ?>" id="replyform">
                                                <input type="hidden" value="<?php
                                                $message = htmlspecialchars($data['message']);
                                                $message = preg_replace('#&lt;a href=&quot;(https?://[?=a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[?=a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                                $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                                $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                                $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                                $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                                $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                                $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                                $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                                $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                                $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                                $message = preg_replace('#&amp;#isU', '&', $message);
                                                echo $message;
                                                ?>" name="message" />
                                                <input type="hidden" value="<?php echo 'Re : ' . $data['subject']; ?>" name="subject" />
                                                <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                                <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                                <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                            </form>
                                            <form method="post" action="send_sim.php" id="forwardform">
                                                <input type="hidden" value="<?php
                                                $message = htmlspecialchars($data['message']);
                                                $message = preg_replace('#&lt;a href=&quot;(https?://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(http://[a-z0-9._/-]+)&lt;/a&gt;#i', '$1', $message);
                                                $message = preg_replace('#&lt;strong&gt;(.+)&lt;/strong&gt;#isU', '[b]$1[/b]', $message);
                                                $message = preg_replace('#&lt;i&gt;(.*)&lt;/i&gt;#isU', '[i]$1[/i]', $message);
                                                $message = preg_replace('#&lt;u&gt;(.*)&lt;/u&gt;#isU', '[u]$1[/u]', $message);
                                                $message = preg_replace('#&lt;s&gt;(.*)&lt;/s&gt;#isU', '[s]$1[/s]', $message);
                                                $message = preg_replace('#&lt;a href=&quot;(http://[a-z0-9._/-]+)&quot; target=&quot;_blank&quot;&gt;(.+)&lt;/a&gt;#isU', '[url=$1]$2[/url]', $message);
                                                $message = preg_replace('#&lt;img width="([0-9]{1,3})" height="([0-9]{1,3})" src=&quot;(.+)&quot; alt=&quot;(.*)&quot; title=&quot;(.*)&quot; /&gt;#isU', '[redim=$1-$2][img=$3]$4[/img][/redim]', $message);
                                                $message = preg_replace('#&lt;img src=&quot;(.+)&quot; alt=&quot;(.*)&quot; /&gt;#isU', '[img=$1]$2[/img]', $message);
                                                $message = preg_replace('#&lt;span style=&quot;font-size:([1-3]?[0-9])px&quot;&gt;(.*)&lt;/span&gt;#isU', '[size=$1]$2[/size]', $message);
                                                $message = preg_replace('#&lt;span style=&quot;color:(\#[A-Fa-f0-9]{6})&quot;&gt;(.*)&lt;/span&gt;#isU', '[color=$1]$2[/color]', $message);
                                                $message = preg_replace('#&amp;#isU', '&', $message);
                                                echo $message;
                                                ?>" name="message" />
                                                <input type="hidden" value="<?php echo 'Fw : ' . $data['subject']; ?>" name="subject" />
                                                <input type="hidden" value="<?php echo $data['sender']; ?>" name="sender" />
                                                <input type="hidden" value="<?php echo $data['send_date']; ?>" name="date" />
                                                <input type="hidden" value="<?php echo $data['send_time']; ?>" name="time" />
                                            </form>
                                            <div class="row">
                                                <?php
        			                        	echo '<div class="col-xs-4 col-md-6">';
        			                        		echo '<strong>From : </strong><a href="send_sim.php?name=' . $data['sender'] . '&subject=' . $data['subject'] . '">' . $data['sender'] . '</a><br />';
        			                        		echo '<strong>Date : </strong>' . $data['send_date'] . '<br />';
        			                        		echo '<strong>Subject : </strong>' . $data['subject'] . '<br />';
        			                        	echo '</div>';
        			                        	echo '<div class="col-xs-4 col-md-6">';
        			                        		echo '<strong>To : </strong>' . $data['receiver'] . '<br />';
        			                        		echo '<strong>Time : </strong>' . $data['send_time'] . '<br />';
                                                    ?>
        			                        		<strong>Priority : </strong>
                                                    <?php
                                                    if ($data['priority'] == "high") {
                                                        echo '<strong style="color:red">High</strong>';
                                                    } else if ($data['priority'] == "normal") {
                                                        echo 'Normal';
                                                    } else {
                                                        echo '<i style="color:#FFD700">Low</i>';
                                                    }
                                                    ?>
                                                    <br />
                                                    <?php
        			                        	echo '</div>';
                                                ?>
                                            </div>
                                            <?php
    			                        	echo '<div class="row">';
                                                echo '<div class="col-xs-12">';
                                                    echo '<br /><strong>Message : </strong><br /><p style="width :95%; height:250px; overflow:auto; border: 1px solid black; border-radius:3px">' . str_replace('
', '<br />', $data['message']) . '</p>';
                                                echo '</div>';
                                            echo '</div>';
    		                        		if ($data['is_read'] == 0) {
                                                if (isset($_GET['sender'])) {
                                                    if ($_GET['sender'] != "true") {
                                                        $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                        $read->execute(array($_GET['id']));
                                                    } else {
                                                        echo '<script>window.close();</script>';
                                                    }
                                                } else {
                                                    $read = $conn->prepare('UPDATE message SET is_read = 1 WHERE id = ?');
                                                    $read->execute(array($data['id']));
                                                }
                                            }
    		                        	} else {
    		                        		echo '<script>window.close();</script>';
    		                        	}
    	                        	}
                            	}
                                $answer->closeCursor();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called when a button linked to the checkboxes is clicked and then treat the request
            function checkboxTreat(type, id) {
                var objCheckBoxes = document.getElementsByName("selectmessage");
                var post = "";
                var confirmMess = "";
                var succeedMess = "";
                var failedMess = "";
                if (type == "delete") {
                    confirmMess = "Do you really want to " + type + " the selected message(s) ?";
                    succeedMess = "The message(s) have/has been successfully put into the trash bin.";
                    failedMess = "The message(s) failed to be put into the trash bin !";
                } else if (type == "save") {
                    confirmMess = "Do you really want to " + type + " the selected message(s) ?";
                    succeedMess = "The message(s) have/has been successfully saved.";
                    failedMess = "The message(s) failed to be saved !";
                } else if (type == "normal") {
                    confirmMess = "Do you really want to put the selected message(s) in your inbox ?";
                    succeedMess = "The message(s) have/has been successfully returned to your inbox.";
                    failedMess = "The message(s) failed to be placed into your inbox !";
                }
                if (id <= 0) {
                    if (confirm(confirmMess)) {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "true") {
                                    alert(succeedMess);
                                    location.reload();
                                } else {
                                    alert(failedMess);
                                }
                            }
                        }
                        for (var i = 0; i < objCheckBoxes.length; i++) {
                            if (objCheckBoxes[i].checked) {
                                post = post + objCheckBoxes[i].id + ",";
                            }
                        }
                        if (post != "") {
                            post = post.substr(0, post.length -1);
                        }
                        xmlhttp.open("POST","inbox_treat.php",true);
                        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xmlhttp.send("post="+post+"&type="+type);
                    }
                } else {
                    if (confirm(confirmMess)) {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "true") {
                                    alert(succeedMess);
                                    location.reload();
                                } else {
                                    alert(failedMess);
                                }
                            }
                        }
                        for (var i = 0; i < objCheckBoxes.length; i++) {
                            if (objCheckBoxes[i].checked) {
                                post = post + objCheckBoxes[i].id + ",";
                            }
                        }
                        if (post != "") {
                            post = post.substr(0, post.length -1);
                        }
                        xmlhttp.open("POST","inbox_treat.php",true);
                        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xmlhttp.send("post="+id+"&type="+type);
                    }
                }
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>