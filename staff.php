<?php 
include_once("check_cookies.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>

    <body onload="hitByUnicorn(); refresh(); writeTable(0);">
        <?php 
        $selected = "Users";
        include_once("menu.php");
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Staff</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="hidden" name="hiddennbentries" id="hiddennbentries" value="10" />
                                                    <input type="hidden" name="hiddenpage" id="hiddenpage" value="1" />
                                                    <input type="hidden" name="hiddendepartment" id="hiddendepartment" value="" />
                                                    <select class="form-control" name="nbentries" id="nbentries" onchange="writeTable(0);">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-offset-3 col-md-3">
                                                    <select class="form-control" name="department" id="department" onchange="writeTable(0);">
                                                        <option value="">Select Department</option>
                                                        <?php
                                                        $answer = $conn->query('SELECT id, name FROM `group` WHERE name <> "Admin"');
                                                        while ($data = $answer->fetch()) {
                                                            echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input name="searchuser" id="searchuser" class="form-control" placeholder="Search User" onkeyup="writeTable(0);" />
                                                </div>
                                            </div><br />
                                            <div id="staffcontent">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!-- Modal PASSWORD -->
        <div class="modal fade" id="ChangePassword" tabindex="-1" role="dialog" aria-labelledby="ChangePassword">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="ChangePassword">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <p id="repeatcheck" style=""></p>
                        <div  id="success" hidden class="alert alert-info"></div>
                        <form onsubmit="return checkPassChangeAdmin();" class="form-horizontal" action="#" method="POST">
                            <input id="userpass" name="user" value="" type="hidden">
                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-4" for="newpassword">New Password</label>
                                    <div class="col-md-5">
                                        <input class="form-control" onkeyup="checkStrength()" id="newpassword" type="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-4" for="repeat">Confirm new password</label>
                                    <div class="col-md-5">
                                        <input class="form-control" onkeyup="checkRepeat()" id="repeat" type="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="strength" class="control-label col-md-4">Strength Of The Password</label><br />
                                    <div class="col-md-5">
                                        <img src="images/passstrength/strength0.png" height="36" width="213" id="strengthimg" /><br /><br />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="closebuttonmodal">Close</button>
                                <button type="Submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="delete" class="modal fade" id="DeleteUser">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button data-dismiss="modal" class="close" type="button">&times;</button>
                        <input type="hidden" id="userdelete" value="" name="user">
                        <h3>Delete user</h3>
                    </div>
                    <div class="modal-body">
                        <p>Do you really want to delete this user : <span id="username"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" onclick="deleteUser()" class="btn btn-primary" href="#">Confirm</a>
                        <a data-dismiss="modal" class="btn btn-default" href="#">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <!--/.fluid-container-->
        <script>
            $(document).on("click", ".passuser", function() {
                var myBookId = $(this).data('id');
                $(".modal-body #userpass").val(myBookId);
            });
            $(document).on("click", ".deleteuser", function() {
                var myBookId = $(this).data('id');
                var myBookName = $(this).data('name');
                $("#userdelete").val(myBookId);
                document.getElementById('username').innerHTML = myBookName;
            });
        </script>
        <script>
            //Function called onload or when the user interact with button and links in the User List. It changes the content displayed in the User List
            function writeTable(pageclicked) {
                var xmlhttp = new XMLHttpRequest();
                var nbentries = 10;
                var department = "";
                document.getElementById('hiddennbentries').value = document.getElementById('nbentries').value;
                document.getElementById('hiddendepartment').value = document.getElementById('department').value;
                nbentries = document.getElementById('hiddennbentries').value;
                department = document.getElementById('hiddendepartment').value;
                var page = 1;
                if (pageclicked != 0) {
                    page = pageclicked;
                    document.getElementById('hiddenpage').value = page;
                } else {
                    page = document.getElementById('hiddenpage').value;
                }
                var searchUser = document.getElementById('searchuser').value;
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("staffcontent").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST","staff_content.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("searchuser="+searchUser+"&page="+page+"&nbentries="+nbentries+"&department="+department);
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>