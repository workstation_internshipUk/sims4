<?php
//This file contains db connect functions
function connectDB($server,$dbname,$user,$password) {
	try {
		$conn = new PDO('mysql:host=' . $server . ';dbname=' . $dbname . ';charset=utf8', $user, $password);
		return $conn;
	} catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
	}
}
//Function to check the end of the string
function endsWith ($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}
function isSelectedGroup($user,$val) {
	if ($user->id_group() == $val) {
		echo "selected";
	}
}
function isSelectedLevel($user,$val) {
	if ($user->level() == $val) {
		echo "selected";
	}
}
//Return working days without week-ends
function getDays($start_date,$end_date) {
    $nb_days = 0;
    $sdate = explode('-',$start_date);
    $edate = explode('-',$end_date);
    $timestampcurr = mktime(0,0,0,$sdate[1],$sdate[2],$sdate[0]);
    $timestampf = mktime(0,0,0,$edate[1],$edate[2],$edate[0]);
    while ($timestampcurr < $timestampf) {
    	if ((date('w',$timestampcurr) != 0) && (date('w',$timestampcurr) != 6)) {
        	$nb_days++;
    	}
		$timestampcurr = mktime(0,0,0,date('m',$timestampcurr),(date('d',$timestampcurr) + 1),date('Y',$timestampcurr));
    }
	return $nb_days;
}
//Function that return the short_desc of the shift_type corresponding to the input int
function getTypeSL($id_type) {
	$conn = connectDB('localhost','Studio_New','root','');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare("SELECT short_desc FROM shift_type WHERE id = ?");
    $stmt->execute(array($id_type));
    $ligne = $stmt->fetch(PDO::FETCH_NUM);
    $stmt->closeCursor();
    if ($ligne[0] != "0") {
        return $ligne[0];
    } else {
        return "-";
    }
}
/*  Show all the days of the week */
function seeWeek($y,$w) {
    /* Create the timestamp of today */
    $today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));  
    if (date("Y") % 4 != 0 || date("Y") % 100 == 0) { //Non leap year
        $nbDays = 365;
    } else { //Leap year
        $nbDays = 366;
    }
    /* For all the days of the years */                               
    for ($i = 1; $i <= $nbDays; $i++) {
    	/* Create a timestamp for each week */
        $week = date("W", mktime(0, 0, 0, 1, $i, $y));
        /* If the week is the same than the current week */
        if ($week == $w) {
            /* For all the days of this week */
            for ($d = 0; $d < 7; $d++) {
                /* If it's today, we write in red */
                if ($today == (mktime(0, 0, 0, 1, $i + $d, $y))) {
        	        echo "<th><font color=\"red\">" . date("l d/m", mktime(0, 0, 0, 1, $i + $d, $y)) . "</font></th>";
        	    } else { /* else in black */
        	        echo "<th>" . date("l d/m", mktime(0, 0, 0, 1, $i + $d, $y)) . "</th>";
        	    }
        	}
        	/* We go out of the loop because we don't need to test all the week */
        	break;
        }
    }
}
/*  Return the day of the week */
function getDateByWeek($y,$w) {
    /* Create the timestamp of today */
    $today = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));     
    if (date("Y") % 4 != 0 || date("Y") % 100 == 0) { //Non leap year
        $nbDays = 365;
    } else { //Leap year
        $nbDays = 366;
    }    
    /* For all the days of the years */
    for ($i = 1; $i <= $nbDays; $i++) {
        /* Create a timestamp for each week */
        $week = date("W", mktime(0, 0, 0, 1, $i, $y));
        /* If the week is the same than the current week */
        if ($week == $w) {
            /* For all the days of this week */
            for ($d = 0; $d < 7; $d++) {
                /* If it's today, we return the name of the day */
                if ($today == (mktime(0, 0, 0, 1, $i+$d, $y))) {
                    return dayName($d);
                }
            }
            /* We go out of the loop because we don't need to test all the week */
            break;
        }
    }
    return 0;
}

function dayName($d) {
    switch ($d) {
        case '0':
            return "mon";
            break;
        case '1':
            return "tue";
            break;
        case '2':
            return "wed";
            break;
        case '3':
            return "thu";
            break;
        case '4':
            return "fri";
            break;
        case '5':
            return "sat";
            break;
        case '6':
            return "sun";
            break;
        default:
            return "mon";
            break;
    }
}
//Check if the user has the rights to go where he is trying to go
function checkPermissions($id_group, $required) {
    if (isset($_SESSION['user'])) {
        if ($id_group <= 0) {
            if (preg_match("#^[0-9]{1,2}$#", $required)) {
                if ($required > $_SESSION['user']->level()) {
                    header('Location: home.php');
                }
            } else {
                echo 'Error in the required var.';
            }
        } else if (preg_match("#^[0-9]{1}$#", $required)) {
            $conn = connectDB('localhost','Studio_New','root','');
            $answer = $conn->prepare('SELECT level FROM permissions WHERE id_group = ? AND id_user = ?');
            $answer->execute(array($id_group, $_SESSION['user']->id()));
            if ($data = $answer->fetch()) {
                if ($required > $data['level']) {
                    header('Location: home.php');
                }
            } else {
                header('Location: home.php');
            }
            $answer->closeCursor();
        } else {
            echo 'Error in the required var.';
        }
    } else {
        header('Location: index.php');
    }
}
//Send back false if not allowed
function lookPermissions($id_group, $required) {
    if (isset($_SESSION['user'])) {
        if ($id_group <= 0) {
            if (preg_match("#^[0-9]{1,2}$#", $required)) {
                if ($required > $_SESSION['user']->level()) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else if (preg_match("#^[0-9]{1}$#", $required)) {
            $conn = connectDB('localhost','Studio_New','root','');
            $answer = $conn->prepare('SELECT level FROM permissions WHERE id_group = ? AND id_user = ?');
            $answer->execute(array($id_group, $_SESSION['user']->id()));
            if ($data = $answer->fetch()) {
                if ($required > $data['level']) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
            $answer->closeCursor();
        } else {
            return false;
        }
    } else {
        return false;
    }
}
?>