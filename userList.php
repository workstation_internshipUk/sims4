<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    
    <body onload="hitByUnicorn(); refresh(); writeTable();">
        <?php
        $selected = "Home";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Interns List</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select name="id" class="form-control" onchange="writeTable();" id="idselect">
                                                <option value="0">--Select Intern--</option>
                                                <?php
                                                $answer = $conn->query('SELECT id, name FROM user ORDER BY name');
                                                while ($data = $answer->fetch()) {
                                                    echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>';
                                                }
                                                $answer->closeCursor();
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="order" class="form-control" onchange="writeTable();" id="orderselect">
                                                <option value="select">--Order By--</option>
                                                <option value="level">Level</option>
                                                <option value="start_date">Start date</option>
                                                <option value="end_date">End date</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="tablecontent">
                                    </div>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called onload and clicking buttons or links, it displays the main table
            function writeTable() {
                var xmlhttp = new XMLHttpRequest();
                var id = document.getElementById("idselect").value;
                var order = document.getElementById("orderselect").value;
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("tablecontent").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("POST","userlist_script.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("id="+id+"&order="+order);
                return false;
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>