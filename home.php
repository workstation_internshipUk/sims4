<?php
include("check_cookies.php");
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
        <script>
            //Function used to delete too old messages
            function checkMessages() {
                var date = new Date();
                var xmlhttp;
                if (((date.getHours() == 8) && (date.getMinutes() >= 55)) || (date.getHours() == 9) && (date.getMinutes() <= 5) || ((date.getHours() == 7) && (date.getMinutes() >= 55)) || (date.getHours() == 8) && (date.getMinutes() <= 5)) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        }
                    }
                    xmlhttp.open("GET","check_messages.php",true);
                    xmlhttp.send();
                }
            }
            //Function used to delete too old shift logs
            function checkShiftLogs() {
                var date = new Date();
                var xmlhttp;
                if (((date.getHours() == 8) && (date.getMinutes() >= 55)) || (date.getHours() == 9) && (date.getMinutes() <= 5) || ((date.getHours() == 7) && (date.getMinutes() >= 55)) || (date.getHours() == 8) && (date.getMinutes() <= 5)) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        }
                    }
                    xmlhttp.open("GET","check_shift_logs.php",true);
                    xmlhttp.send();
                }
            }
            //Function used to delete too old connection logs
            function checkConnectionLogs () {
                var date = new Date();
                var xmlhttp;
                if (((date.getHours() == 8) && (date.getMinutes() >= 55)) || (date.getHours() == 9) && (date.getMinutes() <= 5) || ((date.getHours() == 7) && (date.getMinutes() >= 55)) || (date.getHours() == 8) && (date.getMinutes() <= 5)) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        }
                    }
                    xmlhttp.open("GET","check_connection_logs.php",true);
                    xmlhttp.send();
                }
            }
        </script>
    </head>
    
    <body onload="hitByUnicorn(); refresh(); checkMessages(); checkConnectionLogs(); checkShiftLogs();">
        <?php 
        $selected = "Home";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">SIMS - Home</div>
                                <div class="panel-body" style="text-align:center">
                                    <h1>Welcome <?php echo $_SESSION['user']->name(); ?></h1>
                                    <img src="<?php if ($_SESSION['user']->picture() != '') { echo $_SESSION['user']->picture(); $cut = explode('/', $_SESSION['user']->picture()); $alt = substr($cut[2], 0, -4); } else { echo 'images/userpicture/default.gif'; $alt = 'default'; } ?>" alt="<?php if (isset($alt)) { echo $alt; } ?>" title="<?php if (isset($alt)) { echo $alt; } ?>" /><br />
                                    <strong style="color:blue">Company Mission Statement : </strong><br />
                                    <strong style="color:blue">We use CASH IN and SMART with a PMA to achieve a Win/Win situation.</strong><br />
                                    <img src="images/flag.gif" /> Do not call <img src="images/flag_green.gif" /> You can call<br /><br />
                                    <strong>New Romney / Littlestone</strong><br />
                                    <strong>Call Centre</strong> +44(0) 208 166 0990 <img src="images/flag_green.gif" /><br /><br />
                                    <strong>Mobiles</strong><br />
                                    <strong>Dino Mob</strong> +44(0) 7989 420 877 <img src="images/flag.gif" /><br />
                                    <strong>Cheryl Mob</strong> +44(0) 7908 664 389 <img src="images/flag.gif" /><br />
                                    <strong>Sebastien Mob</strong> +44(0) 7946 469 291 <img src="images/flag.gif" /><br /><br />
                                    <strong>HR Office</strong><br />
                                    <strong>Land Line</strong> +44 (0) 179 736 2727 <img src="images/flag_green.gif" /><br />
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>