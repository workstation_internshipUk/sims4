<?php
include("check_cookies.php");
checkPermissions(6,2);
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    <body onload="hitByUnicorn(); refresh();">
        <?php 
        $selected = "Users";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                	<div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Manage IP Addresses</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form method="post" action="manage_ip.php" onsubmit="return checkIp()" class="form-horizontal">
                                                <div class="form-group" id="ipcontrol">
                                                    <div class="row">
                                                        <label class="control-label col-md-3" for="ADD_IP">Add a new IP<span class="required">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="ADD_IP" class="form-control" id="addip" onkeyup="checkIp()" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group has-error" id="commentcontrol">
                                                    <div class="row">
                                                        <label class="control-label col-md-3" for="COMMENT">Comment<span class="required">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="COMMENT" class="form-control" id="commentinput" onkeyup="checkInput('comment');" /><br />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-6">
                                                            <input class="btn btn-primary btn-block" type="submit" value="Add" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <p id="errorip">
                                                <?php
                                                if (isset($_POST['ADD_IP']) && isset($_POST['COMMENT'])) {
                                                    //If POST on the good format
                                                    if (preg_match("#^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$#",$_POST['ADD_IP'])) {
                                                        $res = explode(".", $_POST['ADD_IP']);
                                                        //If the values are in the good range
                                                        if (256 > $res[0] && 256 > $res[1] && 256 > $res[2] && 256 > $res[3]) {
                                                            $add = $conn->prepare('INSERT INTO ip (ip_address, comment, date_added, id_user) VALUES (?, ?, ?, ?)');
                                                            $add->execute(array($_POST['ADD_IP'], htmlspecialchars($_POST['COMMENT']), date('Y-m-d H:i:s'), $_SESSION['user']->id()));
                                                            echo '<p style="color:green">The IP address has been successfully added.</p>';
                                                        } else {
                                                            echo '<p style="color:red">The IP Address is not on the good format ! Values must be inferior than 256.</p>';
                                                        }
                                                    } else {
                                                        echo '<p style="color:red">The IP address is not on the good format ! IP Address format is : xxx.xxx.xxx.xxx</p>';
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <th>IP Address</th>
                                                <th>Comment</th>
                                                <th>Date Added</th>
                                                <th>ID User</th>
                                                <th>Delete</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $answer = $conn->query('SELECT * FROM ip');
                                                while ($data = $answer->fetch()) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $data['ip_address']; ?></td>
                                                        <td><?php echo $data['comment']; ?></td>
                                                        <td><?php echo $data['date_added']; ?></td>
                                                        <td><?php echo $data['id_user']; ?></td>
                                                        <td><a href="#" onclick="deleteIp(<?php echo $data['id']; ?>)">Delete</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            //Function called onkeyup to check the format of the given IP address
            function checkIp() {
                var ip;
                var res;
                if (document.getElementById('addip').value != "") {
                    ip = document.getElementById('addip').value;
                    if (ip.match(/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/)) {
                        res = ip.split(".");
                        if ((256 > parseInt(res[0])) && (256 > parseInt(res[1])) && (256 > parseInt(res[2])) && (256 > parseInt(res[3]))) {
                            document.getElementById('errorip').style = "text-success";
                            document.getElementById('errorip').innerHTML = "The current written IP Address is on the good format.";
                            document.getElementById('ipcontrol').className = "form-group has-success";
                            return true;
                        } else {
                            document.getElementById('errorip').style = "text-danger";
                            document.getElementById('errorip').innerHTML = "The current written IP Address is not on the good format ! Values must be inferior than 256.";
                            document.getElementById('ipcontrol').className = "form-group has-error";
                            return false;
                        }
                    } else {
                        document.getElementById('errorip').className = "text-danger";
                        document.getElementById('errorip').innerHTML = "The current written IP Address is not on the good format ! IP Address format is : xxx.xxx.xxx.xxx";
                        document.getElementById('ipcontrol').className = "form-group has-error";
                        return false;
                    }
                } else {
                    document.getElementById('errorip').innerHTML = "";
                    document.getElementById('ipcontrol').className = "form-group";
                }
                return false;
            }
            //Function called onclick to delete an IP Address
            function deleteIp(id) {
                if (confirm("Do you really want to delete this IP Address ?")) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText == "true") {
                                alert("This IP has been deleted !");
                                location.reload();
                            } else {
                                alert("This IP failed to be deleted !");
                            }
                        }
                    }
                    xmlhttp.open("POST","delete_ip.php",true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("id="+id);
                }
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}