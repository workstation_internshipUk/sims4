<?php
include('connect_db.php');
?>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Reference</th>
            <th>Extra Hours</th>
            <th>Date</th>
            <th>Comment</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($_POST['iduser']) && isset($_POST['idreference'])) {
            $_POST['iduser'] = (int) $_POST['iduser'];
            $_POST['idreference'] = (int) $_POST['idreference'];
            if ($_POST['iduser'] > 0 && $_POST['idreference'] > 0) {
                $answer0 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                $answer0->execute(array($_POST['iduser']));
                $answer1 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                $answer1->execute(array($_POST['idreference']));
                if ($data0 = $answer0->fetch()) {
                    if ($data1 = $answer1->fetch()) {
                        $answer2 = $conn->prepare('SELECT * FROM extra_hours WHERE id_user = ? AND id_reference = ? ORDER BY id DESC');
                        $answer2->execute(array($_POST['iduser'], $_POST['idreference']));
                        while ($data2 = $answer2->fetch()) {
                            $hours = floor($data2['extra_hours'] / 60);
                            $minutes = $data2['extra_hours'] - (60 * $hours);
                            if ($hours < 10) {
                                $hours = "0" . $hours;
                            }
                            if ($minutes < 10) {
                                $minutes = "0" . $minutes;
                            }
                            $extra_date = new DateTime($data2['extra_date']);
                            echo '<tr>';
                                echo '<td>' . $data0['name'] . '</td>';
                                echo '<td>' . $data1['name'] . '</td>';
                                echo '<td>' . $hours . 'h' . $minutes . '</td>';
                                echo '<td>' . $extra_date->format("d/m/Y") . '</td>';
                                echo '<td>' . $data2['comment'] . '</td>';
                            echo '</tr>';
                        }
                        $answer2->closeCursor();
                    }
                }
                $answer1->closeCursor();
                $answer0->closeCursor();
            } else if ($_POST['iduser'] > 0) {
                $answer0 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                $answer0->execute(array($_POST['iduser']));
                if ($data0 = $answer0->fetch()) {
                    $answer1 = $conn->prepare('SELECT * FROM extra_hours WHERE id_user = ? ORDER BY id DESC');
                    $answer1->execute(array($_POST['iduser']));
                    while ($data1 = $answer1->fetch()) {
                        $answer2 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                        $answer2->execute(array($data1['id_reference']));
                        if ($data2 = $answer2->fetch()) {
                            $hours = floor($data1['extra_hours'] / 60);
                            $minutes = $data1['extra_hours'] - (60 * $hours);
                            if ($hours < 10) {
                                $hours = "0" . $hours;
                            }
                            if ($minutes < 10) {
                                $minutes = "0" . $minutes;
                            }
                            $extra_date = new DateTime($data1['extra_date']);
                            echo '<tr>';
                                echo '<td>' . $data0['name'] . '</td>';
                                echo '<td>' . $data2['name'] . '</td>';
                                echo '<td>' . $hours . 'h' . $minutes . '</td>';
                                echo '<td>' . $extra_date->format("d/m/Y") . '</td>';
                                echo '<td>' . $data1['comment'] . '</td>';
                            echo '</tr>';
                        }
                        $answer2->closeCursor();
                    }
                    $answer1->closeCursor();
                }
                $answer0->closeCursor();
            } else if ($_POST['idreference'] > 0) {
                $answer0 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                $answer0->execute(array($_POST['idreference']));
                if ($data0 = $answer0->fetch()) {
                    $answer1 = $conn->prepare('SELECT * FROM extra_hours WHERE id_reference = ? ORDER BY id DESC');
                    $answer1->execute(array($_POST['idreference']));
                    while ($data1 = $answer1->fetch()) {
                        $answer2 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                        $answer2->execute(array($data1['id_user']));
                        if ($data2 = $answer2->fetch()) {
                            $hours = floor($data1['extra_hours'] / 60);
                            $minutes = $data1['extra_hours'] - (60 * $hours);
                            if ($hours < 10) {
                                $hours = "0" . $hours;
                            }
                            if ($minutes < 10) {
                                $minutes = "0" . $minutes;
                            }
                            $extra_date = new DateTime($data1['extra_date']);
                            echo '<tr>';
                                echo '<td>' . $data2['name'] . '</td>';
                                echo '<td>' . $data0['name'] . '</td>';
                                echo '<td>' . $hours . 'h' . $minutes . '</td>';
                                echo '<td>' . $extra_date->format("d/m/Y") . '</td>';
                                echo '<td>' . $data1['comment'] . '</td>';
                            echo '</tr>';
                        }
                        $answer2->closeCursor();
                    }
                    $answer1->closeCursor();
                }
                $answer0->closeCursor();
            } else {
                $answer0 = $conn->query('SELECT * FROM extra_hours ORDER BY id DESC');
                while ($data0 = $answer0->fetch()) {
                    $answer1 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                    $answer1->execute(array($data0['id_user']));
                    $answer2 = $conn->prepare('SELECT name FROM user WHERE id = ?');
                    $answer2->execute(array($data0['id_reference']));
                    if ($data1 = $answer1->fetch()) {
                        if ($data2 = $answer2->fetch()) {
                            $hours = floor($data0['extra_hours'] / 60);
                            $minutes = $data0['extra_hours'] - (60 * $hours);
                            if ($hours < 10) {
                                $hours = "0" . $hours;
                            }
                            if ($minutes < 10) {
                                $minutes = "0" . $minutes;
                            }
                            $extra_date = new DateTime($data0['extra_date']);
                            echo '<tr>';
                                echo '<td>' . $data1['name'] . '</td>';
                                echo '<td>' . $data2['name'] . '</td>';
                                echo '<td>' . $hours . 'h' . $minutes . '</td>';
                                echo '<td>' . $extra_date->format("d/m/Y") . '</td>';
                                echo '<td>' . $data0['comment'] . '</td>';
                            echo '</tr>';
                        }
                    }
                    $answer2->closeCursor();
                    $answer1->closeCursor();
                }
                $answer0->closeCursor();
            }
        }
        ?>
    </tbody>
</table>
<?php
if (isset($conn)) {
    $conn = null;
}
?>