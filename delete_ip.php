<?php
include_once('connect_db.php');
if (isset($_POST['id'])) {
	$_POST['id'] = (int) $_POST['id'];
	if ($_POST['id'] > 0) {
		$answer = $conn->prepare('DELETE FROM ip WHERE id = ?');
		$answer->execute(array($_POST['id']));
		echo 'true';
	} else {
		echo 'false';
	}
} else {
	echo 'false';
}
if (isset($conn)) {
	$conn = null;
}
?>