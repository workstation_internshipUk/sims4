<?php
include("check_cookies.php");
if (!($_SESSION['user']->name() == "Dino" || $_SESSION['user']->name() == "BenoitT")) {
    header('Location: home.php');
}
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>SIMS 4 - Staff Information Management System</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- Load SCRIPT.JS which will create datepicker for input field  -->
        <script src="js/functions.js"></script>
    </head>
    
    <body onload="hitByUnicorn(); refresh();">
        <?php
        $selected = "";
        require_once 'menu.php';
        ?>
                <div class="col-md-10" id="content">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Deleted Users</div>
                                <div class="panel-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Delete</th>
                                                <th>Restore</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $answer = $conn->query('SELECT id, name, start_date, end_date FROM user WHERE deleted = 1');
                                            while ($data = $answer->fetch()) {
                                                echo '<tr>';
                                                    echo '<td>' . $data['id'] . '</td>';
                                                    echo '<td>' . $data['name'] . '</td>';
                                                    echo '<td>' . $data['start_date'] . '</td>';
                                                    echo '<td>' . $data['end_date'] . '</td>';
                                                    echo '<td><button class="btn btn-primary" onclick="deleteUser(' . $data['id'] . ')">Delete</button></td>';
                                                    echo '<td><button class="btn btn-primary" onclick="restaureUser(' . $data['id'] . ')">Restore</button></td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>
                        <?php
                        include('right.php');
                        ?>
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Studio-Solution.com 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
            function deleteUser(id) {
                if (confirm("Do you really want to definitively delete this user ?")) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText == "true") {
                                alert("The user has been deleted successfully !");
                                location.reload();
                            } else {
                                alert("The user failed to be deleted !");
                            }
                        }
                    }
                    xmlhttp.open("POST","deleted_user_script.php",true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("id="+id+"&type=delete");
                }
            }
            function restaureUser(id) {
                if (confirm("Do you really want to restaure this user ?")) {
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText == "true") {
                                alert("The user has been restaured successfully !");
                                location.reload();
                            } else {
                                alert("The user failed to be restaured !");
                            }
                        }
                    }
                    xmlhttp.open("POST","deleted_user_script.php",true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("id="+id+"&type=restaure");
                }
            }
        </script>
    </body>
</html>
<?php
if (isset($conn)) {
    $conn = null;
}
?>