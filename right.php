                        <div class="col-md-3" id="rightspan">
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">New Romney
                                    <div class="pull-right">
                                        <span class="badge badge-info">
                                            <?php
                                            include('status_header.php');
                                            ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="panel-body" style='height:350px; overflow:auto'>
                                    <?php
                                    include('status_list.php');
                                    ?>
                                </div>
                            </div>
                            <!-- /panel -->
                            <!-- panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <script>
                                        function refreshButton() {
                                            $("#inboxbody").load("inbox_body.php");
                                            $("#inboxheader").load("inbox_header.php");
                                        }
                                    </script>
                                    <a href="inbox.php">Inbox</a> <img src="images/refresh.gif" width="40" height="40" style="cursor:pointer" onclick="refreshButton();"></img>
                                    <div class="pull-right">
                                        <span class="badge badge-info">
                                            <?php
                                            include('inbox_header.php');
                                            ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    include('inbox_body.php');
                                    ?>
                                </div>
                            </div>
                            <!-- /panel -->
                        </div>